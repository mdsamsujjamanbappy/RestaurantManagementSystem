<?php require_once 'header_link.php'; ?>
<?php include("css.php");?>
<link rel="stylesheet" href="../dist/css/invoice-print.css">
<div class="row ">
  <div class="col-md-6 col-md-offset-2 col-sm-12 col-xs-12">
      <div class="invoice_border">
        <div class="">
          <div id="printMe">
            <?php 
             $invoiceNumber = $_SESSION['invoiceNumber'];
             $results = $db_handle->getInvoiceDetailsById($invoiceNumber);
             foreach($results as $dataArr) {
                $invoiceDate = $dataArr['invoiceDate'];
                $invoiceTime = $dataArr['invoiceTime'];
                $discountAmount = $dataArr['discountAmount'];
                $vatAmount = $dataArr['vatAmount'];
                $deliveryCharge = $dataArr['deliveryCharge'];
             }
             ?>
            <div class="row invoice-list">

              <div class="text-center invoice-header">
                <h1><b><?php echo invoiceCompanyTitle();?></b></h1>
                <h4><?php echo invoiceCompanyaddress();?></h4>
                <h4><?php echo invoiceCompanyPhone();?></h4>
                <h4><?php echo invoiceCompanyEmail();?></h4>
              </div>
              <!-- <div class="invoice_gap"></div> -->
             <div class="col-lg-6 col-sm-6">
                <h4>INVOICE NUMBER : <strong><?php echo sprintf('%06u', $invoiceNumber); ?></strong></h4>
                <p></p>
             </div>
             <div class="col-lg-6 col-sm-6">
                <h4>Date & Time: <?php echo $invoiceDate." ".$invoiceTime;?></h4>
             </div>
          </div>
              <!-- <div class="invoice_gap"></div> -->

              <table class="table border-table">
              <tr style="font-weight:bold;">
                  <td class="mobile-hide" width="3%">SN</td>
                  <td>Item Name</td>
                  <td><center>Qty</center></td>
                  <td><center>Rate</center></td>
                  <td class="mobile-hide"><center>Sub-total</center></td>
             </tr>
             <tbody>
              <?php
               $results = $db_handle->getPOSSalesItemList($invoiceNumber);
              $i=0;
              $tot_price = 0;
              $trow=count($results);
              if($trow>0){
               foreach($results as $dataArr) {
                $tot_price+=(($dataArr["quantity"])*(round(($dataArr["unitPrice"]),2)));
              ?>
              <tr>
                  <td class="mobile-hide"><?php echo ++$i; ?></td>
                  <td><?php echo ($dataArr["itemName"]); ?></td>
                  <td><center><?php echo ($dataArr["quantity"]); ?><span class="posqty"><?php echo UNIT;?></span></center></td>
                  <td><center><?php echo CURRENCY1.round($dataArr["unitPrice"],2).CURRENCY; ?></center></td>
                  <td class="mobile-hide"><center><?php echo CURRENCY1.(($dataArr["quantity"])*(round(($dataArr["unitPrice"]),2))).CURRENCY; ?></center></td>
              </tr>
               <?php 
                 
                } 
               }else{
                  echo "<tr><td colspan='6' > <center>No data are matching</center></td></tr>";
                } 
                ?>
                </tbody>
              </table>
              <div class="invoice_divider"></div>
               <div class="row amounts">
                  <div class="col-lg-6 invoice-block col-lg-offset-3">
                      <ul class="to_do amounts">
                          <li><strong> Sub Total   Amount  : </strong><b><?php echo CURRENCY1.round($tot_price,2).CURRENCY; ?></b></li>
                          <li><strong>VAT Amount</strong> (<?php echo ($vatAmount)."%"; ?>) : <b>  <?php echo CURRENCY1.round((($tot_price/100)*$vatAmount),2).CURRENCY; ?></b></li>
                          <li><strong>Discount Amount</strong> :<b> <?php echo CURRENCY1.round(($discountAmount),2).CURRENCY; ?></b></li>
                          <?php if($deliveryCharge>0){ ?>
                          <li><strong> Delivery Charge  : </strong><b> <?php echo CURRENCY1.round(($deliveryCharge),2).CURRENCY; ?></b></li>
                          <?php } ?>
                          <li><strong> Grand Total  : </strong><b> <?php echo CURRENCY1.round(((((($tot_price/100)*$vatAmount)+$tot_price)-$discountAmount)+$deliveryCharge),2).CURRENCY; ?></b></li>
                      </ul>
                  </div>
              </div>
              <center><h4 class="thankYou">**** Thank you ****</h4></center>
             
              <center><button class="btn btn-default btn-md print-button" onclick="printme()" style="margin-top:20px"><i class="fa fa-print"></i> Print</button></center>
            </div>
        </div>
      </div>
      
    </div>
  </div>          

<?php include('js.php');?>
<script> 
window.onload = function () {
    window.print();
}
</script>