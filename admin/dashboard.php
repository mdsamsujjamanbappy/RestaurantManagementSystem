<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $my_tools->title();?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php include('css.php');?>
    <script src="../assets/js/canvasjs.min.js"></script>
       <script>
    window.onload = function () {
     
      var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        theme: "light1",
        title: {
          text: "Todays Sale Statistics"
        },
        axisY: {
          suffix: "%",
          scaleBreaks: {
            autoCalculate: true
          }
        },
        data: [{
          type: "pie",
          yValueFormatString: "#,##0\" pc's\"",
          indexLabel: "{y}",
          indexLabelPlacement: "inside",
          indexLabelFontColor: "white",
          markerColor: "red",
          dataPoints:[ 
              <?php
              date_default_timezone_set('Asia/Dhaka');
              $date = date('Y-m-d');
              $results = $db_handle->getTodaysItemSaleList($date);
              $trow=count($results);
              if($trow>0){
                $dataPoints=array();
                foreach($results as $invoice) {

                  $rid=$invoice['itemId'];
                  $totp_quantity=0;
                  $results1 = $db_handle->getItemSaleHistoryTotalQuantity($rid,$date);
                  if(count($results1)>0){
                    foreach($results1 as $tot) {
                      $itemName=$tot["itemName"];
                      $totp_quantity +=$tot["quantity"];
                    }
                    echo '{ y: '.$totp_quantity.', label: "'.$itemName.'" },';

                  }

                } 
              }
              ?>
          ]
        }]
      });
      chart.render();  


      var chart = new CanvasJS.Chart("chartContainer1", {
        animationEnabled: true,
        theme: "light2",
        title: {
          text: "Todays Sale Statistics"
        },
        axisY: {
          suffix: "%",
          scaleBreaks: {
            autoCalculate: true
          }
        },
        data: [{
          type: "column",
          yValueFormatString: "#,##0\" pc's\"",
          indexLabel: "{y}",
          indexLabelPlacement: "inside",
          indexLabelFontColor: "white",
          markerColor: "red",
          dataPoints:[ 
              <?php
              date_default_timezone_set('Asia/Dhaka');
              $date = date('Y-m-d');
              $results = $db_handle->getTodaysItemSaleList($date);
              $trow=count($results);
              if($trow>0){
                $dataPoints=array();
                foreach($results as $invoice) {

                  $rid=$invoice['itemId'];
                  $totp_quantity=0;
                  $results1 = $db_handle->getItemSaleHistoryTotalQuantity($rid,$date);
                  if(count($results1)>0){
                    foreach($results1 as $tot) {
                      $itemName=$tot["itemName"];
                      $totp_quantity +=$tot["quantity"];
                    }
                    echo '{ y: '.$totp_quantity.', label: "'.$itemName.'" },';

                  }

                } 
              }
              ?>
          ]
        }]
      });
      chart.render();
  

      var chart = new CanvasJS.Chart("chartContainer2", {
        animationEnabled: true,
        theme: "light2",
        title:{
          text: "Sell Statistics Of Last 07 Days"
        },
        axisX: {
          valueFormatString: "DD MMM"
        },
        axisY: {
          title: ""
        },
        data: [{
          color: "green",
          markerBorderColor: "red",
          type: "splineArea", 
          markerColor: "#221155",
          lineColor: "black",
          // xValueType: "dateTime",
          xValueFormat:"YYYY-MM-DD",
          yValueFormatString: "Sell Amount #,##0 Taka",
          dataPoints:[
          
          <?php
            date_default_timezone_set('Asia/Dhaka');
            $r1 = $db_handle->getLastNDays(7, 'Y-m-d');
            foreach ($r1 as $date) {
              $results = $db_handle->getSalesAmountHistoryByDate($date);
              $trow=count($results);
                $totamount=0;
                if($trow>0){
                  foreach($results as $dataArr) {
                      $tot = $dataArr['unitPrice']*$dataArr['quantity'];
                      $totamount+=$tot;
                  } 
                }

                $date=date(strtotime(($date)))*1000;
                echo '{ x: new Date('.$date.'), y: '.$totamount.' },';
              }
            ?>

          ]
        }]
      });
       
      chart.render();


      var chart = new CanvasJS.Chart("chartContainer2pie", {
        animationEnabled: true,
        theme: "light1",
        title:{
          text: "Sell Statistics Of Last 15 Days"
        },
        axisX: {
          valueFormatString: "DD MMM"
        },
        axisY: {
          title: ""
        },
        data: [{
          color: "#4e0308",
          markerBorderColor: "red",
          type: "splineArea", 
          markerColor: "#221155",
          lineColor: "black",
          // xValueType: "dateTime",
          xValueFormat:"YYYY-MM-DD",
          yValueFormatString: "Sell Amount #,##0 Taka",
          dataPoints:[
          
          <?php
            date_default_timezone_set('Asia/Dhaka');
            $r1 = $db_handle->getLastNDays(15, 'Y-m-d');
            foreach ($r1 as $date) {
              $results = $db_handle->getSalesAmountHistoryByDate($date);
              $trow=count($results);
                $totamount=0;
                if($trow>0){
                  foreach($results as $dataArr) {
                      $tot = $dataArr['unitPrice']*$dataArr['quantity'];
                      $totamount+=$tot;
                  } 
                }

                $date=date(strtotime(($date)))*1000;
                echo '{ x: new Date('.$date.'), y: '.$totamount.' },';
              }
            ?>

          ]
        }]
      });
       
      chart.render();


      var chart = new CanvasJS.Chart("chartContainer3", {
        animationEnabled: true,
        theme: "light2",
        title:{
          text: "Sell Statistics Of Last 30 Days"
        },
        axisX: {
          valueFormatString: "DD MMM"
        },
        axisY: {
          title: ""
        },
        data: [{
          color: "#f39c12",
          markerBorderColor: "blue",
          type: "splineArea", 
          markerColor: "#221155",
          lineColor: "black",
          // xValueType: "dateTime",
          xValueFormat:"YYYY-MM-DD",
          yValueFormatString: "Sell Amount #,##0 Taka",
          dataPoints:[
          
          <?php
            date_default_timezone_set('Asia/Dhaka');
            $r1 = $db_handle->getLastNDays(30, 'Y-m-d');
            foreach ($r1 as $date) {
              $results = $db_handle->getSalesAmountHistoryByDate($date);
              $trow=count($results);
                $totamount=0;
                if($trow>0){
                  foreach($results as $dataArr) {
                      $tot = $dataArr['unitPrice']*$dataArr['quantity'];
                      $totamount+=$tot;
                  } 
                }

                $date=date(strtotime(($date)))*1000;
                echo '{ x: new Date('.$date.'), y: '.$totamount.' },';
              }
            ?>

          ]
        }]
      });
       
      chart.render();

}
</script>
 </head>
<body class="hold-transition skin-blue sidebar-mini animated fadeIn">
<div class="wrapper">

  <header class="main-header">
    <?php $my_tools->logoArea();?>
    
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <?php include('messages.php');?>
          <!-- Notifications: style can be found in dropdown.less -->
          <?php include('notifications.php');?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php $my_tools->adminImage();?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php $my_tools->adminFullname();?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php $my_tools->adminImage();?>" class="img-circle" alt="User Image">

                <p>
                  <?php $my_tools->adminFullname();?>
                </p>
              </li>
                  <?php $my_tools->profileLink();?>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <?php $my_tools->control_sidebar();?>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php $my_tools->adminImage();?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php $my_tools->adminFullname();?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <?php include('search_form.php');?>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <?php $my_tools->adminMenu();?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box  box-danger">
        <div class="box-header with-border">

        </div>
        <div class="box-body">
         <div class="row">
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-purple"><i class="fa fa-sitemap"></i></span>
                  <?php
                    $results = $db_handle->getItemCategoryList();
                    $trow=count($results);
                  ?>
                  <div class="info-box-content">
                    <span class="info-box-text">Total Item Categories</span>
                    <span class="info-box-number"><?php echo sprintf('%04u', $trow); ?></span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-red"><i class="fa fa-cutlery"></i></span>
                  <?php
                    $results = $db_handle->getItemList();
                    $trow=count($results);
                  ?>
                  <div class="info-box-content">
                    <span class="info-box-text">Total Items</span>
                    <span class="info-box-number"><?php echo sprintf('%04u', $trow); ?></span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->

              <!-- fix for small devices only -->
              <div class="clearfix visible-sm-block"></div>

              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-green"><i class="fa fa-shopping-cart"></i></span>

                  <div class="info-box-content">
                  <?php
                    $results = $db_handle->getSalesList();
                    $trow=count($results);
                  ?>
                    <span class="info-box-text">Total Sales</span>
                    <span class="info-box-number"><?php echo sprintf('%04u', $trow); ?></span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-yellow"><i class="fa fa-users"></i></span>
                  <?php
                    $results = $db_handle->getEmployeeList();
                    $trow=count($results);
                  ?>
                  <div class="info-box-content">
                    <span class="info-box-text">Total Employee</span>
                    <span class="info-box-number"><?php echo sprintf('%04u', $trow); ?></span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer"></div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <center><h4><b> Latest Sale List</b><h4></center><hr>
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                      <tr>
                        <th>Invoice Number</th>
                        <th>Customer Name</th>
                        <th>Total Amount</th>
                        <th>Date</th>
                      </tr>
                      </thead>
                      <tbody>
                      <?php
                      $results = $db_handle->getSalesList();
                      $i=0;
                      $trow=count($results);
                      if($trow>0){
                       foreach($results as $dataArr) {
                        ++$i;
                        $tot_price=0;
                        $results1 = $db_handle->getPOSSalesItemList($dataArr["invoiceNumber"]);
                        if(count($results1)>0){
                         foreach($results1 as $dataArr1) {
                          $tot_price+=(($dataArr1["quantity"])*($dataArr1["unitPrice"]));
                          }
                        }
                      if($i<=8){
                    ?>
                      <tr>
                        <td><a target="_NEW" href="view_invoice.php?rid=<?php echo base64_encode(($dataArr["invoiceNumber"])); ?>" style="color:#4e0308;font-weight:bold;" data-toggle="tooltip" data-placement="top" title="View Details" >INV-<?php echo sprintf('%06u', ($dataArr["invoiceNumber"])); ?></a></td>
                        <td><?php echo ($dataArr["customerName"]); ?></td>
                        <td><b><?php echo CURRENCY1.round($tot_price,2).CURRENCY; ?></b></td>
                        <td><?php echo ($dataArr["invoiceDate"]); ?></td>
                      </tr>
                      <?php } } } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="col-md-6">
                  <center><h4><b> Latest Expense List</b><h4></center><hr>
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                      <tr>
                        <th>Expense Name</th>
                        <th>Expense Category</th>
                        <th>Expense Amount</th>
                        <th>Expense Date</th>
                      </tr>
                      </thead>
                      <tbody>
                      <?php
                      $results = $db_handle->getExpenseList();
                      $i=0;
                      $trow=count($results);
                      if($trow>0){
                       foreach($results as $dataArr) {
                        ++$i;
                      if($i<=8){
                    ?>
                      <tr>
                        <td><?php echo ($dataArr["name"]); ?></td>
                        <td><?php echo ($dataArr["categoryName"]); ?></td>
                        <td><?php echo CURRENCY1.($dataArr["amount"]).CURRENCY; ?></td>
                        <td><?php echo ($dataArr["expenseDate"]); ?></td>
                      </tr>
                      <?php } } } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                
              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer"></div>
            <!-- /.box-footer-->

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-4 " >

                  <div class="chart ">
                        <div id="chartContainer" class="chart_hidden" style="height: 300px; width: 100%;"></div>
                  </div>
                  <!-- /.chart-responsive -->
                </div>
                <div class="col-md-4">

                  <div class="chart">
                        <div id="chartContainer1" class="chart_hidden" style="height: 300px; width: 100%;"></div>
                  </div>
                  <!-- /.chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                  <div class="table-responsive">
                    <table class="table no-margin" style="font-size:11px;">
                      <thead>
                      <tr>
                        <th>Item Name</th>
                        <th>Category</th>
                        <th>Total Quantity</th>
                      </tr>
                      </thead>
                      <tbody>
                    <?php
                      date_default_timezone_set('Asia/Dhaka');
                      $date = date('Y-m-d');
                      $results = $db_handle->getTodaysItemSaleList($date);
                      $trow=count($results);
                      if($trow>0){
                        $i=0;

                        foreach($results as $invoice) {

                          $rid=$invoice['itemId'];
                          $totp_quantity=0;
                          $results1 = $db_handle->getItemSaleHistoryTotalQuantity($rid,$date);
                          if(count($results1)>0){
                            ++$i;
                            if($i<=8){
                            foreach($results1 as $tot) {
                              $itemName=$tot["itemName"];
                              $categoryName=$tot["categoryName"];
                              $totp_quantity +=$tot["quantity"];
                            } ?>
                            <tr>
                              <td style="text-transform: capitalize;"><?php echo $itemName; ?></td>
                              <td><?php echo ($categoryName); ?></td>
                              <td><?php echo ($totp_quantity); ?></td>
                            </tr>
                         <?php
                            }
                          }

                        } 
                      }
                      ?>
                      </tbody>
                    </table>
                  </div>

                </div>

              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer"></div>
            <!-- /.box-footer-->

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">

                  <div class="chart">
                        <div id="chartContainer2" class="chart_hidden" style="height: 300px; width: 100%;"></div>
                  </div>
                  <!-- /.chart-responsive -->
                </div>
                <div class="col-md-6">

                  <div class="chart">
                        <div id="chartContainer2pie" class="chart_hidden" style="height: 300px; width: 100%;"></div>
                  </div>
                  <!-- /.chart-responsive -->
                </div>
                
              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer"></div>
            <!-- /.box-footer-->

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>


      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">

                  <div class="chart">
                        <div id="chartContainer3" class="chart_hidden" style="height: 300px; width: 100%;"></div>
                  </div>
                  <!-- /.chart-responsive -->
                </div>
                
              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <div class="box-footer"></div>
            <!-- /.box-footer-->

          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $my_tools->myFooter(); ?>

</div>
<!-- ./wrapper -->

<?php include('js.php');?>

</body>
</html>
