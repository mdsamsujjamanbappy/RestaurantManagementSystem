<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $my_tools->title();?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php include('css.php');?> 
  <!-- DataTables -->
  <link rel="stylesheet" href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css"> 
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php $my_tools->logoArea();?>
    
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <?php include('messages.php');?>
          <!-- Notifications: style can be found in dropdown.less -->
          <?php include('notifications.php');?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php $my_tools->adminImage();?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php $my_tools->adminFullname();?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php $my_tools->adminImage();?>" class="img-circle" alt="User Image">

                <p>
                  <?php $my_tools->adminFullname();?>
                </p>
              </li>
                  <?php $my_tools->profileLink();?>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <?php $my_tools->control_sidebar();?>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php $my_tools->adminImage();?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php $my_tools->adminFullname();?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <?php include('search_form.php');?>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <?php $my_tools->adminMenu();?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <?php if(isset($_GET['sst'])){ ?>
           <div class="alert alert-success alert-dismissible fade in" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong> Item Category information has been successfully <?php echo $_GET['smsg'];?>.</strong>
            </div>
            <div class="clearfix"></div>
           <?php } ?>

          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Item Category List</h3>

              <div class="box-tools pull-right">
                
                <button type="button"  data-toggle="modal" data-target="#add_category" class="btn btn-success btn-sm" ><i class="fa fa-plus"></i> Add New Category</button>
                <div class="btn-group">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
               <table id="example1" class="table table-hover table-bordered table-striped table-responsive">
                <thead>
                <tr>
                  <th width="10%">Serial</th>
                  <th>Category Name</th>
                  <th width="45%">Category Description</th>
                  <th width="15%">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  $results = $db_handle->getItemCategoryList();
                  $i=0;
                  $trow=count($results);
                  if($trow>0){
                   foreach($results as $dataArr) {
                    ++$i;
                ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo ($dataArr["categoryName"]); ?></td>
                  <td><?php echo ($dataArr["categoryDescription"]); ?></td>
                  <td>
                      <div class="btn-group">
                      <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        Options <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#" data-toggle="modal" data-target="#view_itemcategory<?php echo $i; ?>" ><i class="fa fa-eye"></i> Details Information</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#edit_itemcategory<?php echo $i; ?>" ><i class="fa fa-edit"></i> Edit Information</a></li>
                        <li><a href="delete_item_category_information.php?id=<?php echo base64_encode($dataArr['id']); ?>" onclick="return confirm('Are you sure to delete?')"><i class="fa fa-trash"></i> Delete Information</a></li>

                      </ul>
                    </div>
                  </td>
                </tr>

                <div class="modal fade" id="view_itemcategory<?php echo $i; ?>">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-server"></i> Category Details Information</h4>
                      </div>
                      <div class="modal-body">
                        <div class="form-group">
                          <label for="categoryName">Category Name</label>
                          <input type="text" disabled id="categoryName" class="form-control" value="<?php echo $dataArr['categoryName']; ?>"  >
                        </div>
                        <div class="form-group">
                          <label for="categoryDescription">Category Description</label>
                          <textarea disabled name="categoryDescription" id="categoryDescription" class="form-control"><?php echo $dataArr['categoryDescription']; ?></textarea>
                        </div>
                        <br>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                      </div>
                       
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->


                <div class="modal fade" id="edit_itemcategory<?php echo $i; ?>">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <form action="update_category.php" method="POST">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-server"></i> Edit Category Information</h4>
                      </div>
                      <div class="modal-body">
                        <div class="form-group">
                          <label for="categoryName">Category Name</label>
                          <input name="_MSBtoken" hidden value="<?php echo md5(rand(14446,50200));?>" >
                          <input name="id" hidden value="<?php echo $dataArr['id']; ?>" >
                          <input type="text" required id="categoryName" class="form-control" value="<?php echo $dataArr['categoryName']; ?>" name="categoryName" >

                        </div>
                        <div class="form-group">
                          <label for="categoryDescription">Category Description</label>
                          <textarea name="categoryDescription" id="categoryDescription" class="form-control" placeholder="Kitchen Description "><?php echo $dataArr['categoryDescription']; ?></textarea>
                        </div>
                        <br>
                      </div>
                      <div class="modal-footer">
                        <button type="submit" class="btn btn-success pull-left"><i class="fa fa-refresh"></i> Update Information</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                      </div>
                    </form>

                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

                <?php } } ?>
              </tbody>
              </table>

            </div>

            </div>
            <!-- ./box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $my_tools->myFooter(); ?>

  <div class="modal fade" id="add_category">
    <div class="modal-dialog">
      <div class="modal-content">
       <form action="save_category.php" method="POST">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="fa fa-server"></i> New Category Information</h4>
        </div>
        <div class="modal-body">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-map-signs"></i></span>
            <input name="_MSBtoken" hidden value="<?php echo md5(rand(14446,50200));?>" >
            <input type="text" autofocus required id="categoryName" name="categoryName" class="form-control" placeholder="Category Name ">
          </div>
          <br>

          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-align-left"></i></span>
            <textarea name="categoryDescription" id="categoryDescription" class="form-control" placeholder="Category Description "></textarea>
          </div>
          <br>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success pull-left"><i class="fa fa-save"></i> Save Information</button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
        </div>
         
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

 
</div>
<!-- ./wrapper -->
<?php include('js.php');?>
<!-- DataTables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="../vendors/select2/dist/js/select2.full.min.js"></script>
<script>
  $(function () {
    $('#example1').DataTable()
  })
</script>
</body>
</html>
