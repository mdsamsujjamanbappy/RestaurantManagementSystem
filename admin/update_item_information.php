<?php
require_once 'header_link.php';

if(isset($_POST['_MSBtoken'])){

	$id = $_POST['id'];
	$itemName = $_POST['itemName'];
	$itemCode = $_POST['itemCode'];
	$itemSellPrice = $_POST['itemSellPrice'];
	$itemStockManagement = $_POST['itemStockManagement'];
	
	if(isset($_POST['itemQuantity'])){
		$itemQuantity = $_POST['itemQuantity'];
	}else{
		$itemQuantity = 0;
	}
	
	$itemDescription = $_POST['itemDescription'];
	$itemCategoryId = $_POST['itemCategoryId'];
	$itemDepartmentId = $_POST['itemDepartmentId'];
	$itemExpireDateAction = $_POST['itemExpireDateAction'];
	$itemExpireDate = $_POST['itemExpireDate'];
	$itemStockAlert = $_POST['itemStockAlert'];
	$image=$_POST['itemPrePhoto'];

	if(!empty($_FILES['itemPhoto']['name'])){
      if(($_FILES['itemPhoto']['size']) <= (3000*1024))
      {	
      	if($image!="idefault.jpg"){
          	$url = '../item_images/'.$image;
      		unlink($url);
      	}


    	  $maxrow= $id;

          $picture_tmp = $_FILES['itemPhoto']['tmp_name'];
          $picture_name = $_FILES['itemPhoto']['name'];
          $picture_type = $_FILES['itemPhoto']['type'];
         
          $arr1 = explode(".", $picture_tmp);
          $extension1 = strtolower(array_pop($arr1));  
          
          $arr = explode(".", $picture_name);
          $extension = strtolower(array_pop($arr));

          $image="item".date('Ymdhs')."_".$maxrow.".".$extension;
          $newfilename1="item".date('Ymdhs')."_".$maxrow.".".$extension1;
          $path = '../item_images/'.$image;

          move_uploaded_file($picture_tmp, $path);
	    }
	  }
	$db_handle->updateItemInformation($id,$itemName,$itemCode,$itemSellPrice,$itemStockManagement,$itemQuantity,$itemDescription,$itemCategoryId,$itemDepartmentId,$itemExpireDateAction,$itemExpireDate,$itemStockAlert,$image);

	echo "<script>document.location.href='item_list.php?sst=success&&smsg=updated';</script>";
}
?>