<?php 
require_once 'header_link.php';
if(!isset($_SESSION['tmpSuppliesId']))
{
 $tmp_id1 = rand(1000000,9999999);
 $_SESSION['tmpSuppliesId']=md5($tmp_id1);
}

$tmpSuppliesId = $_SESSION['tmpSuppliesId'];
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $my_tools->title();?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php include('css.php');?> 
  <script type="text/javascript" src="typeahead.js"></script>
   <!-- DataTables -->
  <link rel="stylesheet" href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">  
  <script>
  function getItemList(val) {
         $.ajax({
         type: "POST",
         url: "get_item_list.php",
         data:'categoryId='+val,
         success: function(data){
             $("#item-list").html(data);
        }
         });
     }

  </script>
  <script type="text/javascript">
      $(document).ready(function(){
      $('#itemSearch').typeahead({
        source: function (query, result) {
          $.ajax({
            url: "get_autocomplete_item_list.php",
            data: 'search=' + query,            
            dataType: "json",
            type: "POST",
              success: function (data) {
                result($.map(data, function (item) {
                return item;
                }));
              }
            });
          }
        });
      });
  </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php $my_tools->logoArea();?>
    
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <?php include('messages.php');?>
          <!-- Notifications: style can be found in dropdown.less -->
          <?php include('notifications.php');?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php $my_tools->adminImage();?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php $my_tools->adminFullname();?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php $my_tools->adminImage();?>" class="img-circle" alt="User Image">

                <p>
                  <?php $my_tools->adminFullname();?>
                </p>
              </li>
                  <?php $my_tools->profileLink();?>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <?php $my_tools->control_sidebar();?>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php $my_tools->adminImage();?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php $my_tools->adminFullname();?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <?php include('search_form.php');?>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <?php $my_tools->adminMenu();?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">

          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Stock Supply</h3>

              <div class="box-tools pull-right">
                
                <div class="btn-group">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-5 col-md-offset-2">
                   <center>
                   <form action="" method="POST">
                     
                    <input type="text" autofocus id="itemSearch" name="ItemName" class="form-control" placeholder="Write Item Name Here and press Enter Key" value="<?php if(isset($_POST['ItemName'])){ echo $_POST['ItemName'];}?>" >
                   
                   </form>
                    <?php 
                      if(isset($_POST['ItemName'])){
                    ?>

                   <form action="save_item_tmpsupplies.php" method="POST">
                      <hr>
                      <div class="row">
                          <?php
                            $results = $db_handle->getItemDetailsByName($_POST['ItemName']);
                            $i=0;
                            $trow=count($results);
                            if($trow>0){
                             foreach($results as $dataArr) {
                          ?>
                        <div class="col-md-6">
                          <input name="_MSBtoken" hidden value="<?php echo md5(rand(14446,50200));?>" >
                          <input type="text" disabled class="form-control" name="" value="<?php echo $dataArr['itemName'];?>" placeholder="">
                          <input  value="<?php echo $dataArr['id'];?>" name="itemId" hidden >
                        </div>
                        <div class="col-md-6">
                          <input type="text" disabled class="form-control" value="<?php echo $dataArr['categoryName'];?>">
                        </div>
                        <div class="col-md-6">
                          <input type="number" autofocus step="any" class="form-control" min="0" name="purchasePrice" required placeholder="Purchase Price">
                        </div>
                        <div class="col-md-6">
                          <input type="number" step="any" class="form-control" min="0" name="quantity" required placeholder="Quantity">
                        </div>

                        <div class="col-md-12">
                        <hr>
                        
                          <input type="submit" class="btn btn-success" value="Add To List">
                        </div>

                        <?php } } ?>
                      </div>
                   
                   </form>
                    <?php } ?>
                   </center>
                </div>
                <div class="col-md-3">
                   <center>
                    <?php 
                      if(!isset($_POST['ItemName'])){
                    ?>
                      <button type="button"  data-toggle="modal" data-target="#add_item_by_category" class="btn btn-success btn-sm" ><i class="fa fa-plus"></i> Add Item By Category</button>
                      <?php } ?>
                   </center>
                </div>
              </div>
              
              <div class="table-responsive">
               
              <hr>
               <table id="example1" class="table table-hover table-bordered table-striped table-responsive" style="font-size:12px;">
                <thead>
                <tr>
                  <th width="5%">Serial</th>
                  <th>Item Name</th>
                  <th>Item Category</th>
                  <th>Purchase Price</th>
                  <th>Quantity</th>
                  <th>Item Type</th>
                  <th width="7%">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  $results = $db_handle->getTmpSuppliesList($tmpSuppliesId);
                  $i=0;
                  $trow=count($results);
                  if($trow>0){
                   foreach($results as $dataArr) {
                    ++$i;
                ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo ($dataArr["itemName"]); ?></td>
                  <td><?php echo ($dataArr["categoryName"]); ?></td>
                  <td><?php echo CURRENCY1.($dataArr["purchasePrice"]).CURRENCY; ?></td>
                  <td><?php echo ($dataArr["quantity"]); ?></td>
                  <td>
                    <?php 
                      if(($dataArr["itemType"])==1){
                        echo "<span class='label bg-purple' style='font-size:12px'>Finish Item</span>";
                      }elseif (($dataArr["itemType"])==0) {
                        echo "<span class='label label-primary' style='font-size:12px'> Raw Item</span>";
                      }else{
                        echo "";
                        } 
                    ?>
                  </td>
                  <td>
                     <a href="delete_supplies_item.php?id=<?php echo base64_encode($dataArr['id']); ?>" class="btn btn-danger btn-xs"  onclick="return confirm('Are you sure to delete?')"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>

                <?php } } ?>
              </tbody>
              </table>

            </div>

            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              <div class="row">
              <form action="save_new_supply.php" method="POST">
                
                <div class="col-md-5 col-md-offset-2">
                  <select name="supplierId" required class="form-control">
                    <option value="">Select Supplier </option> 
                     <?php
                      $results = $db_handle->getSupplierList();
                      $i=0;
                      $trow=count($results);
                      if($trow>0){
                       foreach($results as $dataArr) {
                    ?>
                    <option value="<?php echo $dataArr['id'];?>"><?php echo $dataArr['supplierName'];?></option>
                  <?php } } ?>
                  </select>
                </div>
                <div class="col-md-3">
                  <input type="submit" class="btn btn-success" value="Save Supplies Information">
                </div>

              </form>
              </div>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $my_tools->myFooter(); ?>

  <div class="modal fade" id="add_item_by_category">
    <div class="modal-dialog">
      <div class="modal-content">
       <form action="save_item_tmpsupplies.php" method="POST">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="fa fa-cubes"></i> New Supplies Item </h4>
        </div>
        <div class="modal-body">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-server"></i></span>
            <input name="_MSBtoken" hidden value="<?php echo md5(rand(14446,50200));?>" >
             <select class="form-control" name="categoryId" required onChange="getItemList(this.value);" >
               <option value=""> Select Category</option>
                                       
             <?php
                  $results = $db_handle->getItemCategoryList();
                  $i=0;
                  $trow=count($results);
                  if($trow>0){
                   foreach($results as $dataArr) {
                     echo "<option value='".$dataArr['id']."'>".$dataArr['categoryName']."</option>";
                   }
                 }
                ?>
            </select>
          </div>
          <br>

          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-archive"></i></span>
            <select id="item-list"  class="form-control" name="itemId"  onclick="getProductQuantity(this.value);" onChange="getProductPrice(this.value);"  required  class="select2me form-control"></select>
          </div>
          <br>

          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-money"></i></span>
            <input type="number" step="any" required id="purchasePrice" name="purchasePrice" class="form-control" placeholder="Purchase Price ">
          </div>
          <br>

          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-signal"></i></span>
            <input type="number" step="any" required id="quantity" name="quantity" class="form-control" placeholder="Quantity amount ">
          </div>
          <br>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success pull-left"><i class="fa fa-save"></i> Add Item</button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
        </div>
         
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

</div>
<!-- ./wrapper -->
<?php include('js.php');?>
<!-- DataTables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $('#example1').DataTable()

  })
</script>
</body>
</html>
