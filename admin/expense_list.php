<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $my_tools->title();?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php include('css.php');?> 
  <!-- DataTables -->
  <link rel="stylesheet" href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php $my_tools->logoArea();?>
    
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <?php include('messages.php');?>
          <!-- Notifications: style can be found in dropdown.less -->
          <?php include('notifications.php');?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php $my_tools->adminImage();?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php $my_tools->adminFullname();?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php $my_tools->adminImage();?>" class="img-circle" alt="User Image">

                <p>
                  <?php $my_tools->adminFullname();?>
                </p>
              </li>
                  <?php $my_tools->profileLink();?>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <?php $my_tools->control_sidebar();?>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php $my_tools->adminImage();?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php $my_tools->adminFullname();?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <?php include('search_form.php');?>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <?php $my_tools->adminMenu();?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <?php if(isset($_GET['sst'])){ ?>
           <div class="alert alert-success alert-dismissible fade in" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong> Expense information has been successfully <?php echo $_GET['smsg'];?>.</strong>
            </div>
            <div class="clearfix"></div>
           <?php } ?>

          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Expense list</h3>

              <div class="box-tools pull-right">
                
                <div class="btn-group">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
               <table id="example1" class="table table-hover table-bordered table-striped table-responsive">
                <thead>
                <tr>
                  <th width="5%">Serial</th>
                  <th>Expense Name</th>
                  <th>Expense Category</th>
                  <th>Expense Amount</th>
                  <th>Expense Date</th>
                  <th width="10%">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  $results = $db_handle->getExpenseList();
                  $i=0;
                  $trow=count($results);
                  if($trow>0){
                   foreach($results as $dataArr) {
                    ++$i;
                ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo ($dataArr["name"]); ?></td>
                  <td><?php echo ($dataArr["categoryName"]); ?></td>
                  <td><?php echo CURRENCY1.($dataArr["amount"]).CURRENCY; ?></td>
                  <td><?php echo ($dataArr["expenseDate"]); ?></td>
                  <td>
                        <a href="#" data-toggle="modal" data-target="#view<?php echo $i; ?>" class="btn btn-success btn-sm" title="View Details" ><i class="fa fa-eye"></i></a>
                        <a href="edit_expense.php?id=<?php echo base64_encode($dataArr['id']); ?>"  title="Edit Details" class="btn btn-primary btn-sm" ><i class="fa fa-edit"></i></a>
                        <!-- <a href="delete_expense.php?id=<?php echo base64_encode($dataArr['id']); ?>" onclick="return confirm('Are you sure to delete?')"  class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i></a> -->
                  </td>
                </tr>

                  <div class="modal fade" id="view<?php echo $i; ?>">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title"><i class="fa fa-sticky-note"></i> View Expense Details</h4>
                        </div>
                        <div class="modal-body">
                          <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" disabled required id="name" class="form-control" value="<?php echo ($dataArr["name"]); ?>" >
                          </div>

                          <div class="form-group">
                            <label for="categoryName">Category Name</label>
                            <input type="text" disabled required id="categoryName" class="form-control" value="<?php echo ($dataArr["categoryName"]); ?>" >
                          </div>

                          <div class="form-group">
                            <label for="expenseAmount">Expense Amount</label>
                            <input type="text" disabled required id="expenseAmount" class="form-control" value="<?php echo CURRENCY1.($dataArr["amount"]).CURRENCY; ?>" >
                          </div>

                          <div class="form-group">
                            <label for="expenseDate">Expense Date</label>
                            <input type="text" disabled required id="expenseDate" class="form-control" value="<?php echo ($dataArr["expenseDate"]); ?>" >
                          </div>

                          <div class="form-group">
                            <label for="reference">Expense Reference</label>
                            <textarea disabled required id="reference" class="form-control" ><?php echo ($dataArr["reference"]); ?></textarea>
                          </div>

                          <div class="form-group">
                            <label for="description">Expense Description</label>
                            <textarea disabled required id="description" class="form-control" ><?php echo ($dataArr["description"]); ?></textarea>
                          </div>

                          <div class="form-group">
                            <label for="attachment">Expense Attachment</label><br>
                            <?php if($dataArr["attachment"]!=""){ ?>
                              <a target="_NEW" href="../expense_attachment/<?php echo $dataArr["attachment"]; ?>" class="btn btn-success"><i class="fa fa-download"></i> Download</a>

                              <?php }else{ echo "<span class='label label-danger' style='font-size:12px;'>No Attachment.</span>";} ?>
                          </div>
                          <br>
                          <div class="form-group">
                            <label for="createdDateTime">Created DateTime</label>
                            <input disabled required id="createdDateTime" value="<?php echo ($dataArr["createdDateTime"]); ?>" class="form-control" >
                          </div>

                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                        </div>
                         
                        </form>
                      </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                  </div>
                  <!-- /.modal -->

                <?php } } ?>
              </tbody>
              </table>

            </div>

            </div>
            <!-- ./box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <?php $my_tools->myFooter(); ?>

</div>
<!-- ./wrapper -->
<?php include('js.php');?>
<!-- DataTables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $('#example1').DataTable()

  })
</script>
</body>
</html>
