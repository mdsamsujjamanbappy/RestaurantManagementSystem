<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $my_tools->title();?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php include('css.php'); ?> 
  <!-- DataTables -->
  <link rel="stylesheet" href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">  
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="../vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php $my_tools->logoArea();?>
    
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <?php include('messages.php');?>
          <!-- Notifications: style can be found in dropdown.less -->
          <?php include('notifications.php');?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php $my_tools->adminImage();?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php $my_tools->adminFullname();?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php $my_tools->adminImage();?>" class="img-circle" alt="User Image">

                <p>
                  <?php $my_tools->adminFullname();?>
                </p>
              </li>
                  <?php $my_tools->profileLink();?>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <?php $my_tools->control_sidebar();?>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php $my_tools->adminImage();?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php $my_tools->adminFullname();?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <?php include('search_form.php');?>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <?php $my_tools->adminMenu();?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6 col-md-offset-2">
          <div class="box box-success">
           
            <!-- /.box-header -->
            <div class="box-body">
            <div class="row">
              <form action="" method="POST" >
                <div class="col-md-5">
                    <div class="form-group">
                      <input name="sdate" style="text-align:center;" value="<?php if(isset($_POST['sdate'])){ echo $_POST['sdate']; }else{ echo date('Y-m'); } ?>" required id="sdate" class="form-control" placeholder="Insert Month"> 
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                      <label for=""></label>
                      <button type="submit" class="btn btn-success"> <i class="fa fa-search"></i> Search </button>
                    </div>
                </div>
              </form>

            </div>

            </div>
          </div>
        </div>
        <?php if(isset($_POST['sdate'])){ ?>
        <div class="col-md-12 ">
          <div class="box box-success animated zoomIn">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-money"></i> Monthly Basis Employee Salary Report (<?php echo date("F", strtotime($_POST['sdate']))." ".date("Y", strtotime($_POST['sdate'])); ?>) </h3>

              <div class="box-tools pull-right">
                
                <div class="btn-group">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
               <table id="example1" class="table table-hover table-bordered table-striped table-responsive">
                <thead>
                <tr>
                  <th width="5%">Serial</th>
                  <th>Employee Name</th>
                  <th>Phone</th>
                  <th>Designation</th>
                  <th>Salary Amount</th>
                  <th>Salary Date</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  $date = $_POST['sdate'];
                  $year = date("Y", strtotime($date));
                  $month = date("m", strtotime($date));
                  $results = $db_handle->getMonthlyBasisEmployeeSalaryReport($month,$year);
                  $i=0;
                  $totAmnt=0;
                  $trow=count($results);
                  if($trow>0){
                   foreach($results as $dataArr) {
                    ++$i;
                    $totAmnt+=($dataArr["amount"]);
                ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo ($dataArr["empName"]); ?></td>
                  <td><?php echo ($dataArr["empPhone"]); ?></td>
                  <td><?php echo ($dataArr["designationName"]); ?></td>
                  <td><?php echo CURRENCY1.($dataArr["amount"]).CURRENCY; ?></td>
                  <td><?php echo ($dataArr["sdate"]); ?></td>
                </tr>

                <?php } } ?>
              </tbody>
              <tfoot>
                <tr>
                  <th></th>
                  <th colspan="3" style="text-align:right;">Total Amount</th>
                  <th><b><?php echo CURRENCY1.($totAmnt).CURRENCY; ?></b></th>
                </tr>
              </tfoot>
              </table>

            </div>

            </div>
            <!-- ./box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>

        <?php } ?>
        <!-- /.col -->
      </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $my_tools->myFooter(); ?>

</div>
<!-- ./wrapper -->
<?php include('js.php');?>
<!-- DataTables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- date-range-picker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<!-- bootstrap datepicker -->
<script src="../vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
<script>
  $(function () {
    $('#example1').DataTable()

    $("#sdate").datepicker( {
    format: "yyyy-mm",
    startView: "months", 
    minViewMode: "months"
    });

  })
</script>
</body>
</html>
