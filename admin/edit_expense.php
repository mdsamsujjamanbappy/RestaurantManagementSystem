<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $my_tools->title();?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php include('css.php');?> 
  <!-- Select2 -->
  <link rel="stylesheet" href="../vendors/select2/dist/css/select2.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="../vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
 </head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php $my_tools->logoArea();?>
    
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <?php include('messages.php');?>
          <!-- Notifications: style can be found in dropdown.less -->
          <?php include('notifications.php');?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php $my_tools->adminImage();?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php $my_tools->adminFullname();?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php $my_tools->adminImage();?>" class="img-circle" alt="User Image">

                <p>
                  <?php $my_tools->adminFullname();?>
                </p>
              </li>
                  <?php $my_tools->profileLink();?>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <?php $my_tools->control_sidebar();?>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php $my_tools->adminImage();?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php $my_tools->adminFullname();?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <?php include('search_form.php');?>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <?php $my_tools->adminMenu();?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">

          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Expense Information</h3>

              <div class="box-tools pull-right">
                
                <div class="btn-group">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="update_expense.php" method="POST" enctype="multipart/form-data">
              <br>
              <div class="row">
                <div class="col-md-5">

                 <?php
                  $id = base64_decode($_GET['id']);
                  $results = $db_handle->getExpenseDetailsById($id);
                  $i=0;
                  $trow=count($results);
                  if($trow>0){
                   foreach($results as $dataArr) {
                    ++$i;
                ?>
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input name="id" hidden value="<?php echo $dataArr['id'];?>" >
                    <input name="_MSBtoken" hidden value="<?php echo md5(rand(14446,50200));?>" >
                    <input type="text" autofocus class="form-control" required id="name" value="<?php echo $dataArr['name'];?>" name="name" placeholder="Expense Name" >
                  </div> 

                  <div class="form-group">
                    <label for="expenseAmount">Expense Amount</label>
                    <input type="number" step="any" required id="expenseAmount" class="form-control" name="expenseAmount" value="<?php echo $dataArr['amount'];?>"  placeholder="Expense Amount" >
                  </div>

                  <div class="form-group">
                    <label for="expenseDate">Expense Date</label>
                    <input type="text" id="expenseDate" required class="form-control" name="expenseDate" value="<?php echo $dataArr['expenseDate'];?>"  >
                  </div>

                  <div class="form-group">
                    <label for="description">Expense Description</label>
                    <textarea id="description" name="description" class="form-control" placeholder="Expense Description"  ><?php echo $dataArr['description'];?></textarea>
                  </div>

                </div>
                
                <div class="col-md-5 col-md-offset-1">

                  <div class="form-group">
                    <label for="categoryId">Expense Category </label>
                    <select name="categoryId" required class="select2 form-control" style="width:100%" id="categoryId" >
                    <option value=""> Select Expense Category </option>
                      <?php
                       $results2 = $db_handle->getExpenseCategoryList();
                       foreach($results2 as $dataArr2) {
                        ?>
                           <option <?php if($dataArr2['id']==$dataArr['categoryId']){echo "selected";}?> value="<?php echo $dataArr2['id']; ?>"><?php echo $dataArr2['categoryName']; ?></option>
                        <?php } ?>
                   </select>
                  </div>

                  <div class="form-group">
                    <label for="reference">Expense Reference</label>
                    <textarea  id="reference" name="reference" class="form-control"  placeholder="Expense Reference"  ><?php echo $dataArr['reference'];?></textarea>
                  </div>
   

                  <div class="form-group">
                    <label for="attachment">Expense Attachment </label>
                      <br>
                        <?php if($dataArr["attachment"]!=""){ ?>
                          <a target="_NEW" href="../expense_attachment/<?php echo $dataArr["attachment"]; ?>" class="btn btn-default"><i class="fa fa-download"></i> Previous Attachment</a>

                          <?php }else{ echo "<span class='label label-danger' style='font-size:12px;'>No Attachment.</span>";} ?>
                      <br >
                      <br >
                    <input  name="preattachment" value="<?php echo $dataArr["attachment"]; ?>" hidden >
                    <input type="file" id="attachment" name="attachment" class="form-control btn-success">
                  </div>

                </div>
              </div>
                <hr>
                  <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Update Information</button>
                  &nbsp;&nbsp;
                  <button type="reset" class="btn btn-default"><i class="fa fa-undo"></i> Reset</button> 
                  &nbsp;&nbsp;
                  <a href="expense_list.php" class="btn btn-primary"><i class="fa fa-th"></i> Expense List</a>
              <?php } } ?>
             </form>
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $my_tools->myFooter(); ?>

</div>
<!-- ./wrapper -->
<?php include('js.php');?>
<!-- date-range-picker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<!-- bootstrap datepicker -->
<script src="../vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
<!-- Select2 -->
<script src="../vendors/select2/dist/js/select2.full.min.js"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Date picker
    $('#expenseDate').datepicker({
      autoclose: true
    })

  })
</script>
</body>
</html>
