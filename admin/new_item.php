<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $my_tools->title();?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php include('css.php');?> 
  <!-- Select2 -->
  <link rel="stylesheet" href="../vendors/select2/dist/css/select2.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="../vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
 </head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php $my_tools->logoArea();?>
    
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <?php include('messages.php');?>
          <!-- Notifications: style can be found in dropdown.less -->
          <?php include('notifications.php');?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php $my_tools->adminImage();?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php $my_tools->adminFullname();?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php $my_tools->adminImage();?>" class="img-circle" alt="User Image">

                <p>
                  <?php $my_tools->adminFullname();?>
                </p>
              </li>
                  <?php $my_tools->profileLink();?>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <?php $my_tools->control_sidebar();?>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php $my_tools->adminImage();?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php $my_tools->adminFullname();?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <?php include('search_form.php');?>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <?php $my_tools->adminMenu();?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">

          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">New Item Information</h3>

              <div class="box-tools pull-right">
                
                <div class="btn-group">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="save_item_information.php" method="POST" enctype="multipart/form-data">
              <br>
              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    <label for="itemName">Item Name</label>
                    <input name="_MSBtoken" hidden value="<?php echo md5(rand(14446,50200));?>">
                    <input type="text" class="form-control" autofocus id="itemName" name="itemName" placeholder="Insert Item name " required >
                  </div>
                  <div class="form-group">
                    <label for="itemCode">Item Code</label>
                    <input type="text" id="itemCode" class="form-control" name="itemCode" placeholder="Insert Item Code " required >
                  </div>
                  <div class="form-group">
                    <label for="itemSellPrice">Item Selling Price </label>
                    <input type="number" step="any" min="0" id="itemSellPrice" class="form-control" name="itemSellPrice" placeholder="Insert Item Selling Price " required >
                  </div>
                  <div class="form-group">
                    <label for="itemStockManagement">Item Stock Management </label>
                    <select name="itemStockManagement" id="itemStockManagement" required class="form-control">
                      <option value="">-- Select --</option>
                      <option value="1">Enable</option>
                      <option value="0">Disable</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="itemQuantity">Item Quantity </label>
                    <input type="number" step="any" id="itemQuantity" class="form-control" name="itemQuantity" min="0" placeholder="Insert Item Quantity" required >
                  </div>

                  <div class="form-group">
                    <label for="itemStockAlert">Stock Alert  </label>
                    <select name="itemStockAlert" required class="form-control">
                      <option selected value="1">Enable</option>
                      <option value="0">Disable</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="itemDescription">Item Description </label>
                    <textarea id="itemDescription" class="form-control" name="itemDescription" placeholder="Insert Item Description"  ></textarea>
                  </div>
                </div>
                
                <div class="col-md-5 col-md-offset-1">
                  <div class="form-group">
                    <label for="itemCategoryId">Item Category </label>
                    <select name="itemCategoryId" required class="select2 form-control" style="width:100%" id="itemCategoryId" >
                    <option value=""> Select Item Category</option>
                      <?php
                       $results1 = $db_handle->getItemCategoryList();
                       foreach($results1 as $dataArr1) {
                        ?>
                           <option value="<?php echo $dataArr1['id']; ?>"><?php echo $dataArr1['categoryName']; ?></option>
                        <?php } ?>
                   </select>
                  </div>

                  <div class="form-group">
                    <label for="itemDepartmentId">Item Department </label>
                    <select name="itemDepartmentId" class="select2 form-control" style="width:100%" id="itemDepartmentId" >
                    <option value=""> Select Item Department</option>
                      <?php
                       $results2 = $db_handle->getItemDepartmentList();
                       foreach($results2 as $dataArr2) {
                        ?>
                           <option value="<?php echo $dataArr2['id']; ?>"><?php echo $dataArr2['departmentName']; ?></option>
                        <?php } ?>
                   </select>
                  </div>

                  <div class="form-group">
                    <label for="itemType">Item Type </label>
                    <select name="itemType" required class="select2 form-control" style="width:100%" id="itemDepartmentId" >
                      <option value="">-- Select --</option>
                      <option value="0">Raw Item</option>
                      <option value="1">Finish Item</option>
                   </select>
                  </div>

                  <div class="form-group">
                    <label for="itemExpireDate">Item Expiration Date </label>
                    <input type="text" id="itemExpireDate" class="form-control" name="itemExpireDate" placeholder="Insert Item Selling Price " required >
                  </div>

                  <div class="form-group">
                    <label for="itemExpireDateAction">Action in the event of expiry  </label>
                    <select name="itemExpireDateAction" class="form-control">
                      <option value="">-- Select --</option>
                      <option value="1">Allow Sale</option>
                      <option value="0">Prevent Sale</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="itemPhoto">Item Photo </label>
                    <div id="imagePreview"></div>
                    <input type="file" id="itemPhoto" onchange="return fileValidation()" name="itemPhoto" accept="image/*" class="form-control btn-success">
                  </div>

                </div>
              </div>
                <hr>
                  <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save Item Information</button>
                  &nbsp;&nbsp;
                  <button type="reset" class="btn btn-default"><i class="fa fa-undo"></i> Reset</button>
                    
                  </form>
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $my_tools->myFooter(); ?>

</div>
<!-- ./wrapper -->
<?php include('js.php');?>
<!-- date-range-picker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<!-- bootstrap datepicker -->
<script src="../vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
<!-- Select2 -->
<script src="../vendors/select2/dist/js/select2.full.min.js"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Date picker
    $('#itemExpireDate').datepicker({
      startDate: new Date(),
      autoclose: true
    })

  })
</script>
<script>
  document.getElementById('itemStockManagement').addEventListener('change', function() {
    if (this.value == 1) {
        document.getElementById('itemQuantity').disabled = false;
    } else {
        document.getElementById('itemQuantity').disabled = true;
    }
  });

  function fileValidation(){
    var fileInput = document.getElementById('itemPhoto');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    if(!allowedExtensions.exec(filePath)){
        alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
        fileInput.value = '';
        return false;
    }else{
        //Image preview
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                document.getElementById('imagePreview').innerHTML = '<img  width="200px" src="'+e.target.result+'"/><br>';
            };
            reader.readAsDataURL(fileInput.files[0]);
        }
    }
  }
</script>
</body>
</html>
