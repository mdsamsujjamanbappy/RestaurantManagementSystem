<?php
require_once 'header_link.php';

date_default_timezone_set('Asia/Dhaka');
$date = date('Y-m-d');
$time = date("h:i:sa");
$results = $db_handle->insertActivityLog($_SESSION['adminRMSId'],"Sign out", $date, $time);

unset($_SESSION['adminRMSAccess']);
unset($_SESSION['adminRMSId']);
unset($_SESSION['adminRMSfname']);
unset($_SESSION['adminRMSImage']);
echo "<script> document.location.href='../index.php';</script>";
?>