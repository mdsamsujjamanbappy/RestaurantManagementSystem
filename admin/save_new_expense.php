<?php
require_once 'header_link.php';

if(isset($_POST['_MSBtoken'])){

	$name = $_POST['name'];
	$expenseAmount = $_POST['expenseAmount'];
	$expenseDate = $_POST['expenseDate'];
	$description = $_POST['description'];
	$categoryId = $_POST['categoryId'];
	$reference = $_POST['reference'];
	$attachment="";

	if(!empty($_FILES['attachment']['name'])){
      if(($_FILES['attachment']['size']) <= (5000*1024))
      {

    	  $maxrow= $db_handle->getTotalRowNumber("tbexpenselist");

          $picture_tmp = $_FILES['attachment']['tmp_name'];
          $picture_name = $_FILES['attachment']['name'];
          $picture_type = $_FILES['attachment']['type'];
         
          $arr1 = explode(".", $picture_tmp);
          $extension1 = strtolower(array_pop($arr1));  
          
          $arr = explode(".", $picture_name);
          $extension = strtolower(array_pop($arr));

          $attachment="expense".date('Ymdhs')."_".$maxrow.".".$extension;
          $newfilename1="expense".date('Ymdhs')."_".$maxrow.".".$extension1;
          $path = '../expense_attachment/'.$attachment;

          move_uploaded_file($picture_tmp, $path);
	    }
	  }

	$db_handle->saveExpenseInformation($name,$expenseAmount,$expenseDate,$description,$categoryId,$reference,$attachment);

	echo "<script>document.location.href='expense_list.php?sst=success&&smsg=added';</script>";
}
?>