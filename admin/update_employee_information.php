<?php
require_once 'header_link.php';

if(isset($_POST['_MSBtoken'])){

	$id = $_POST['id'];
	$empName = $_POST['empName'];
	$empPhone = $_POST['empPhone'];
	$empEmail = $_POST['empEmail'];
	$empJoiningDate = $_POST['empJoiningDate'];
	$empAddress = $_POST['empAddress'];
	$description = $_POST['description'];
	$empDesignationId = $_POST['empDesignationId'];
	$empSalaryType = $_POST['empSalaryType'];
	$salaryAmount = $_POST['salaryAmount'];
	$empAccountStatus = $_POST['empAccountStatus'];
	$empGender = $_POST['empGender'];
	$image = $_POST['empPreImage'];

	if(isset($_FILES['empImage']['name'])){
      if(($_FILES['empImage']['size']) <= (5000*1024))
      {		
      	if($image != "default.jpg"){
          $url = '../employee_images/'.$image;
          unlink($url);
      	}

    	  $maxrow= $db_handle->getTotalRowNumber("tbemployeelist");
    	  $maxrow++;
          $picture_tmp = $_FILES['empImage']['tmp_name'];
          $picture_name = $_FILES['empImage']['name'];
          $picture_type = $_FILES['empImage']['type'];
         
          $arr1 = explode(".", $picture_tmp);
          $extension1 = strtolower(array_pop($arr1));  
          
          $arr = explode(".", $picture_name);
          $extension = strtolower(array_pop($arr));

          $image="employee".date('Ymdhs')."_".$maxrow.".".$extension;
          $newfilename1="employee".date('Ymdhs')."_".$maxrow.".".$extension1;
          $path = '../employee_images/'.$image;

          move_uploaded_file($picture_tmp, $path);
	    }
	  }

	$db_handle->updateEmployeeInformation($id,$empName,$empPhone,$empEmail,$empJoiningDate,$empAddress,$description,$empDesignationId,$empSalaryType,$salaryAmount,$empAccountStatus,$empGender,$image);

	echo "<script>document.location.href='employee_list.php?sst=success&&smsg=updated';</script>";
}
?>