<?php require_once 'header_link.php'; ?>
<?php include('css.php');?> 
<!-- Select2 -->
<link rel="stylesheet" href="../vendors/select2/dist/css/select2.css">
<!-- DataTables -->
<link rel="stylesheet" href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">  
<title>Point of Sale</title>
<!-- Main content -->
<div class="container-fluid">
  
  <script>
  function getItemList(val) {
         $.ajax({
         type: "POST",
         url: "get_item_list.php",
         data:'categoryId='+val,
         success: function(data){
             $("#item-list").html(data);
        }
         });
     }

  </script>
<section class="content">
<div class="row">
  <div class="col-md-12">
    <div class="pull-left">
        <a href="#" title="" class="btn btn-default"><i class="fa fa-dashboard"></i> Dashboard</a>
    </div>
    <div class="pull-right">
        <a href="#" title="" class="btn btn-success"><i class="fa fa-history"></i> Hold Orders</a>
        <a href="logout.php" title="" class="btn btn-danger"><i class="fa fa-sign-out"></i> Logout</a>
    </div>
  </div>
</div>

<br>

<div class="row">
  <div class="col-md-6">
    <!-- Default box -->
    <div class="box box-danger">
      <div class="box-header with-border">
      <div class="row">
        <div class="col-md-9">
          <div>
            <select name="customerId" class="select2 form-control" style="min-width:100%">
              <option value=""> Select Specific Customer</option>
              <?php
               $results2 = $db_handle->getDesignationList();
               foreach($results2 as $dataArr2) {
                ?>
                <option value="<?php echo $dataArr2['id']; ?>"><?php echo $dataArr2['designationName']; ?></option>
                <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-md-3">
              <a href="#" title="" class="btn btn-default pull-right"><i class="fa fa-user-plus"></i> Add New Customer</a>
          
        </div>
      </div>
      </div>
      <div class="box-body" style="min-height:60%">
        <table class="table table-responsive table-hover table-bordered" style="font-size:12px;">
          <thead>
            <tr>
              <th>Item</th>
              <th width="15%"><center>Unit Price</center></th>
              <th width="15%"><center>Quantity</center></th>
              <th width="15%"><center>Total</center></th>
              <th width="8%">#</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Fried Chicken</td>
              <td><center>৳ 20</center></td>
              <td>
                <center>2</center>
              </td>
              <td><center>৳ 40</center></td>
              <td>
                <a href="#" title="" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
              </td>
            </tr>
            <tr>
              <td>Sweet & Spicy Nuts</td>
              <td><center>৳ 480</center></td>
              <td>
                <center>1</center>
              </td>
              <td><center>৳ 480</center></td>
              <td>
                <a href="#" title="" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
              </td>
            </tr>
            <tr>
              <td>Sub Pizza</td>
              <td><center>৳ 750</center></td>
              <td>
                <center>2</center>
              </td>
              <td><center>৳ 1500</center></td>
              <td>
                <a href="#" title="" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
              </td>
            </tr>
            <tr>
              <td>Lemon Drop Martini</td>
              <td><center>৳ 90</center></td>
              <td>
                <center>2</center>
              </td>
              <td><center>৳ 180</center></td>
              <td>
                <a href="#" title="" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <div>
          <div class="pull-left">
              <a href="#" title="" class="btn btn-default"><i class="fa fa-th"></i> Dine In</a>
              <a href="#" title="" class="btn btn-success"><i class="fa fa-hand-stop-o"></i> Hold Order</a>
          </div>
          <div class="pull-right">
              <a href="#" title="" class="btn btn-primary"><i class="fa fa-cart-plus"></i> Checkout</a>
          </div>
        </div>        
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->
  </div>
  <div class="col-md-6">
    <!-- Default box -->
    <div class="box box-danger">
      <div class="box-header with-border">
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-6">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-server"></i></span>
              <input name="_MSBtoken" hidden value="<?php echo md5(rand(14446,50200));?>" >
               <select class="form-control" name="categoryId" required onChange="getItemList(this.value);" >
                 <option value=""> Select Category</option>
                                         
               <?php
                    $results = $db_handle->getItemCategoryList();
                    $i=0;
                    $trow=count($results);
                    if($trow>0){
                     foreach($results as $dataArr) {
                       echo "<option value='".$dataArr['id']."'>".$dataArr['categoryName']."</option>";
                     }
                   }
                  ?>
              </select>
            </div>
            <br>

            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-archive"></i></span>
              <select id="item-list"  class="form-control" name="itemId"  onclick="getProductQuantity(this.value);" onChange="getProductPrice(this.value);"  required  class="select2me form-control"></select>
            </div>
            
          </div>
          <div class="col-md-6">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-money"></i></span>
              <input type="number" name="price" step="any" placeholder="Item price will be appear here" readonly min="0" class="form-control" >
            </div>
            <br>
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-signal"></i></span>
              <input type="number" name="quantity" min="1" step="any" placeholder="Quantity Amount" required class="form-control" >
            </div>
            <br>
           
          </div>
          
            <div class="form-group">
              <center><button type="submit" class="btn btn-success btn-sm"><i class="fa fa-cart-plus"></i> Add to List</button></center>
            </div>
        </div>
        <div>
          <div class="col-md-12">
            <hr>
            <table id="example1" class="table table-hover table-bordered table-striped table-responsive" style='font-size:12px'>
              <thead>
              <tr>
                <th>Item Name</th>
                <th>Category</th>
                <th>Sell Price</th>
                <th width="15%">#</th>
              </tr>
              </thead>
              <tbody>
              <?php
                $results = $db_handle->getItemList();
                $trow=count($results);
                if($trow>0){
                 foreach($results as $dataArr) {
                  ++$i;
              ?>
              <tr>
                <td><?php echo ($dataArr["itemName"]); ?></td>
                <td><?php echo ($dataArr["categoryName"]); ?></td>
                <td><?php echo CURRENCY1.($dataArr["itemSellPrice"]).CURRENCY; ?></td>

                <td></td>
              </tr>
              <?php } } ?>
            </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->
  </div>
</div>
</section>
<footer style="padding:2px 20px;">
<hr>
  <div class="pull-right hidden-xs">
    <b>Version</b> 1.0.0
  </div>
  <strong>Copyright &copy; <a href="https://feits.co">FEITS </a>.</strong> All rights
  reserved.
</footer>

</div>
<?php include('js.php');?>
<!-- DataTables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="../vendors/select2/dist/js/select2.full.min.js"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
    $('#example1').DataTable()

  })
</script>

