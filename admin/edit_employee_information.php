<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $my_tools->title();?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php include('css.php');?> 
  <!-- Select2 -->
  <link rel="stylesheet" href="../vendors/select2/dist/css/select2.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="../vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
 </head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php $my_tools->logoArea();?>
    
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <?php include('messages.php');?>
          <!-- Notifications: style can be found in dropdown.less -->
          <?php include('notifications.php');?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php $my_tools->adminImage();?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php $my_tools->adminFullname();?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php $my_tools->adminImage();?>" class="img-circle" alt="User Image">

                <p>
                  <?php $my_tools->adminFullname();?>
                </p>
              </li>
                  <?php $my_tools->profileLink();?>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <?php $my_tools->control_sidebar();?>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php $my_tools->adminImage();?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php $my_tools->adminFullname();?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <?php include('search_form.php');?>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <?php $my_tools->adminMenu();?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">

          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-user-plus"></i> New Employee Information</h3>

              <div class="box-tools pull-right">
                
                <div class="btn-group">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="update_employee_information.php" method="POST" enctype="multipart/form-data">

              <?php
                  $id=base64_decode($_GET['id']);
                  $results = $db_handle->getEmployeeDetailsById($id);
                  if(count($results)>0){
                   foreach($results as $dataArr) {
                ?>
              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    <label for="empName">Employee Name</label>
                    <input name="_MSBtoken" hidden value="<?php echo md5(rand(14446,50200));?>" >
                    <input name="id" hidden value="<?php echo $id;?>" >
                    <input type="text" value="<?php echo $dataArr['empName']; ?>" class="form-control" required id="empName" name="empName" placeholder="Employee Name" >
                  </div> 

                  <div class="form-group">
                    <label for="empPhone">Employee Phone</label>
                    <input type="text"  value="<?php echo $dataArr['empPhone']; ?>" id="empPhone" class="form-control" name="empPhone" placeholder="Employee Phone Number" >
                  </div>

                  <div class="form-group">
                    <label for="empEmail">Employee Email</label>
                    <input type="email" id="empEmail" class="form-control" name="empEmail" value="<?php echo $dataArr['empEmail']; ?>" placeholder="Employee Email Address">
                  </div>

                  <div class="form-group">
                    <label for="empGender">Employee Gender</label>
                    <select name="empGender" id="empGender" required class="form-control">
                      <option <?php if($dataArr['empGender']==""){ echo "selected"; } ?>  value=""> -- Select Gender --</option>
                      <option <?php if($dataArr['empGender']==1){ echo "selected"; } ?> value="1">Male</option>
                      <option <?php if($dataArr['empGender']==2){ echo "selected"; } ?> value="2">Female</option>
                      <option <?php if($dataArr['empGender']==3){ echo "selected"; } ?> value="3">Other</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="empJoiningDate">Employee Joining Date</label>
                    <input type="text" required id="empJoiningDate" class="form-control" name="empJoiningDate" value="<?php echo $dataArr['empJoiningDate']; ?>" placeholder="Employee Joining Date" >
                  </div>

                  <div class="form-group">
                    <label for="empAddress">Employee Address</label>
                    <textarea  id="empAddress" name="empAddress" class="form-control"  placeholder="Employee Address" ><?php echo $dataArr['empAddress']; ?></textarea>
                  </div>

                  <div class="form-group">
                    <label for="description">Description</label>
                    <textarea id="description" name="description" class="form-control" placeholder="Description write here "><?php echo $dataArr['empDescription']; ?></textarea>
                  </div>

                </div>
                
                <div class="col-md-5 col-md-offset-1">

                  <div class="form-group">
                    <label for="empDesignationId">Employee Designation </label>
                    <select name="empDesignationId" required class="form-control" style="width:100%" id="empDesignationId" >
                    <option value=""> Select Employee Designation </option>
                      <?php
                       $results2 = $db_handle->getDesignationList();
                       foreach($results2 as $dataArr2) {
                        ?>
                           <option <?php if($dataArr2['id']==$dataArr['empDesignationId']){ echo "selected";} ?> value="<?php echo $dataArr2['id']; ?>"><?php echo $dataArr2['designationName']; ?></option>
                        <?php } ?>
                   </select>
                  </div>

                  <div class="form-group">
                    <label for="empSalaryType">Employee Salary Type</label>
                    <select name="empSalaryType" required class="form-control">
                      <option value="">-- Select --</option>
                      <option <?php if($dataArr['empSalaryType']==0){ echo "selected"; } ?> value="0">Monthly Basis</option>
                      <option <?php if($dataArr['empSalaryType']==1){ echo "selected"; } ?> value="1">Daily Basis</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="salaryAmount">Employee Salary Amount </label>
                    <input type="number" step="any" required id="salaryAmount" class="form-control" value="<?php echo $dataArr['salaryAmount']; ?>" name="salaryAmount" min="0" placeholder="Employee Salary Amount " >
                  </div>

                  <div class="form-group">
                    <label for="empAccountStatus">Employee Account Status</label>
                    <select name="empAccountStatus" required class="form-control">
                      <option <?php if($dataArr['empAccountStatus']==1){ echo "selected"; } ?>  value="1">Enable</option>
                      <option  <?php if($dataArr['empAccountStatus']==0){ echo "selected"; } ?>  value="0">Disable</option>
                    </select>
                  </div>


                  <div class="form-group">
                    <label for="empImage">Employee Photo</label><br>
                    <img src="../employee_images/<?php echo $dataArr['empImage']; ?>" alt="<?php echo $dataArr['empName']; ?>" width="200px" class="">
                    <input name="empPreImage" hidden value="<?php echo $dataArr['empImage']; ?>" >
                    <input type="file" onchange="return fileValidation()" id="empImage" name="empImage" class="form-control btn-default">
                  </div>

                </div>

              </div>

              <?php } } ?>
                  <hr>
                  <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Update Information</button>
                  &nbsp;&nbsp;
                  <button type="reset" class="btn btn-default"><i class="fa fa-undo"></i> Reset</button>
              
              </form>
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $my_tools->myFooter(); ?>

</div>
<!-- ./wrapper -->
<?php include('js.php');?>
<!-- date-range-picker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<!-- bootstrap datepicker -->
<script src="../vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
<!-- Select2 -->
<script src="../vendors/select2/dist/js/select2.full.min.js"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Date picker
    $('#empJoiningDate').datepicker({
      autoclose: true
    })

  })
</script>
<script>
  
  function fileValidation(){
    var fileInput = document.getElementById('empImage');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    if(!allowedExtensions.exec(filePath)){
        alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
        fileInput.value = '';
        return false;
    }else{
        //Image preview
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                document.getElementById('imagePreview').innerHTML = '<img  width="200px" src="'+e.target.result+'"/><br>';
            };
            reader.readAsDataURL(fileInput.files[0]);
        }
    }
  }
</script>  
</body>
</html>
