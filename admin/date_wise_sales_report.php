<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $my_tools->title();?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php include('css.php'); ?> 
  <!-- DataTables -->
  <link rel="stylesheet" href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">  
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="../vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php $my_tools->logoArea();?>
    
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <?php include('messages.php');?>
          <!-- Notifications: style can be found in dropdown.less -->
          <?php include('notifications.php');?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php $my_tools->adminImage();?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php $my_tools->adminFullname();?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php $my_tools->adminImage();?>" class="img-circle" alt="User Image">

                <p>
                  <?php $my_tools->adminFullname();?>
                </p>
              </li>
                  <?php $my_tools->profileLink();?>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <?php $my_tools->control_sidebar();?>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php $my_tools->adminImage();?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php $my_tools->adminFullname();?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <?php include('search_form.php');?>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <?php $my_tools->adminMenu();?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="box box-success">
           
            <!-- /.box-header -->
            <div class="box-body">
            <div class="row">
              <form action="" method="POST" >
                <div class="col-md-4">
                    <div class="form-group">
                      <input name="fromDate" style="text-align:center;" value="<?php if(isset($_POST['fromDate'])){ echo $_POST['fromDate']; }else{ echo date('Y-m-d'); } ?>" required id="fromDate" class="form-control" placeholder="Insert start date"> 
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                      <input name="toDate" style="text-align:center;" value="<?php if(isset($_POST['toDate'])){ echo $_POST['toDate']; }else{ echo date('Y-m-d'); } ?>" required id="toDate" class="form-control" placeholder="Insert end date"> 
                    </div>
                </div>
                <div class="col-md-4">
                    <center>
                      <div class="form-group">
                      <label for=""></label>
                      <button type="submit" class="btn btn-success"> <i class="fa fa-search"></i> Search </button>
                    </div>
                    </center>
                </div>
              </form>

            </div>

            </div>
          </div>
        </div>
        <?php if(isset($_POST['fromDate'])&&isset($_POST['toDate'])){ ?>
        <div class="col-md-12 ">
          <div class="box box-success animated zoomIn">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-clock-o"></i> Date Wise Sales Report (<?php echo $_POST['fromDate']." to ".$_POST['toDate']; ?>) </h3>

              <div class="box-tools pull-right">
                
                <div class="btn-group">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
               <table id="example1" class="table table-bordered table-striped table-responsive">
                <thead>
                <tr>
                  <th width="12%">Invoice Number</th>
                  <th>Customer Name</th>
                  <th width="14%">Total Amount</th>
                  <th width="10%">Date</th>
                  <th width="12%">Time</th>
                  <th width="12%">Payment Type</th>
                  <th width="12%">Executive</th>
                  <th width="6%">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  $totalAmnt=0;
                  date_default_timezone_set('Asia/Dhaka');
                  $todaysDate = date("Y-m-d");
                  $results = $db_handle->getDateWiseSalesList($_POST['fromDate'],$_POST['toDate']);
                  $i=0;
                  $trow=count($results);
                  if($trow>0){
                   foreach($results as $dataArr) {
                    ++$i;
                    $tot_price=0;
                    $results1 = $db_handle->getPOSSalesItemList($dataArr["invoiceNumber"]);
                    if(count($results1)>0){
                     foreach($results1 as $dataArr1) {
                      $tot_price+=(($dataArr1["quantity"])*($dataArr1["unitPrice"]));
                      }
                    }
                    $totalAmnt+=$tot_price;
                ?>
                <tr class="">
                  <td><?php echo sprintf('%06u', ($dataArr["invoiceNumber"])); ?></td>
                  <td><?php echo ($dataArr["customerName"]); ?></td>
                  <td><b><?php echo CURRENCY1.round($tot_price,2).CURRENCY; ?></b></td>
                  <td><?php echo ($dataArr["invoiceDate"]); ?></td>
                  <td><?php echo ($dataArr["invoiceTime"]); ?></td>
                  <td><?php if(($dataArr["paymentType"])==1){ echo "Cash Payment";}else{ echo "Undefined"; } ?></td>
                  <td><?php echo ($dataArr["billby"]); ?></td>
                  <td>
                        <a target="_NEW" href="view_invoice.php?rid=<?php echo base64_encode(($dataArr["invoiceNumber"])); ?>" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="View Details" ><i class="fa fa-eye"></i></a>
                        <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Edit Information" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> -->
                  </td>
                </tr>

                <?php } } ?>
              </tbody>
              <tfoot>
                <tr class="">
                  <th></th>
                  <th style="text-align:right;">Total Amount</th>
                  <th><b><?php echo CURRENCY1.round($totalAmnt,2).CURRENCY; ?></b></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  
                </tr>
              </tfoot>
              </table>

            </div>

            </div>
            <!-- ./box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>

        <?php } ?>
        <!-- /.col -->
      </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $my_tools->myFooter(); ?>

</div>
<!-- ./wrapper -->
<?php include('js.php');?>
<!-- DataTables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- date-range-picker -->
<script src="../vendors/moment/min/moment.min.js"></script>
<!-- bootstrap datepicker -->
<script src="../vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
<script>
  $(function () {
    $('#example1').DataTable({
        "order": [[ 0, "desc" ]]
    })

    //Date picker
    $('#fromDate').datepicker({
      autoclose: true
    })

    //Date picker
    $('#toDate').datepicker({
      autoclose: true
    })

  })
</script>
</body>
</html>
