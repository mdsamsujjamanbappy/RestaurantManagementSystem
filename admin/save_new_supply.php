<?php
require_once 'header_link.php';

$tmpSuppliesId = $_SESSION['tmpSuppliesId'];
$supplierId= $_POST['supplierId'];

$results = $db_handle->getNewSuppliesId();
foreach($results as $row0) {
    $suppliesId = $row0['maximum'];
}

$suppliesId+=1;

$results = $db_handle->getTmpSuppliesList($tmpSuppliesId);
$trow=count($results);
if($trow>0){
	foreach($results as $dataArr) {
		 $id = ($dataArr["id"]);
		 $itemId = ($dataArr["itemId"]);
		 $quantity = ($dataArr["quantity"]);
		 $purchasePrice = ($dataArr["purchasePrice"]);
		 $itemType = ($dataArr["itemType"]);

		 $db_handle->saveSuppliesItem($suppliesId,$itemId,$quantity,$purchasePrice);
		 $db_handle->deleteTmpSuppliesItem($id);
		 
		 if($itemType==1){
		 	$db_handle->increaseItemQuantity($itemId,$quantity);
		 }


	}	
}
$userId = $_SESSION['adminRMSId'] ;
$db_handle->saveToSuppliesList($suppliesId,$supplierId, $userId);
unset($_SESSION['tmpSuppliesId']);
echo "<script>document.location.href='supply_details.php?istatus=added&&supid=".base64_encode($suppliesId)."';</script>";

?>