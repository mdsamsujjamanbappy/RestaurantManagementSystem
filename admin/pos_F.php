<?php require_once 'header_link.php'; ?>
<?php include('css.php');
if(isset($_SESSION['tmpPOSNumber'])){
$tmpPOSNumber=$_SESSION['tmpPOSNumber'];
}else{
  $_SESSION['tmpPOSNumber'] = rand(10,99999999);
}
?> 
<!-- Select2 -->
<link rel="stylesheet" href="../vendors/select2/dist/css/select2.css"> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  

<!-- DataTables -->
<link rel="stylesheet" href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">  
<title>Point of Sale</title>
<!-- Main content -->
<div class="container-fluid">
  
<script>
  function getItemList(val) {
         $.ajax({
         type: "POST",
         url: "get_item_list.php",
         data:'categoryId='+val,
         success: function(data){
             $("#item-list").html(data);
             $("#item-list").removeAttr('disabled');
        }
         });
     } 

     function addItemToList(val) {
        $.ajax({
          type: "POST",
          url: "add_item_to_list.php",
          data:'iId='+val,
          success: function(data){
             $('#posItemsList').load('pos_data.php');
          }
        });
     }

     function removeItemPOSList(val) {
        var x = confirm('Are you sure to delete?');
        
        if(x==true){
           $.ajax({
              type: "POST",
              url: "remove_item_pos_list.php",
              data:'iId='+val,
              success: function(data){
                 $('#posItemsList').load('pos_data.php');

              }
            });
        }

     }

     function increaseItemQuantityToPOS(val) {
        $.ajax({
          type: "POST",
          url: "increase_item_quantity_to_list.php",
          data:'iId='+val,
          success: function(data){
             $('#posItemsList').load('pos_data.php');
          }
        });
     }

     function decreaseItemQuantityToPOS(val) {
        $.ajax({
          type: "POST",
          url: "decrease_item_quantity_to_list.php",
          data:'iId='+val,
          success: function(data){
             $('#posItemsList').load('pos_data.php');
          }
        });
     }

      function getItemPrice(val) {
          $.ajax({
          type: "POST",
          url: "get_item_price.php",
          data:'pId='+val,
          success: function(data){
              $("#item_price").html(data);
              $("#item_quantity").val('1');
              $('#item_quantity').prop('disabled', false); 

          }
          });
      }

       function getTableList(val) {
         $.ajax({
         type: "POST",
         url: "get_table_list_ajax.php",
         data:'areaId='+val,
         success: function(data){
             $("#table-list").html(data);
        }
         });
     } 

    function dineinOrderProceess() {
          $('#posBookingTableList').load('get_booking_table_list.php');
    } 

    function holdOrderTable(val) {
         alert("Khali nai");
         
    } 

  </script>
<section class="content">
<div class="row">
  <div class="col-md-12">
    <div class="pull-left">
        <a href="dashboard.php" title="" class="btn btn-default"><i class="fa fa-dashboard"></i> Dashboard</a>
    </div>
    <div class="pull-right">
        <!-- <button type="button" id="hold_orders_list" data-toggle="modal" data-target="#hold_orders" class="btn btn-success"><i class="fa fa-history"></i> Hold Orders</button> -->
        <a href="signout.php" title="" class="btn btn-danger"><i class="fa fa-sign-out"></i> Sign Out</a>
    </div>
  </div>
</div>

<br>

<div class="row">
  <div class="col-md-6">
    <!-- Default box -->
    <div class="box box-danger">
      <div class="box-header with-border">
      
      </div>
      <div class="box-body" style="min-height:65%">
        <div  id="posItemsList">
          
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <div>
          <div class="pull-left">
              <!-- <a href="#" title="" class="btn btn-default"  data-toggle="modal" data-target="#dinein_order" onclick="dineinOrderProceess()" ><i class="fa fa-th"></i> Dine In</a> -->
              <!-- <a data-toggle="modal" data-target="#hold_order" class="btn btn-primary"><i class="fa fa-hand-stop-o"></i> Hold Order</a> -->
          </div>
          <div class="pull-right">
              <a data-toggle="modal" data-target="#checkout_pos"  onclick="checkout()" class="btn btn-success"><i class="fa fa-cart-plus"></i> Checkout</a>
          </div>
          <div id="printInvoice">
            
          </div>
        </div>        
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->
  </div>
  <div class="col-md-6">
    <!-- Default box -->
    <div class="box box-danger">
      <div class="box-header with-border">
      </div>
      <div class="box-body">
        <div class="row">
          <form id="itemDataForm">
          <div class="col-md-6">
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-server"></i></span>
              <input name="_MSBtoken" hidden value="<?php echo md5(rand(14446,50200));?>" >
               <select class="form-control" name="categoryId" id="categoryId" required onChange="getItemList(this.value);" >
                 <option value=""> Select Category</option>
                                         
               <?php
                    $results = $db_handle->getItemCategoryList();
                    $i=0;
                    $trow=count($results);
                    if($trow>0){
                     foreach($results as $dataArr) {
                       echo "<option value='".$dataArr['id']."'>".$dataArr['categoryName']."</option>";
                     }
                   }
                  ?>
              </select>
            </div>
            <br>

            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-archive"></i></span>
              <select id="item-list"  class="form-control" name="itemId"  onChange="getItemPrice(this.value);" required  class="select2me form-control"></select>
            </div>
            
          </div>
          <div class="col-md-6">
            <div class="input-group" id="item_price">
               <span class="input-group-addon"><i class="fa fa-money"></i></span>
              <input type="number" name="price" id="price" step="any" placeholder="Item price will be appear here" readonly min="0" class="form-control" >
            </div>
            <br>
            <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-signal"></i></span>
              <input type="number" name="item_quantity" min="1" step="any" placeholder="Quantity Amount" id="item_quantity" value="0" required class="form-control" >
            </div>
            <br>
           
          </div>
          
            <div class="form-group">
              <center><button type="button" class="btn btn-success btn-sm" id="submit_item_to_list"><i class="fa fa-cart-plus"></i> Add to List</button>
              <br>
              <div id="response"></div>  
              </center>
            </div>
          </form>
        </div>
        <div>
          <div class="col-md-12">
            <hr>
            <table id="example1" class="table table-hover table-bordered table-striped table-responsive" style='font-size:12px'>
              <thead>
              <tr>
                <th>Item Code</th>
                <th>Item Name</th>
                <th>Category</th>
                <th>Sell Price</th>
                <th width="10%">#</th>
              </tr>
              </thead>
              <tbody>
              <?php
                $results = $db_handle->getItemList();
                $trow=count($results);
                if($trow>0){
                 foreach($results as $dataArr) {
                  ++$i;
              ?>
              <tr>
                <td><?php echo ($dataArr["itemCode"]); ?></td>
                <td><?php echo ($dataArr["itemName"]); ?></td>
                <td><?php echo ($dataArr["categoryName"]); ?></td>
                <td><?php echo CURRENCY1.($dataArr["itemSellPrice"]).CURRENCY; ?></td>

                <td>
                  <button value="<?php echo ($dataArr["id"]); ?>" onclick="addItemToList(this.value);" class="btn btn-sm btn-success"><i class="fa fa-plus"></i></button>
                  
                </td>
              </tr>
              <?php } } ?>
            </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->
  </div>
</div>
</section>

  <div class="modal fade" id="hold_orders">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="fa fa-sheqel"></i> New Area Information</h4>
        </div>
        <div class="modal-body">
          <table  id="example1"  class="table table-responsive table-hover table-bordered" style="font-size:12px;">
            <thead>
              <tr>
                <th>Item</th>
                <th width="15%"><center>Unit Price</center></th>
                <th width="15%"><center>Quantity</center></th>
                <th width="15%"><center>Total</center></th>
                <th width="8%">#</th>
              </tr>
            </thead>
            <tbody>
             <?php
              $results = $db_handle->getPOSTmpItemList($_SESSION['tmpPOSNumber'] );
              $tot = 0;
              $trow=count($results);
              if($trow>0){
               foreach($results as $dataArr) {
                  $tot+=($dataArr['quantity']*$dataArr['unitPrice']);
              ?>
              <tr>
                <td><?php echo $dataArr['itemName'];?></td>
                <td><center><?php echo CURRENCY1.($dataArr['unitPrice']).CURRENCY;?></center></td>
                <td>
                  <center><?php echo $dataArr['quantity']. UNIT;?></center>
                </td>
                <td><center><?php echo CURRENCY1.($dataArr['quantity']*$dataArr['unitPrice']).CURRENCY;?></center></td>
                <td>
                  <a href="#" title="" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                </td>
              </tr>
              <?php } } ?>

            </tbody>
              <tfoot>
              <tr>
                <th colspan="2"></th>
                <th style="text-align:right">Total Amount</th>
                <th><center><?php echo CURRENCY1.($tot).CURRENCY; ?></center></th>
                <th></th>
              </tr>
            </tfoot>
          </table>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Save Information</button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
        </div>
         
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->


  <div class="modal fade" id="dinein_order">
    <div class="modal-dialog" style="width: 960px !important;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="fa fa-sheqel"></i>Hold Order</h4>
        </div>
        <div class="modal-body">
          
        <div class="row">
            <div class="col-md-4">
              <form id="dinein_order_table_data">

                <div class="form-group">
                   <select name="tableAreaId" required class="form-control" style="width:100%" id="areaId" onChange="getTableList(this.value);"  >
                    <option value=""> Select Area</option>
                      <?php
                       $results1 = $db_handle->getAreaList();
                       foreach($results1 as $dataArr1) {
                        ?>
                           <option value="<?php echo $dataArr1['id']; ?>"><?php echo $dataArr1['areaName']; ?></option>
                        <?php } ?>
                   </select>
                </div>
                <div class="form-group">
                  <select id="table-list" name="tableId" required  class="select3 form-control"  style="width:100%" ></select>
                </div>
                <div class="form-group">
                  <input type="number" name="seatsNumber" id="seatsNumber" min="0" required class="form-control" placeholder="Number of seats ">
                </div>

                <div class="form-group">
                   <select name="waiterId" class="select3 form-control" style="width:100%" id="waiterId">
                    <option value=""> Select Waiter </option>
                      <?php
                       $results1 = $db_handle->getEmployeeList();
                       foreach($results1 as $dataArr1) {
                        ?>
                           <option value="<?php echo $dataArr1['id']; ?>"><?php echo $dataArr1['empName']; ?></option>
                        <?php } ?>
                   </select>
                </div>
                <span id="dineInOrderResponse"></span>
                <span id="loadTmpPOSValue"></span>
                <br>
                <br>

                <button type="button" id="dineInOrderTable" class="btn btn-primary pull-left"><i class="fa fa-check-square"></i> Book Table </button>
                </form>
            </div>
            <div class="col-md-6 col-md-offset-1">
               <div  id="posBookingTableList"></div>
            </div>
        </div>

        </div>
        <div class="modal-footer">
          
        </div>
         
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->



  <div class="modal fade" id="checkout_pos">
    <div class="modal-dialog" style="width: 960px !important;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="fa fa-shopping-cart"></i> Checkout </h4>
        </div>
        <div class="modal-body">
          
        <div class="row">
            <div class="col-md-6">
              <form id="checkout_data">
                <div class="form-group">
                  <select name="customerId" id="customerId" class="select2 form-control" style="width:100%">
                    <option value="0"> Select Specific Customer</option>
                    <?php
                     $results2 = $db_handle->getCustomerList();
                     foreach($results2 as $dataArr2) {
                      ?>
                      <option value="<?php echo $dataArr2['id']; ?>"><?php echo $dataArr2['fullname']; ?></option>
                      <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="discountAmount">Discount Amount</label>
                  <input type="number" onChange="checkout()" name="discountAmount" id="discountAmount" min="0" value="0" class="form-control" placeholder="Insert Discount Amount ">
                </div>

                <div class="form-group">
                  <label for="vatAmount">VAT(%) Amount</label>
                  <input type="number" onChange="checkout()"  name="vatAmount" id="vatAmount" min="0" value="0" class="form-control" placeholder="Insert VAT(%) Amount ">
                </div>

                <div class="form-group">
                  <label for="deliveryCharge">Delivery Charge</label>
                  <input type="number" onChange="checkout()"  name="deliveryCharge" id="deliveryCharge" min="0" value="0" class="form-control" placeholder="Enter Delivery Charge">
                </div>

                <div class="form-group">
                  <label for="paidAmount">Paid Amount</label>
                  <input type="number" name="paidAmount" id="paidAmount" min="0" required class="form-control" placeholder="Enter Paid Amount">
                </div>

                <div class="form-group">
                  <label for="paymentType">Payment Method</label>
                  <select name="paymentType"  class="form-control" style="width:100%">
                    <option value="1" selected > Cash Payment</option>
                    <option value="2"> Card Payment</option>
                  </select>
                </div>

                <div id="posresponse"><br></div>

                <button type="button" id="finalCheckOut" class="btn btn-primary pull-left"><i class="fa fa-cart-plus" ></i> Proccess To Checkout </button>
                </form>
            </div>
            <div class="col-md-4 col-md-offset-1">
              <div>
              <h4><center><b>Summery</b></center></h4>
              <hr>
                <table class="table table-bordered table-hover table-responsive" style='font-size:13px'>
                  <tr>
                    <td width="60%">Sub-total</td>
                    <td style="font-weight:bold;text-align:center;"><span id="viewSubtotalAmount"> 0 </span></td>
                  </tr>
                  <tr>
                    <td>Discount Amount</td>
                    <td style="font-weight:bold;text-align:center;"><div id="viewDiscountAmount"></td>
                  </tr>
                  <tr>
                    <td>VAT Amount</td>
                    <td style="font-weight:bold;text-align:center;"><div id="viewVatAmount"></div></td>
                  </tr>
                  <tr>
                    <td>Delivery Charge</td>
                    <td style="font-weight:bold;text-align:center;"><div id="viewDeliveryChargeAmount"></div></td>
                  </tr>
                  <tr>
                    <td>Total Payable Amount</td>
                    <td style="font-weight:bold;text-align:center;"><div id="totalPayableAmount"></div></td>
                  </tr>
                  <tr>
                    <td>Paid Amount</td>
                    <td style="font-weight:bold;text-align:center;"><div id="viewPaidAmount"></div></td>
                  </tr>
                  <tr>
                    <td>Return Amount</td>
                    <td style="font-weight:bold;text-align:center;"><div id="viewReturnAmount"></div></td>
                  </tr>
                </table>

              </div>
            </div>
        </div>

        </div>
        <div class="modal-footer">
          
        </div>
         
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->


  <div class="modal fade" id="hold_order_success">
    <div class="modal-dialog" style="width: 220px !important;border-radius:5px;" >
      <div class="modal-content"  style="border-radius:10px;" >
        <div class="modal-body">
          <center>
             <i class="fa fa-check-circle-o" style='font-size: 200;color:green;'></i>
             <h2>Success</h2>
          </center>
        </div>

      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->




<footer style="padding:2px 20px;">
<hr>
  <div class="pull-right hidden-xs">
    <b>Version</b> 1.0.0
  </div>
  <strong>Copyright &copy; <a href="https://feits.co">FEITS </a>.</strong> All rights
  reserved.
</footer>

</div>
<?php include('js.php');?>
<!-- DataTables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="../vendors/select2/dist/js/select2.full.min.js"></script>
<script>  
 $(document).ready(function(){  
      $('#submit_item_to_list').click(function(){  
           var itemId = $('#itemId').val();  
           var item_quantity = $('#item_quantity').val();  
           var price = $('#price').val();  
           if(itemId == '' || item_quantity == '' || price == '')  
           {  
                $('#response').html("<span class='text-danger' style='font-weight:bold;''>Invalid data.</span>");  
           }  
           else  
           {  
                $.ajax({  
                     url:"insert_item_to_list_ajax.php",  
                     method:"POST",  
                     data:$('#itemDataForm').serialize(),  
                     beforeSend:function(){  
                          $('#response').html('<span class="text-info"  style="font-weight:bold;">Loading response...</span>');  
                     },  
                     success:function(data){  
                          $('#itemDataForm').trigger("reset"); 
                          $('#categoryId').val(""); 
                          $('#price').val("0"); 
                          $('#item-list').attr('disabled', 'disabled'); 
                          $('#item_quantity').prop('disabled', true); 
                          $('#price').prop('disabled', true); 

                          $('#response').fadeIn().html(data);  
                          setTimeout(function(){  
                               $('#response').fadeOut("slow");  
                          }, 2000);  
                          $('#posItemsList').load('pos_data.php');
                     }  
                });  
           }  
      });  
  
  $('#item-list').attr('disabled', 'disabled'); 
  $('#posItemsList').load('pos_data.php');
  $('#posBookingTableList').load('get_booking_table_list.php');
 });  
 </script>
 <script>  
 $(document).ready(function(){  
      $('#finalCheckOut').click(function(){  
          var paidAmount = $('#paidAmount').val();  
          var countItemList =  $('#countItemList').val();
           if(parseInt(countItemList)==0)  
           {  
                alert("Cart is empty. Add Item First to checkout.");  
                $('#checkout_pos').modal('hide');  

           }else if(!paidAmount)  
           {  
                $('#posresponse').html("<span class='text-danger' style='font-weight:bold;''>Invalid data.</span><br><br>");  
           }  
           else  
           {  
                $.ajax({  
                     url:"save_pos_ajax.php",  
                     method:"POST",  
                     data:$('#checkout_data').serialize(),  
                     beforeSend:function(){  
                          $('#posresponse').html('<span class="text-info"  style="font-weight:bold;">Proccessing ...</span><br><br>');  
                     },  
                     success:function(data){  
                          $('#checkout_data').trigger("reset"); 
                          $('#customerId').find('option:first').attr('selected', 'selected');
                          $('#customerId').select2('val','0');
                          $('#posresponse').fadeIn().html(data);  
                          setTimeout(function(){  
                               $('#posresponse').fadeOut("slow");  
                          }, 6000);  
                          
                          $('#checkout_pos').modal('hide');  
                          $('#hold_order_success').modal('hide');

                          setTimeout(function(){  
                            $('#hold_order_success').modal('hide');
                        }, 2000); 
                          $('#posItemsList').load('pos_data.php');
                          printExternal('print_invoice.php');
                     }  
                });  
           }  
      });  
  
  $('#item-list').attr('disabled', 'disabled'); 
  $('#posItemsList').load('pos_data.php');
  $('#posBookingTableList').load('get_booking_table_list.php');
 });  
 </script>

 <script>  
  function checkout() {
      // $('#posItemsList').load('pos_data.php');
      var subtotal =0;
      subtotal =  $('#subtotalvalue1').val();
      if(subtotal.length === 0){
        subtotal=0;
      }

      var deliveryCharge = $('#deliveryCharge').val();
      if(deliveryCharge.length === 0){
        deliveryCharge=0;
      }

      var discount = $('#discountAmount').val();
      if(discount.length === 0){
        discount=0;
      }

      var paidAmount = $('#paidAmount').val();
      if(paidAmount.length === 0){
        paidAmount=0;
      }


      $('#viewSubtotalAmount').html("<?php echo CURRENCY1;?>"+parseFloat(subtotal).toFixed(2)+"<?php echo CURRENCY; ?>"); 
      
      $('#viewDiscountAmount').html("<?php echo CURRENCY1; ?>"+ parseFloat(discount).toFixed(2) + "<?php echo CURRENCY; ?>"); 
      
      var vat = ($('#vatAmount').val()/100)*subtotal;
      $('#viewVatAmount').html("<?php echo CURRENCY1; ?>" + parseFloat(vat).toFixed(2) + "<?php echo CURRENCY; ?>"); 

      $('#viewDeliveryChargeAmount').html("<?php echo CURRENCY1; ?>"+ parseFloat(deliveryCharge).toFixed(2) + "<?php echo CURRENCY; ?>"); 

      $('#viewPaidAmount').html("<?php echo CURRENCY1; ?>"+ parseFloat(paidAmount).toFixed(2) + "<?php echo CURRENCY; ?>"); 
      


      var netPayableAmount0 = (((parseFloat(subtotal)+parseFloat(vat))-parseFloat(discount))+parseFloat(deliveryCharge));

      $('#viewReturnAmount').html("<?php echo CURRENCY1; ?>"+ (parseFloat(paidAmount)-parseFloat(netPayableAmount0)).toFixed(2) + "<?php echo CURRENCY; ?>"); 

      $('#totalPayableAmount').html("<?php echo CURRENCY1; ?>"+ netPayableAmount0.toFixed(2) + "<?php echo CURRENCY; ?>"); 

  } 

  $("#paidAmount").keyup(function (e) {
    checkout();
    if (e.which === 13) {
      $("#finalCheckOut").click();
    }
   });
  checkout();


function printExternal(url) {
    var printWindow = window.open( url, 'Print', 'left=200, top=200, width=950, height=500, toolbar=0, resizable=0');
    printWindow.addEventListener('load', function(){
        printWindow.print();
        printWindow.close();
    }, true);
}

 </script>  
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
    $('.select3').select2()
    $('#example1').DataTable({
      "pageLength": 5
    })
  })
</script>

