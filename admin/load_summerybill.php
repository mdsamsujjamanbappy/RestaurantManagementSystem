     <?php 
require_once 'header_link.php';
     ?>
        <div class="row">
            <div class="col-md-6">
              <form id="checkout_data">
                <div class="form-group">
                  <select name="customerId"  class="select2 form-control" style="width:100%">
                    <option value=""> Select Specific Customer</option>
                    <?php
                     $results2 = $db_handle->getDesignationList();
                     foreach($results2 as $dataArr2) {
                      ?>
                      <option value="<?php echo $dataArr2['id']; ?>"><?php echo $dataArr2['designationName']; ?></option>
                      <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="discountAmount">Discount Amount</label>
                  <input type="number" onChange="checkout()" name="discountAmount" id="discountAmount" min="0" value="0" class="form-control" placeholder="Insert Discount Amount ">
                </div>

                <div class="form-group">
                  <label for="vatAmount">VAT(%) Amount</label>
                  <input type="number" onChange="checkout()"  name="vatAmount" id="vatAmount" min="0" value="0" class="form-control" placeholder="Insert VAT(%) Amount ">
                </div>

                <div class="form-group">
                  <label for="deliveryCharge">Delivery Charge</label>
                  <input type="number" onChange="checkout()"  name="deliveryCharge" id="deliveryCharge" min="0" value="0" class="form-control" placeholder="Enter Delivery Charge">
                </div>

                <div class="form-group">
                  <label for="paidAmount">Paid Amount</label>
                  <input type="number" onkeyup="checkout()" name="paidAmount" id="paidAmount" min="0" required class="form-control" placeholder="Enter Paid Amount">
                </div>

                <div id="posresponse">
                  
                <br>
                </div>
                <button type="button" id="finalCheckOut" class="btn btn-primary pull-left"><i class="fa fa-cart-plus" ></i> Proccess To Checkout </button>
                </form>
            </div>
            <div class="col-md-4 col-md-offset-1">
              <div>
              <h4><center><b>Summery</b></center></h4>
              <hr>
                <table class="table table-bordered table-hover table-responsive" style='font-size:13px'>
                  <tr>
                    <td width="60%">Sub-total</td>
                    <td style="font-weight:bold;text-align:center;"><span id="subtotalAmount"> 0 </span></td>
                  </tr>
                  <tr>
                    <td>Discount Amount</td>
                    <td style="font-weight:bold;text-align:center;"><div id="viewDiscountAmount"></td>
                  </tr>
                  <tr>
                    <td>VAT Amount</td>
                    <td style="font-weight:bold;text-align:center;"><div id="viewVatAmount"></div></td>
                  </tr>
                  <tr>
                    <td>Delivery Charge</td>
                    <td style="font-weight:bold;text-align:center;"><div id="viewDeliveryChargeAmount"></div></td>
                  </tr>
                  <tr>
                    <td>Total Payable Amount</td>
                    <td style="font-weight:bold;text-align:center;"><div id="totalPayableAmount"></div></td>
                  </tr>
                  <tr>
                    <td>Paid Amount</td>
                    <td style="font-weight:bold;text-align:center;"><div id="viewPaidAmount"></div></td>
                  </tr>
                  <tr>
                    <td>Return Amount</td>
                    <td style="font-weight:bold;text-align:center;"><div id="viewReturnAmount"></div></td>
                  </tr>
                </table>

              </div>
            </div>
        </div>

<script>
function checkout1() {
   
      var subtotal = "<?php echo $_SESSION['subtotalvalue']; ?>";
      if(subtotal.length === 0){
        subtotal=0;
      }

      var deliveryCharge = $('#deliveryCharge').val();
      if(deliveryCharge.length === 0){
        deliveryCharge=0;
      }

      var discount = $('#discountAmount').val();
      if(discount.length === 0){
        discount=0;
      }

      var paidAmount = $('#paidAmount').val();
      if(paidAmount.length === 0){
        paidAmount=0;
      }


      $('#subtotalAmount').html("<?php echo CURRENCY1.$_SESSION['subtotalvalue'].CURRENCY; ?>"); 
      
      $('#viewDiscountAmount').html("<?php echo CURRENCY1; ?>"+ parseFloat(discount).toFixed(2) + "<?php echo CURRENCY; ?>"); 
      
      var vat = ($('#vatAmount').val()/100)*subtotal;
      $('#viewVatAmount').html("<?php echo CURRENCY1; ?>" + parseFloat(vat).toFixed(2) + "<?php echo CURRENCY; ?>"); 

      $('#viewDeliveryChargeAmount').html("<?php echo CURRENCY1; ?>"+ parseFloat(deliveryCharge).toFixed(2) + "<?php echo CURRENCY; ?>"); 

      $('#viewPaidAmount').html("<?php echo CURRENCY1; ?>"+ parseFloat(paidAmount).toFixed(2) + "<?php echo CURRENCY; ?>"); 
      


      var netPayableAmount0 = (((parseFloat(subtotal)+parseFloat(vat))-parseFloat(discount))+parseFloat(deliveryCharge));

      $('#viewReturnAmount').html("<?php echo CURRENCY1; ?>"+ (parseFloat(paidAmount)-parseFloat(netPayableAmount0)).toFixed(2) + "<?php echo CURRENCY; ?>"); 

      $('#totalPayableAmount').html("<?php echo CURRENCY1; ?>"+ netPayableAmount0.toFixed(2) + "<?php echo CURRENCY; ?>"); 

      $('#paidAmount').val().focus(); 
  } 
  checkout();
 </script>  