<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $my_tools->title();?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php include('css.php');?> 
  <!-- DataTables -->
  <link rel="stylesheet" href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">  
  <!-- Select2 -->
  <link rel="stylesheet" href="../vendors/select2/dist/css/select2.min.css">
 </head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php $my_tools->logoArea();?>
    
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <?php include('messages.php');?>
          <!-- Notifications: style can be found in dropdown.less -->
          <?php include('notifications.php');?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php $my_tools->adminImage();?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php $my_tools->adminFullname();?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php $my_tools->adminImage();?>" class="img-circle" alt="User Image">

                <p>
                  <?php $my_tools->adminFullname();?>
                </p>
              </li>
                  <?php $my_tools->profileLink();?>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <?php $my_tools->control_sidebar();?>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php $my_tools->adminImage();?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php $my_tools->adminFullname();?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <?php include('search_form.php');?>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <?php $my_tools->adminMenu();?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <br>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <?php if(isset($_GET['sst'])){ ?>
           <div class="alert alert-success alert-dismissible fade in" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong> Table information has been successfully <?php echo $_GET['smsg'];?>.</strong>
            </div>
            <div class="clearfix"></div>
           <?php } ?>

          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Table List</h3>

              <div class="box-tools pull-right">
                
                <button type="button"  data-toggle="modal" data-target="#add_area" class="btn btn-success btn-sm" ><i class="fa fa-plus"></i> Add New Table</button>
                <div class="btn-group">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
               <table id="example1" class="table table-hover table-bordered table-striped table-responsive">
                <thead>
                <tr>
                  <th width="6%">Serial</th>
                  <th>Table Name</th>
                  <th>Area Name</th>
                  <th>Maximum Seats</th>
                  <th>Status</th>
                  <th width="15%">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  $results = $db_handle->getTableList();
                  $i=0;
                  $trow=count($results);
                  if($trow>0){
                   foreach($results as $dataArr) {
                    ++$i;
                ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo ($dataArr["tableName"]); ?></td>
                  <td><?php echo ($dataArr["areaName"]); ?></td>
                  <td><?php echo ($dataArr["tableMaxSeats"]); ?></td>
                  <td>
                    <?php 
                      if(($dataArr["tableStatus"])==1){
                        echo "<span class='label label-success' style='font-size:12px'>Available</span>";
                      }elseif (($dataArr["tableStatus"])==2) {
                        echo "<span class='label label-primary' style='font-size:12px'>In Use</span>";
                      }elseif (($dataArr["tableStatus"])==3) {
                        echo "<span class='label label-danger' style='font-size:12px'>Out of Use</span>";
                      }elseif (($dataArr["tableStatus"])==4) {
                        echo "<span class='label label-info' style='font-size:12px'>Reserved</span>";
                      }else{
                        echo "";
                        } 
                    ?>
                  </td>
                  <td>
                      <div class="btn-group">
                      <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        Options <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#" data-toggle="modal" data-target="#view_table<?php echo $i; ?>" ><i class="fa fa-eye"></i> Details Information</a></li>
                        <li><a href="edit_table_information.php?id=<?php echo base64_encode($dataArr['id']); ?>"><i class="fa fa-edit"></i> Edit Information</a></li>
                        <li><a href="delete_table_information.php?id=<?php echo base64_encode($dataArr['id']); ?>" onclick="return confirm('Are you sure to delete?')"><i class="fa fa-trash"></i> Delete Information</a></li>

                      </ul>
                    </div>
                  </td>
                </tr>

                <div class="modal fade" id="view_table<?php echo $i; ?>">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-eye"></i> Table Details Information</h4>
                      </div>
                      <div class="modal-body">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-map-signs"></i></span>
                          <input type="text" readonly  class="form-control" value="<?php echo $dataArr['tableName']; ?>" >
                          <input name="id" hidden value="<?php echo $dataArr['id']; ?>">
                        </div>
                        <br>

                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-dot-circle-o"></i></span>
                           <select name="tableAreaId" required class="form-control" style="width:100%" id="areaId" disabled >
                               <option value=""> Select Area</option>
                              <?php
                               $results1 = $db_handle->getAreaList();
                               foreach($results1 as $dataArr1) {
                                ?>
                                <option <?php if($dataArr1['id']==$dataArr['tableAreaId']){ echo "selected";} ?> value="<?php echo $dataArr1['id']; ?>"><?php echo $dataArr1['areaName']; ?></option>
                                <?php } ?>
                           </select>
                        </div>
                        <br>
                        
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-wheelchair"></i></span>
                          <input type="number" readonly min="0" required id="tableMaxSeats" name="tableMaxSeats"  value="<?php echo $dataArr['tableMaxSeats']; ?>" class="form-control" placeholder="Table Maximum Seats ">
                        </div>
                        <br>

                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-align-left"></i></span>
                          <textarea readonly name="tableDescription" id="tableDescription" class="form-control" placeholder="Table Description "><?php echo $dataArr['tableDescription']; ?></textarea>
                        </div>
                        <br>

                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-qrcode"></i></span>
                          <select disabled name="status" required class="form-control" style="width:100%" id="status" >
                            <option value=""> Select Status</option>
                            <option <?php if($dataArr['tableStatus']==1){ echo "selected"; } ?> value="1"> Available</option>
                            <option <?php if($dataArr['tableStatus']==2){ echo "selected"; } ?> value="2"> In Use</option>
                            <option <?php if($dataArr['tableStatus']==3){ echo "selected"; } ?> value="3"> Out of Use</option>
                            <option <?php if($dataArr['tableStatus']==4){ echo "selected"; } ?> value="4"> Reserved</option>
                          </select>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                      </div>
                       
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

                <?php } } ?>
              </tbody>
              </table>

            </div>

            </div>
            <!-- ./box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $my_tools->myFooter(); ?>

  <div class="modal fade" id="add_area">
    <div class="modal-dialog">
      <div class="modal-content">
       <form action="save_table.php" method="POST">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="fa fa-hourglass-3"></i> New Table Information</h4>
        </div>
        <div class="modal-body">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-map-signs"></i></span>
            <input name="_MSBtoken" hidden value="<?php echo md5(rand(14446,50200));?>" >
            <input type="text" autofocus required id="tableName" name="tableName" class="form-control" placeholder="Table Name ">
          </div>
          <br>

          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-dot-circle-o"></i></span>
             <select name="tableAreaId" required class="form-control" style="width:100%" id="areaId" >
              <option value=""> Select Area</option>
                <?php
                 $results1 = $db_handle->getAreaList();
                 foreach($results1 as $dataArr1) {
                  ?>
                     <option value="<?php echo $dataArr1['id']; ?>"><?php echo $dataArr1['areaName']; ?></option>
                  <?php } ?>
             </select>
          </div>
          <br>
          
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-wheelchair"></i></span>
            <input type="number" min="0" required id="tableMaxSeats" name="tableMaxSeats" class="form-control" placeholder="Table Maximum Seats ">
          </div>
          <br>

          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-align-left"></i></span>
            <textarea name="tableDescription" id="tableDescription" class="form-control" placeholder="Table Description "></textarea>
          </div>
          <br>

          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-qrcode"></i></span>
            <select name="status" required class="form-control" style="width:100%" id="status" >
              <option value=""> Select Status</option>
              <option value="1"> Available</option>
              <option value="2"> In Use</option>
              <option value="3"> Out of Use</option>
              <option value="4"> Reserved</option>
            </select>
          </div>
          <br>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success pull-left"><i class="fa fa-save"></i> Save Information</button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
        </div>
         
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

 
</div>
<!-- ./wrapper -->
<?php include('js.php');?>
<!-- DataTables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="../vendors/select2/dist/js/select2.full.min.js"></script>
<script>
  $(function () {
    $('#example1').DataTable()


    //Initialize Select2 Elements
    $('.select2').select2()
  })
</script>
</body>
</html>
