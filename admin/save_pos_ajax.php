<?php  
require_once 'header_link.php';
$results = $db_handle->getPOSTmpItemList($_SESSION['tmpPOSNumber'] );
if(count($results)>0){
if(isset($_POST['paidAmount'])){
	if(!empty($_SESSION['tmpPOSNumber'])){

		$results = $db_handle->getNewInvoiceNumber();
        foreach($results as $row0) {
            $invoiceNumber = $row0['maximum'];
        }
        
        $invoiceNumber+=1;

		$customerId= $_POST['customerId'];
		$discountAmount= $_POST['discountAmount'];
		$vatAmount= $_POST['vatAmount'];
		$deliveryCharge= $_POST['deliveryCharge'];
		$paidAmount= $_POST['paidAmount'];
		$paymentType= $_POST['paymentType'];
		$tmpPOSNumber=$_SESSION['tmpPOSNumber'];

        date_default_timezone_set('Asia/Dhaka');
        $invoiceDate = date("Y-m-d");
        $invoiceTime = date("h:i:sa");
        $userId =$_SESSION['adminRMSId'];
			
		$results = $db_handle->getPOSTmpItemList($_SESSION['tmpPOSNumber'] );

		 if(count($results)>0){
		    foreach($results as $dataArr) {
		    	$tmp_id = $dataArr["id"];
		    	$itemStockManagement = $dataArr["itemStockManagement"];
                $itemId = $dataArr["itemId"];
                $quantity = $dataArr["quantity"];
                $unitPrice = $dataArr["unitPrice"];
	           $db_handle->tmpToSales($invoiceNumber,$itemId,$quantity,$unitPrice);
	           if($itemStockManagement==1){
	           		 $db_handle->updateDecreaseItemQuantity($itemId,$quantity);
	           	}
               $db_handle->removeItemFromPOSTmp($tmp_id);
		    }
		 }

		$r = $db_handle->savePOSInvoice($invoiceNumber,$customerId,$discountAmount,$vatAmount,$deliveryCharge,$paidAmount,$invoiceDate,$invoiceTime,$userId,$paymentType);
			
			$_SESSION['invoiceNumber']=$invoiceNumber;
			
			if($r>0){
				if(!empty($_SESSION['holdId'])){
					$r = $db_handle->removeHoldIdFromDB($_SESSION['holdId']);
					unset($_SESSION['holdId']);
				}
				unset($_SESSION['tmpPOSNumber']);
				echo "<span class='text-success'  style='font-weight:bold;'><br> Invoice Number <span id='sInvoiceNumber'>".$invoiceNumber."</span> has been successfully generated.</span><br><br>";
			}

	}else{ 
		echo "<span class='text-danger' style='font-weight:bold;'><br> Invalid Data. </span>"; }

}
}else{ 
	echo "<span class='text-danger' style='font-weight:bold;'><br> No data on cart. </span>";
	 }
 ?>