<?php
require_once 'header_link.php';

if(isset($_POST['_MSBtoken'])){

	$itemName = $_POST['itemName'];
	$itemCode = $_POST['itemCode'];
	$itemSellPrice = $_POST['itemSellPrice'];
	$itemStockManagement = $_POST['itemStockManagement'];
	
	if(isset($_POST['itemQuantity'])){
		$itemQuantity = $_POST['itemQuantity'];
	}else{
		$itemQuantity = 0;
	}
	
	$itemDescription = $_POST['itemDescription'];
	$itemCategoryId = $_POST['itemCategoryId'];
	$itemDepartmentId = $_POST['itemDepartmentId'];
	$itemType = $_POST['itemType'];
	$itemExpireDateAction = $_POST['itemExpireDateAction'];
	$itemExpireDate = $_POST['itemExpireDate'];
	$itemStockAlert = $_POST['itemStockAlert'];
	$image="idefault.jpg";
	if(isset($_FILES['itemPhoto']['name'])){
      if(($_FILES['itemPhoto']['size']) <= (3000*1024))
      {

    	  $maxrow= $db_handle->getTotalRowNumber("tbitemlist");

          $picture_tmp = $_FILES['itemPhoto']['tmp_name'];
          $picture_name = $_FILES['itemPhoto']['name'];
          $picture_type = $_FILES['itemPhoto']['type'];
         
          $arr1 = explode(".", $picture_tmp);
          $extension1 = strtolower(array_pop($arr1));  
          
          $arr = explode(".", $picture_name);
          $extension = strtolower(array_pop($arr));

          $image="item".date('Ymdhs')."_".$maxrow.".".$extension;
          $newfilename1="item".date('Ymdhs')."_".$maxrow.".".$extension1;
          $path = '../item_images/'.$image;

          move_uploaded_file($picture_tmp, $path);
	    }
	  }

	$db_handle->saveItemInformation($itemName,$itemCode,$itemSellPrice,$itemStockManagement,$itemQuantity,$itemDescription,$itemCategoryId,$itemDepartmentId,$itemType,$itemExpireDateAction,$itemExpireDate,$itemStockAlert,$image);

	echo "<script>document.location.href='item_list.php?sst=success&&smsg=added';</script>";
}
?>