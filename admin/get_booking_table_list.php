<?php require_once 'header_link.php'; ?>

<table id="example1" class="table table-responsive table-hover table-bordered" style="font-size:12px;">
  <thead>
    <tr>
	  <th width="6%">Serial</th>
	  <th>Table Name</th>
	  <th>Area Name</th>
	  <th>Waiter Name</th>
	  <th>Status</th>
	</tr>
  </thead>
  <tbody>
      <?php
      $results = $db_handle->getBookedTableList();
      $i=0;
      $trow=count($results);
      if($trow>0){
       foreach($results as $dataArr) {
        ++$i;
    ?>
    <tr>
      <td><?php echo $i; ?></td>
      <td><?php echo ($dataArr["tableName"]); ?></td>
      <td><?php echo ($dataArr["areaName"]); ?></td>
      <td><?php echo ($dataArr["tableMaxSeats"]); ?></td>
      <td><span class='label label-danger' style='font-size:12px'>Booked</span></td>
    </tr>
    <?php } } ?>

  </tbody>
  
</table>
