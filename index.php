<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Restaurant Management System </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="icon" href="assets/img/favicon.png" type="image/gif" sizes="16x16">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="vendors/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/custom.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="dist/css/animate.css">
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style type="text/css" media="screen">
    html{
      height: 0 !important;

    }
  </style>
   
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <center>
    <!-- <img  id="loading_logo1" src="assets/img/loading.gif" style="width:150px;padding-bottom:10px"><br> -->
  </center>
  <!-- /.login-logo -->
  <div class="login-box-body">

  <div class="login-logo">
    <a href="index.php"><b>FEITS - RMS</b></a>
  </div>

    <p class="login-box-msg">Sign in to start your session</p>

    <form id="signin" method="post">
      <div class="form-group has-feedback">
        <input type="text" name="uname" class="form-control" placeholder="Username">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="upass" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div  class="form-group has-feedback">
          <select name="userType" class="form-control" required >
            <option value="">Select User Type</option>
            <option value="1">Admin</option>
            <option value="2">Executive</option>
          </select>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-12">
          <center><button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button></center>
        </div>
        <!-- /.col -->
      </div>
    </form>
    <center><img  id="loading_logo"  src="assets/img/loading.gif" style="width:150px;padding-bottom:10px"><br></center>
    <div class="login-footer">
      Copyright &copy; FEITS 
    </div>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
 <script>
      $(document).ready(function() {
      $("#loading_logo").hide();
      $('#signin').submit(function(e) {
        e.preventDefault();
        $.ajax({
           type: "POST",
           url: 'check_login.php',
           data: $(this).serialize(),  
           beforeSend:function(){  
                $('#loading_logo').show(); 
                $('#signin').hide(); 
                $('.login-box-msg').html("Please wait ...");
           },  
           success: function(data)
           {
            var audio = new Audio('assets/audio/beep.mp3');
            var audio1 = new Audio('assets/audio/beep.mp3');

            var result = $.trim(data);
            // alert(result);
              if (result === 'adminLogin') {

                 setTimeout(function(){ 
                      $('.login-box-msg').html("<div class='alert alert-success  animated fadeIn'>Login Successfull</div>");
                      audio.play();

                    setTimeout(function(){ 
                         $('.login-box-msg').html("<div class='alert alert-success  animated fadeIn'>Redirect to Dashboard</div>");
                           var audio2 = new Audio('assets/audio/thankyou.mp3');
                            audio2.play();
                          
                    }, 1000); 
                   
                    setTimeout(function(){  
                      window.location = 'admin/dashboard.php';
                    }, 3000); 

                }, 500); 

              }
              else if (result === 'executiveLogin') {

                 setTimeout(function(){ 
                      $('.login-box-msg').html("<div class='alert alert-success  animated fadeIn'>Login Successfull</div>");
                      audio.play();

                    setTimeout(function(){ 
                         $('.login-box-msg').html("<div class='alert alert-success  animated fadeIn'>Redirect to Dashboard</div>");
                           var audio2 = new Audio('assets/audio/thankyou.mp3');
                            audio2.play();
                          
                    }, 1000); 
                   
                    setTimeout(function(){  
                      window.location = 'executive/dashboard.php';
                    }, 3000); 

                }, 500); 

              }
              else {

                    var audio = new Audio('assets/audio/error.mp3');

                    setTimeout(function(){ 
                      $('#loading_logo').hide(); 
                      $('#signin').fadeIn(); 
                        $('.login-box-msg').html("<div class='animated fadeIn' style='font-size:17px;color:red;font-weight:bold;'>Invalid Credentials</div>");
                        audio.play();
                    } , 500); 
              }
           }
       });
     });
    });
  </script>
</body>
</html>
