<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $my_tools->title();?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php include('css.php');?>
 </head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php $my_tools->logoArea();?>
    
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <?php include('messages.php');?>
          <!-- Notifications: style can be found in dropdown.less -->
          <?php include('notifications.php');?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php $my_tools->executiveImage();?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php $my_tools->exFullname();?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php $my_tools->executiveImage();?>" class="img-circle" alt="User Image">

                <p>
                  <?php $my_tools->exFullname();?>
                </p>
              </li>
                  <?php $my_tools->profileLink();?>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <?php $my_tools->control_sidebar();?>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php $my_tools->executiveImage();?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php $my_tools->exFullname();?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <?php include('search_form.php');?>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <?php $my_tools->executiveMenu();?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">

          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">View Item Information</h3>

              <div class="box-tools pull-right">
                
                <div class="btn-group">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php
                  $id = base64_decode($_GET['id']);
                  $results = $db_handle->getItemListById($id);
                  $i=0;
                  $trow=count($results);
                  if($trow>0){
                   foreach($results as $dataArr) {
                    ++$i;
                ?>
              <br>
              <div class="row">
                <div class="col-md-5">

                  <div class="form-group">
                    <label for="itemName">Item Name</label>
                    <input disabled type="text" value="<?php echo ($dataArr["itemName"]); ?>" class="form-control" autofocus id="itemName" name="itemName" placeholder="Insert Item name " required >
                  </div>

                  <div class="form-group">
                    <label for="itemCode">Item Code</label>
                    <input disabled type="text" id="itemCode" value="<?php echo ($dataArr["itemCode"]); ?>" class="form-control" name="itemCode" placeholder="Insert Item Code " required >
                  </div>

                  <div class="form-group">
                    <label for="itemSellPrice">Item Selling Price </label>
                    <input disabled type="number" step="any" value="<?php echo ($dataArr["itemSellPrice"]); ?>" min="0" id="itemSellPrice" class="form-control" name="itemSellPrice" placeholder="Insert Item Selling Price " required >
                  </div>

                  <div class="form-group">
                    <label for="itemStockManagement">Item Stock Management </label>
                    <select disabled name="itemStockManagement" id="itemStockManagement" required class="form-control">
                      <option value="">-- Select --</option>
                      <option <?php if($dataArr['itemStockManagement']==1){ echo "selected"; } ?> value="1">Enable</option>
                      <option <?php if($dataArr['itemStockManagement']==0){ echo "selected"; } ?> value="0">Disable</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="itemQuantity">Item Quantity </label>
                    <input disabled type="number" step="any" value="<?php echo ($dataArr["itemQuantity"]); ?>" id="itemQuantity" class="form-control" name="itemQuantity" min="0" placeholder="Insert Item Quantity" required >
                  </div>

                  <div class="form-group">
                    <label for="itemStockAlert">Stock Alert  </label>
                    <select disabled name="itemStockAlert" required class="form-control">
                      <option <?php if($dataArr['itemStockAlert']==1){ echo "selected"; } ?> value="1">Enable</option>
                      <option <?php if($dataArr['itemStockAlert']==0){ echo "selected"; } ?> value="0">Disable</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="itemDescription">Item Description </label>
                    <textarea disabled id="itemDescription" class="form-control" name="itemDescription" placeholder="Insert Item Description" ><?php echo ($dataArr["itemDescription"]); ?></textarea>
                  </div>

                </div>
                <div class="col-md-5 col-md-offset-1">
                  <div class="form-group">
                    <label for="itemCategoryId">Item Category </label>
                    <select disabled name="itemCategoryId" required class="select2 form-control" style="width:100%" id="itemCategoryId" >
                    <option value=""> Select Item Category</option>
                      <?php
                       $results1 = $db_handle->getItemCategoryList();
                       foreach($results1 as $dataArr1) {
                        ?>
                           <option  <?php if($dataArr['itemDepartmentId']==$dataArr1['id']){ echo "selected"; } ?>  value="<?php echo $dataArr1['id']; ?>"><?php echo $dataArr1['categoryName']; ?></option>
                        <?php } ?>
                   </select>
                  </div>

                  <div class="form-group">
                    <label for="itemDepartmentId">Item Department </label>
                    <select disabled name="itemDepartmentId" required class="select2 form-control" style="width:100%" id="itemDepartmentId" >
                    <option value=""> Select Item Department</option>
                      <?php
                       $results2 = $db_handle->getItemDepartmentList();
                       foreach($results2 as $dataArr2) {
                        ?>
                           <option <?php if($dataArr['itemDepartmentId']==$dataArr2['id']){ echo "selected"; } ?> value="<?php echo $dataArr2['id']; ?>"><?php echo $dataArr2['departmentName']; ?></option>
                        <?php } ?>
                   </select>
                  </div>

                  <div class="form-group">
                    <label for="itemExpireDate">Item Expiration Date </label>
                    <input disabled type="text" id="itemExpireDate" value="<?php echo ($dataArr["itemExpireDate"]); ?>" class="form-control" name="itemExpireDate" placeholder="Insert Item Selling Price " required >
                  </div>

                  <div class="form-group">
                    <label for="itemExpireDateAction">Action in the event of expiry  </label>
                    <select disabled name="itemExpireDateAction" required class="form-control">
                      <option value="">-- Select --</option>
                      <option <?php if($dataArr['itemExpireDateAction']==1){ echo "selected"; } ?> value="1">Allow Sale</option>
                      <option <?php if($dataArr['itemExpireDateAction']==0){ echo "selected"; } ?> value="0">Prevent Sale</option>
                    </select>
                  </div>

                  <div class="form-group">

                    <label for="itemPhoto">Item Photo </label><br>

                    <img src="../item_images/<?php echo $dataArr['itemPhoto'];?>" class="img-thumbnail" width="200px">
                  </div>

                </div>
              </div>
              <?php } } ?>
              <hr>
               
            </div>
            <!-- ./box-body -->
            <div class="box-footer">
              
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <?php $my_tools->myFooter(); ?>

</div>
<!-- ./wrapper -->

<?php include('js.php');?>

</body>
</html>
