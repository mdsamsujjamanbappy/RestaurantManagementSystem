<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $my_tools->title();?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php include('css.php');?>
  <!-- DataTables -->
  <link rel="stylesheet" href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">  

 </head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php $my_tools->logoArea();?>
    
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <?php include('messages.php');?>
          <!-- Notifications: style can be found in dropdown.less -->
          <?php include('notifications.php');?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php $my_tools->executiveImage();?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php $my_tools->exFullname();?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php $my_tools->executiveImage();?>" class="img-circle" alt="User Image">

                <p>
                  <?php $my_tools->exFullname();?>
                </p>
              </li>
                  <?php $my_tools->profileLink();?>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <?php $my_tools->control_sidebar();?>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php $my_tools->executiveImage();?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php $my_tools->exFullname();?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <?php include('search_form.php');?>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <?php $my_tools->executiveMenu();?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Sales List </h3>

              <div class="box-tools pull-right">
                
                <div class="btn-group">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
               <table id="example1" class="table table-bordered table-striped table-responsive">
                <thead>
                <tr>
                  <th width="12%">Invoice Number</th>
                  <th>Customer Name</th>
                  <th width="14%">Total Amount</th>
                  <th width="10%">Date</th>
                  <th width="12%">Time</th>
                  <th width="12%">Payment Type</th>
                  <th width="12%">Executive</th>
                  <th width="6%">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  $exId = $_SESSION['exRMSId'];
                  $results = $db_handle->getSalesListForExecutivePanel($exId);
                  $i=0;
                  $trow=count($results);
                  if($trow>0){
                   foreach($results as $dataArr) {
                    ++$i;
                    $tot_price=0;
                    $results1 = $db_handle->getPOSSalesItemList($dataArr["invoiceNumber"]);
                    if(count($results1)>0){
                     foreach($results1 as $dataArr1) {
                      $tot_price+=(($dataArr1["quantity"])*($dataArr1["unitPrice"]));
                      }
                    }

                ?>
                <tr class="">
                  <td>INV-<?php echo sprintf('%06u', ($dataArr["invoiceNumber"])); ?></td>
                  <td><?php echo ($dataArr["customerName"]); ?></td>
                  <td><b><?php echo CURRENCY1.round($tot_price,2).CURRENCY; ?></b></td>
                  <td><?php echo ($dataArr["invoiceDate"]); ?></td>
                  <td><?php echo ($dataArr["invoiceTime"]); ?></td>
                  <td><?php if(($dataArr["paymentType"])==1){ echo "Cash Payment";}else{ echo "Undefined"; } ?></td>
                  <td><?php echo ($dataArr["billby"]); ?></td>
                  <td>
                        <a target="_NEW" href="view_invoice.php?rid=<?php echo base64_encode(($dataArr["invoiceNumber"])); ?>" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="View Details" ><i class="fa fa-eye"></i></a>
                        <!-- <a href="#" data-toggle="tooltip" data-placement="top" title="Edit Information" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> -->
                  </td>
                </tr>

                <?php } } ?>
              </tbody>
              </table>

            </div>

            </div>
            <!-- ./box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>

    </section>
    <!-- /.content -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $my_tools->myFooter(); ?>

</div>
<!-- ./wrapper -->

<?php include('js.php');?>
<!-- DataTables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $('#example1').DataTable({
        "order": [[ 0, "desc" ]]
    })

  })
</script>
</body>
</html>
