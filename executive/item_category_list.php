<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $my_tools->title();?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php include('css.php');?>
 </head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php $my_tools->logoArea();?>
    
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <?php include('messages.php');?>
          <!-- Notifications: style can be found in dropdown.less -->
          <?php include('notifications.php');?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php $my_tools->executiveImage();?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php $my_tools->exFullname();?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php $my_tools->executiveImage();?>" class="img-circle" alt="User Image">

                <p>
                  <?php $my_tools->exFullname();?>
                </p>
              </li>
                  <?php $my_tools->profileLink();?>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <?php $my_tools->control_sidebar();?>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php $my_tools->executiveImage();?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php $my_tools->exFullname();?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <?php include('search_form.php');?>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <?php $my_tools->executiveMenu();?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-th"></i> Item Category List</h3>

              <div class="box-tools pull-right">
                
               <div class="btn-group">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
               <table id="example1" class="table table-hover table-bordered table-striped table-responsive">
                <thead>
                <tr>
                  <th width="10%">Serial</th>
                  <th>Category Name</th>
                  <th width="45%">Category Description</th>
                  <th width="6%">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  $results = $db_handle->getItemCategoryList();
                  $i=0;
                  $trow=count($results);
                  if($trow>0){
                   foreach($results as $dataArr) {
                    ++$i;
                ?>
                <tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo ($dataArr["categoryName"]); ?></td>
                  <td><?php echo ($dataArr["categoryDescription"]); ?></td>
                  <td>
                  <a class="btn btn-success btn-sm" href="#" data-toggle="modal" data-target="#view_itemcategory<?php echo $i; ?>" ><i class="fa fa-eye"></i></a>
                  </td>
                </tr>

                <div class="modal fade" id="view_itemcategory<?php echo $i; ?>">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-server"></i> Category Details Information</h4>
                      </div>
                      <div class="modal-body">
                        <div class="form-group">
                          <label for="categoryName">Category Name</label>
                          <input type="text" disabled id="categoryName" class="form-control" value="<?php echo $dataArr['categoryName']; ?>"  >
                        </div>
                        <div class="form-group">
                          <label for="categoryDescription">Category Description</label>
                          <textarea disabled name="categoryDescription" id="categoryDescription" class="form-control"><?php echo $dataArr['categoryDescription']; ?></textarea>
                        </div>
                        <br>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
                      </div>
                       
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

                <?php } } ?>
              </tbody>
              </table>

            </div>

            </div>
            <!-- ./box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $my_tools->myFooter(); ?>

</div>
<!-- ./wrapper -->

<?php include('js.php');?>

</body>
</html>
