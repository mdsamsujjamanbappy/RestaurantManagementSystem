<?php 
require_once 'header_link.php'; 
if(empty($_SESSION['tmpPOSNumber'])){
    $_SESSION['tmpPOSNumber'] = rand(10,99999999);
  } 
?>

<!-- DataTables -->
<link rel="stylesheet" href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">  

<table  id="example1"  class="table table-responsive table-hover table-bordered" style="font-size:12px;">
  <thead>
    <tr>
      <th>Item</th>
      <th width="15%"><center>Unit Price</center></th>
      <th width="15%"><center>Quantity</center></th>
      <th width="15%"><center>Total</center></th>
      <th width="8%">#</th>
    </tr>
  </thead>
  <tbody>
   <?php
    $results = $db_handle->getPOSTmpItemList($_SESSION['tmpPOSNumber']);
    $tot = 0;
    $trow=count($results);
    if($trow>0){
     foreach($results as $dataArr) {
        $tot+=($dataArr['quantity']*$dataArr['unitPrice']);
    ?>
    <tr>
      <td><?php echo $dataArr['itemName'];?></td>
      <td><center><?php echo CURRENCY1.($dataArr['unitPrice']).CURRENCY;?></center></td>
      <td>
        <center>
          <div class="input-group input-group-sm">
            <span class="input-group-btn item-control-btns-wrapper">
              <button class="btn btn-default item-reduce"  value="<?php echo ($dataArr["id"]); ?>" onclick="decreaseItemQuantityToPOS(this.value);" >-</button>
              <button class="btn btn-default" style="width:50px;"><center><?php echo ($dataArr['quantity']);?></center></button>
              <button class="btn btn-default item-add"   value="<?php echo ($dataArr["id"]); ?>" onclick="increaseItemQuantityToPOS(this.value);" >+</button>
            </span>
          </div>
        </center>
      </td>
      <td><center><?php echo CURRENCY1.($dataArr['quantity']*$dataArr['unitPrice']).CURRENCY;?></center></td>
      <td>
        <button value="<?php echo ($dataArr["id"]); ?>" onclick="removeItemPOSList(this.value);" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
      </td>
    </tr>
    <?php } } ?>

  </tbody>
    <tfoot>
    <tr>
      <th colspan="2"></th>
      <th style="text-align:right">Total Amount</th>
      <th>
      <center><?php echo CURRENCY1; ?><?php echo ($tot); ?><?php echo CURRENCY; ?></center>

      <input id="subtotalvalue1" value="<?php echo ($tot); ?>" hidden >
      <input id="countItemList" value="<?php echo (count($results)); ?>" hidden >
      
      </th>
      <th></th>
    </tr>
  </tfoot>
</table>

<!-- DataTables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>



<script>
  $(function () {
    $('#example1').DataTable({
      "pageLength": 7
    })

  })
</script>