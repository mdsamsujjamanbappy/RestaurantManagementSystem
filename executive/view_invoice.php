<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $my_tools->title();?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php include('css.php');?>
  <link rel="stylesheet" href="../dist/css/invoice-print.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <?php $my_tools->logoArea();?>
    
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <?php include('messages.php');?>
          <!-- Notifications: style can be found in dropdown.less -->
          <?php include('notifications.php');?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php $my_tools->executiveImage();?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php $my_tools->exFullname();?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php $my_tools->executiveImage();?>" class="img-circle" alt="User Image">

                <p>
                  <?php $my_tools->exFullname();?>
                </p>
              </li>
                  <?php $my_tools->profileLink();?>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <?php $my_tools->control_sidebar();?>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php $my_tools->executiveImage();?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php $my_tools->exFullname();?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <?php include('search_form.php');?>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <?php $my_tools->executiveMenu();?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

   
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
        
          <div class="box box-success print-invoice">
            <div class="box-header with-border">
              <h3 class="box-title"> <i class="fa fa-file-text"></i> Invoice details</h3>

              <div class="box-tools pull-right">
                <!-- <a href="sales.php"><button type="button" class="btn btn-success btn-sm" ><i class="fa fa-reorder"></i> Sales List</button></a> -->
                <div class="btn-group">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
              <div class="col-md-6 col-md-offset-2 col-sm-12 col-xs-12">
                <?php 
                   $invoiceNumber=base64_decode($_GET['rid']);
                   $results = $db_handle->getInvoiceDetailsById($invoiceNumber);
                   if(count($results)>0){
                  ?>
                  <div class="animated zoomIn">
                    <div class="">
                      <div id="printMe">
                        <?php 
                         foreach($results as $dataArr) {
                            $invoiceDate = $dataArr['invoiceDate'];
                            $invoiceTime = $dataArr['invoiceTime'];
                            $discountAmount = $dataArr['discountAmount'];
                            $vatAmount = $dataArr['vatAmount'];
                            $deliveryCharge = $dataArr['deliveryCharge'];
                         }
                         ?>
                        <div class="row invoice-list">

                          <div class="text-center invoice-header">
                            <h1><b><?php echo invoiceCompanyTitle();?></b></h1>
                            <h4><?php echo invoiceCompanyaddress();?></h4>
                            <h4><?php echo invoiceCompanyPhone();?></h4>
                            <h4><?php echo invoiceCompanyEmail();?></h4>
                          </div>
                          <!-- <div class="invoice_gap"></div> -->
                         <div class="col-lg-6 col-sm-6">
                            <h4>INVOICE NUMBER : <strong><?php echo sprintf('%06u', $invoiceNumber); ?></strong></h4>
                            <p></p>
                         </div>
                         <div class="col-lg-6 col-sm-6">
                            <h4>Date & Time: <?php echo $invoiceDate." ".$invoiceTime;?></h4>
                         </div>
                      </div>
                          <!-- <div class="invoice_gap"></div> -->

                          <table class="table border-table">
                          <tr style="font-weight:bold;">
                              <td class="mobile-hide" width="3%">SN</td>
                              <td>Item Name</td>
                              <td><center>Qty</center></td>
                              <td><center>Rate</center></td>
                              <td class="mobile-hide"><center>Sub-total</center></td>
                         </tr>
                         <tbody>
                          <?php
                           $results = $db_handle->getPOSSalesItemList($invoiceNumber);
                          $i=0;
                          $tot_price = 0;
                          $trow=count($results);
                          if($trow>0){
                           foreach($results as $dataArr) {
                            $tot_price+=(($dataArr["quantity"])*(round(($dataArr["unitPrice"]),2)));
                          ?>
                          <tr>
                              <td class="mobile-hide"><?php echo ++$i; ?></td>
                              <td><?php echo ($dataArr["itemName"]); ?></td>
                              <td><center><?php echo ($dataArr["quantity"]); ?><span class="posqty"><?php echo UNIT;?></span></center></td>
                              <td><center><?php echo CURRENCY1.round($dataArr["unitPrice"],2).CURRENCY; ?></center></td>
                              <td class="mobile-hide"><center><?php echo CURRENCY1.(($dataArr["quantity"])*(round(($dataArr["unitPrice"]),2))).CURRENCY; ?></center></td>
                          </tr>
                           <?php 
                             
                            } 
                           }else{
                              echo "<tr><td colspan='6' > <center>No data are matching</center></td></tr>";
                            } 
                            ?>
                            </tbody>
                          </table>
                          <div class="invoice_divider"></div>
                           <div class="row amounts">
                              <div class="col-lg-6 invoice-block col-lg-offset-3">
                                  <ul class="to_do amounts">
                                      <li><strong> Sub Total   Amount  : </strong><b><?php echo CURRENCY1.round($tot_price,2).CURRENCY; ?></b></li>
                                      <li><strong>VAT Amount</strong> (<?php echo ($vatAmount)."%"; ?>) : <b>  <?php echo CURRENCY1.round((($tot_price/100)*$vatAmount),2).CURRENCY; ?></b></li>
                                      <li><strong>Discount Amount</strong> :<b> <?php echo CURRENCY1.round(($discountAmount),2).CURRENCY; ?></b></li>
                                      <?php if($deliveryCharge>0){ ?>
                                      <li><strong> Delivery Charge  : </strong><b> <?php echo CURRENCY1.round(($deliveryCharge),2).CURRENCY; ?></b></li>
                                      <?php } ?>
                                      <li><strong> Grand Total  : </strong><b> <?php echo CURRENCY1.round(((((($tot_price/100)*$vatAmount)+$tot_price)-$discountAmount)+$deliveryCharge),2).CURRENCY; ?></b></li>
                                  </ul>
                              </div>
                          </div>
                          <center><h4 class="thankYou">**** Thank you ****</h4></center>
                         
                          <center><button class="btn btn-default btn-md print-button" onclick="printme()" style="margin-top:20px"><i class="fa fa-print"></i> Print Invoice </button></center>
                        </div>
                    </div>
                  </div>
                  <?php }else{ ?>
                  <div class="animated zoomIn">
                            <h1 style="color:red;" class="btn-app"><b>Invalid Invoice ID.</b></h1>
                  </div>
                  <?php } ?>
                  
                </div>
              </div>

            </div>
            <!-- ./box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>


    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php $my_tools->myFooter(); ?>

</div>
<!-- ./wrapper -->

<?php include('js.php');?>
<!-- DataTables -->
<script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script> 
  function printme(){
      window.print();
  }
</script>
</body>
</html>
