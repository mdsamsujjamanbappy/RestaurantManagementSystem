<?php
require_once("controller.php");

class myFunctions extends MSBController{
	
	/* All Global Function */

	function getTotalRowNumber($tbname){
		$tbname = $this->makeSecure($tbname);
  		$query ="SELECT * FROM $tbname ";
  		return $this->getRowNumber($query);
	}

	function checkDependency($tbname,$colname,$id){
		$tbname = $this->makeSecure($tbname);
		$colname = $this->makeSecure($colname);
		$id = $this->makeSecure($id);

	  	$query ="SELECT * FROM $tbname WHERE $colname ='$id'";
	  	return $this->getRowNumber($query);
	}
	
	// Function to get the client IP address
	function get_client_ip() {
	    $ipaddress = '';
	    if (isset($_SERVER['HTTP_CLIENT_IP']))
	        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_FORWARDED']))
	        $ipaddress = $_SERVER['HTTP_FORWARDED'];
	    else if(isset($_SERVER['REMOTE_ADDR']))
	        $ipaddress = $_SERVER['REMOTE_ADDR'];
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}

	function insertActivityLog($id,$activity, $date, $time){
		$id=$this->makeSecure($id);
		$activity=$this->makeSecure($activity);
		$date=$this->makeSecure($date);
		$time=$this->makeSecure($time);
		$ip=$this->get_client_ip();

		$query="INSERT INTO tbactivity_logs (user_id, activity, adate, time,loginIp) VALUES('$id','$activity','$date','$time','$ip')";
	  	return $this->insertData($query);
	}

	/* Manage Area Information */ 

	function getAreaList(){
	  	$query ="SELECT * FROM tbarealist";
	  	return $this->getData($query);
	}

	function saveAreaInformation($name,$description){
		$name = $this->makeSecure($name);
		$description = $this->makeSecure($description);
  		$query ="INSERT INTO  tbarealist(areaName, areaDescription) VALUES ('$name','$description')";
  		return $this->insertData($query);
	}

	function updateAreaInformation($id,$name,$description){
		$id = $this->makeSecure($id);
		$name = $this->makeSecure($name);
		$description = $this->makeSecure($description);
  		$query ="UPDATE tbarealist SET areaName='$name', areaDescription='$description' WHERE id='$id'";
  		return $this->updateData($query);
	}

	function deleteAreaInformation($id){
		$id = $this->makeSecure($id);
  		$query ="DELETE FROM tbarealist WHERE id='$id'";
  		return $this->deleteData($query);
	}


	/* Manage Table Information */ 

	function getTableList(){
	  	$query ="SELECT ttl.*, tal.areaName FROM tbtablelist ttl LEFT JOIN tbarealist tal ON tal.id=ttl.tableAreaId";
	  	return $this->getData($query);
	}

	function getTableListById($id){
		$id = $this->makeSecure($id);
	  	$query ="SELECT ttl.*, tal.areaName FROM tbtablelist ttl LEFT JOIN tbarealist tal ON tal.id=ttl.tableAreaId WHERE ttl.id='$id'";
	  	return $this->getData($query);
	}

	function saveTableInformation($tableName,$tableAreaId,$tableMaxSeats,$tableDescription,$status){
		$tableName = $this->makeSecure($tableName);
		$tableAreaId = $this->makeSecure($tableAreaId);
		$tableMaxSeats = $this->makeSecure($tableMaxSeats);
		$tableDescription = $this->makeSecure($tableDescription);
		$status = $this->makeSecure($status);
  		$query ="INSERT INTO  tbtablelist(tableName, tableMaxSeats, tableStatus, tableDescription, tableAreaId) VALUES ('$tableName', '$tableMaxSeats', '$tableStatus', '$tableDescription', '$tableAreaId')";
  		return $this->insertData($query);
	}

	function updateTableInformation($id,$tableName,$tableAreaId,$tableMaxSeats,$tableDescription,$status){
		$id = $this->makeSecure($id);
		$tableName = $this->makeSecure($tableName);
		$tableAreaId = $this->makeSecure($tableAreaId);
		$tableMaxSeats = $this->makeSecure($tableMaxSeats);
		$tableDescription = $this->makeSecure($tableDescription);
		$tableStatus = $this->makeSecure($status);
  		$query ="UPDATE tbtablelist SET tableName='$tableName', tableMaxSeats='$tableMaxSeats', tableStatus='$tableStatus', tableDescription='$tableDescription', tableAreaId='$tableAreaId' WHERE id='$id'";
  		return $this->updateData($query);
	}

	function deleteTableInformation($id){
		$id = $this->makeSecure($id);
  		$query ="DELETE FROM tbtablelist WHERE id='$id'";
  		return $this->deleteData($query);
	}


	/* Manage Kitchen Information */ 

	function getKitchenList(){
	  	$query ="SELECT * FROM tbkitchenlist";
	  	return $this->getData($query);
	}

	function saveKitchenInformation($name,$description){
		$name = $this->makeSecure($name);
		$description = $this->makeSecure($description);
  		$query ="INSERT INTO  tbkitchenlist(kitchenName, kitchenDescription) VALUES ('$name','$description')";
  		return $this->insertData($query);
	}

	function updateKitchenInformation($id,$name,$description){
		$id = $this->makeSecure($id);
		$name = $this->makeSecure($name);
		$description = $this->makeSecure($description);
  		$query ="UPDATE tbkitchenlist SET kitchenName='$name', kitchenDescription='$description' WHERE id='$id'";
  		return $this->updateData($query);
	}

	function deleteKitchenInformation($id){
		$id = $this->makeSecure($id);
  		$query ="DELETE FROM tbkitchenlist WHERE id='$id'";
  		return $this->deleteData($query);
	}

	/* Manage Item Category Information */ 

	function getItemCategoryList(){
	  	$query ="SELECT * FROM tbitemcategory";
	  	return $this->getData($query);
	}

	function saveCategoryInformation($name,$description){
		$name = $this->makeSecure($name);
		$description = $this->makeSecure($description);
  		$query ="INSERT INTO  tbitemcategory(categoryName, categoryDescription) VALUES ('$name','$description')";
  		return $this->insertData($query);
	}

	function updateCategoryInformation($id,$name,$description){
		$id = $this->makeSecure($id);
		$name = $this->makeSecure($name);
		$description = $this->makeSecure($description);
  		$query ="UPDATE tbitemcategory SET categoryName='$name', categoryDescription='$description' WHERE id='$id'";
  		return $this->updateData($query);
	}

	function deleteCategoryInformation($id){
		$id = $this->makeSecure($id);
  		$query ="DELETE FROM tbitemcategory WHERE id='$id'";
  		return $this->deleteData($query);
	}

	/* Manage Item Department Information */ 

	function getItemDepartmentList(){
	  	$query ="SELECT * FROM tbitemdepartment";
	  	return $this->getData($query);
	}

	function saveDepartmentInformation($name,$description){
		$name = $this->makeSecure($name);
		$description = $this->makeSecure($description);
  		$query ="INSERT INTO  tbitemdepartment(departmentName, departmentDescription) VALUES ('$name','$description')";
  		return $this->insertData($query);
	}

	function deleteDepartmentInformation($id){
		$id = $this->makeSecure($id);
  		$query ="DELETE FROM tbitemdepartment WHERE id='$id'";
  		return $this->deleteData($query);
	}

	/* Manage Item Information */ 

	function getItemList(){
	  	$query ="SELECT til.*, tic.categoryName FROM tbitemlist til LEFT JOIN tbitemcategory tic ON tic.id=til.itemCategoryId";
	  	return $this->getData($query);
	}

	function getItemListById($id){
	  	$query ="SELECT til.*, tic.categoryName FROM tbitemlist til LEFT JOIN tbitemcategory tic ON tic.id=til.itemCategoryId WHERE til.id='$id'";
	  	return $this->getData($query);
	}

	function getItemDetailsByName($keyword){
	  	$query ="SELECT til.*, tic.categoryName FROM tbitemlist til LEFT JOIN tbitemcategory tic ON tic.id=til.itemCategoryId WHERE til.itemName='$keyword'";
	  	return $this->getData($query);
	}

	function getItemListByCatId($id){
	  	$query ="SELECT til.*, tic.categoryName FROM tbitemlist til LEFT JOIN tbitemcategory tic ON tic.id=til.itemCategoryId WHERE til.itemCategoryId='$id'";
	  	return $this->getData($query);
	}

	function getAutocompleteItemList($keyword){
	  	$query ="SELECT * FROM tbitemlist WHERE itemName LIKE '%$keyword%'";
	  	return $this->getData($query);
	}

	function saveItemInformation($itemName,$itemCode,$itemSellPrice,$itemStockManagement,$itemQuantity,$itemDescription,$itemCategoryId,$itemDepartmentId,$itemType,$itemExpireDateAction,$itemExpireDate,$itemStockAlert,$image){
		$itemName = $this->makeSecure($itemName);
		$itemCode = $this->makeSecure($itemCode);
		$itemSellPrice = $this->makeSecure($itemSellPrice);
		$itemStockManagement = $this->makeSecure($itemStockManagement);
		$itemQuantity = $this->makeSecure($itemQuantity);
		$itemDescription = $this->makeSecure($itemDescription);
		$itemCategoryId = $this->makeSecure($itemCategoryId);
		$itemDepartmentId = $this->makeSecure($itemDepartmentId);
		$itemType = $this->makeSecure($itemType);
		$itemExpireDateAction = $this->makeSecure($itemExpireDateAction);
		$itemExpireDate = $this->makeSecure($itemExpireDate);
		$itemStockAlert = $this->makeSecure($itemStockAlert);
		$image = $this->makeSecure($image);
  		$query ="INSERT INTO  tbitemlist(itemName, itemCategoryId, itemDepartmentId, itemType, itemDescription, itemCode, itemSellPrice, itemQuantity, itemExpireDate, itemExpireDateAction, itemStockManagement, itemStockAlert, itemPhoto) VALUES ('$itemName','$itemCategoryId','$itemDepartmentId','$itemType','$itemDescription','$itemCode','$itemSellPrice','$itemQuantity','$itemExpireDate','$itemExpireDateAction','$itemStockManagement','$itemStockAlert','$image')";
  		return $this->insertData($query);
	}

	function updateItemInformation($id,$itemName,$itemCode,$itemSellPrice,$itemStockManagement,$itemQuantity,$itemDescription,$itemCategoryId,$itemDepartmentId,$itemExpireDateAction,$itemExpireDate,$itemStockAlert,$image){
		$id = $this->makeSecure($id);
		$itemName = $this->makeSecure($itemName);
		$itemCode = $this->makeSecure($itemCode);
		$itemSellPrice = $this->makeSecure($itemSellPrice);
		$itemStockManagement = $this->makeSecure($itemStockManagement);
		$itemQuantity = $this->makeSecure($itemQuantity);
		$itemDescription = $this->makeSecure($itemDescription);
		$itemCategoryId = $this->makeSecure($itemCategoryId);
		$itemDepartmentId = $this->makeSecure($itemDepartmentId);
		$itemExpireDateAction = $this->makeSecure($itemExpireDateAction);
		$itemExpireDate = $this->makeSecure($itemExpireDate);
		$itemStockAlert = $this->makeSecure($itemStockAlert);
		$image = $this->makeSecure($image);
  		$query ="UPDATE tbitemlist SET itemName='$itemName', itemCategoryId='$itemCategoryId', itemDepartmentId='$itemDepartmentId', itemDescription='$itemDescription', itemCode='$itemCode', itemSellPrice='$itemSellPrice', itemQuantity='$itemQuantity', itemExpireDate='$itemExpireDate', itemExpireDateAction='$itemExpireDateAction', itemStockManagement='$itemStockManagement', itemStockAlert='$itemStockAlert', itemPhoto='$image' WHERE id='$id'";
  		return $this->updateData($query);
	}

	function deleteItemInformation($id){
		$id = $this->makeSecure($id);
  		$query ="DELETE FROM tbitemlist WHERE id='$id'";
  		return $this->deleteData($query);
	}

	/* Manage Supplier */ 

	function getSupplierList(){
	  	$query ="SELECT * FROM tbsupplier";
	  	return $this->getData($query);
	}

	function saveSupplierInformation($supplierName,$supplierPhone,$supplierEmail,$supplierAddress,$supplierDescription){
		$supplierName = $this->makeSecure($supplierName);
		$supplierPhone = $this->makeSecure($supplierPhone);
		$supplierEmail = $this->makeSecure($supplierEmail);
		$supplierAddress = $this->makeSecure($supplierAddress);
		$supplierDescription = $this->makeSecure($supplierDescription);

  		$query ="INSERT INTO  tbsupplier (supplierName, supplierPhone, supplierEmail, supplierAddress, supplierDescription) VALUES ('$supplierName','$supplierPhone','$supplierEmail','$supplierAddress','$supplierDescription')";
  		return $this->insertData($query);
	}

	function updateSupplierInformation($id,$supplierName,$supplierPhone,$supplierEmail,$supplierAddress,$supplierDescription){
		$id = $this->makeSecure($id);
		$supplierName = $this->makeSecure($supplierName);
		$supplierPhone = $this->makeSecure($supplierPhone);
		$supplierEmail = $this->makeSecure($supplierEmail);
		$supplierAddress = $this->makeSecure($supplierAddress);
		$supplierDescription = $this->makeSecure($supplierDescription);

  		$query ="UPDATE tbsupplier SET supplierName='$supplierName', supplierPhone='$supplierPhone', supplierEmail='$supplierEmail', supplierAddress='$supplierAddress', supplierDescription='$supplierDescription' WHERE id='$id'";
  		return $this->updateData($query);
	}

	/* Manage Supplies Item */ 

	function getTmpSuppliesList($id){
		$id = $this->makeSecure($id);
	  	$query ="SELECT ttsi.*,til.itemName,til.itemCategoryId,til.itemType,tic.categoryName FROM tbtmpsuppliesitems ttsi LEFT JOIN tbitemlist til ON til.id=ttsi.itemId LEFT JOIN tbitemcategory tic ON tic.id=til.itemCategoryId WHERE tmpId='$id'";
	  	return $this->getData($query);
	}

	function saveTmpSuppliesItem($tmpSuppliesId,$itemId,$purchasePrice,$quantity){
		$tmpSuppliesId = $this->makeSecure($tmpSuppliesId);
		$itemId = $this->makeSecure($itemId);
		$purchasePrice = $this->makeSecure($purchasePrice);
		$quantity = $this->makeSecure($quantity);

  		$query ="INSERT INTO tbtmpsuppliesitems (tmpId, itemId, quantity, purchasePrice) VALUES('$tmpSuppliesId','$itemId','$quantity','$purchasePrice')";
  		return $this->insertData($query);
	}

	function deleteTmpSuppliesItem($id){
		$id = $this->makeSecure($id);
  		$query ="DELETE FROM tbtmpsuppliesitems WHERE id='$id'";
  		return $this->deleteData($query);
	}

	function getNewSuppliesId(){
		$query = "SELECT MAX(suppliesId) as maximum FROM tbsupplieslist";
  		return $this->getData($query);
	}

	function saveSuppliesItem($suppliesId,$itemId,$quantity,$purchasePrice){
		$suppliesId = $this->makeSecure($suppliesId);
		$itemId = $this->makeSecure($itemId);
		$purchasePrice = $this->makeSecure($purchasePrice);
		$quantity = $this->makeSecure($quantity);

  		$query ="INSERT INTO tbsuppliesitems (suppliesId, itemId, quantity, purchasePrice) VALUES('$suppliesId','$itemId','$quantity','$purchasePrice')";
  		return $this->insertData($query);
	}

	function increaseItemQuantity($itemId,$quantity){
		$itemId = $this->makeSecure($itemId);
		$quantity = $this->makeSecure($quantity);

  		$query ="UPDATE tbitemlist SET itemQuantity = itemQuantity + '$quantity' WHERE id='$itemId'";
  		return $this->insertData($query);
	}

	function saveToSuppliesList($suppliesId,$supplierId, $userId){
		$suppliesId = $this->makeSecure($suppliesId);
		$supplierId = $this->makeSecure($supplierId);
		$userId = $this->makeSecure($userId);

  		$query ="INSERT INTO tbsupplieslist (suppliesId, supplierId, createdBy) VALUES('$suppliesId','$supplierId','$userId')";
  		return $this->insertData($query);
	}

	function getSupplyItemListBySupplyId($id){
		$id = $this->makeSecure($id);
	  	$query ="SELECT tsi.*,til.itemName,til.itemCategoryId,til.itemType,tic.categoryName FROM tbsuppliesitems tsi LEFT JOIN tbitemlist til ON til.id=tsi.itemId LEFT JOIN tbitemcategory tic ON tic.id=til.itemCategoryId WHERE suppliesId='$id'";
	  	return $this->getData($query);
	}

	function getSupplyDetailsBySupplyId($id){
		$id = $this->makeSecure($id);
	  	$query ="SELECT * FROM tbsupplieslist tsl  LEFT JOIN tbsupplier ts ON ts.id=tsl.supplierId WHERE suppliesId='$id'";
	  	return $this->getData($query);
	}

	function getSuppliesList(){
	  	$query ="SELECT tsl.*, ts.supplierName, ts.supplierPhone FROM tbsupplieslist tsl LEFT JOIN tbsupplier ts ON ts.id=tsl.supplierId";
	  	return $this->getData($query);
	}

	/* Expense Category */

	function getExpenseCategoryList(){
	  	$query ="SELECT * FROM tbexpensecategory";
	  	return $this->getData($query);
	}

	function saveExpenseCategoryInformation($categoryName, $categoryDescription){
		$categoryName = $this->makeSecure($categoryName);
		$categoryDescription = $this->makeSecure($categoryDescription);

  		$query ="INSERT INTO tbexpensecategory (categoryName, categoryDescription) VALUES('$categoryName','$categoryDescription')";
  		return $this->insertData($query);
	}

	function updateExpenseCategoryInformation($id,$categoryName, $categoryDescription){
		$id = $this->makeSecure($id);
		$categoryName = $this->makeSecure($categoryName);
		$categoryDescription = $this->makeSecure($categoryDescription);

  		$query ="UPDATE tbexpensecategory SET categoryName='$categoryName', categoryDescription='$categoryDescription' WHERE id='$id'";
  		return $this->updateData($query);
	}

	function deleteExpenseCategoriesInformation($id){
		$id = $this->makeSecure($id);
  		$query ="DELETE FROM tbexpensecategory WHERE id='$id'";
  		return $this->deleteData($query);
	}

	/* Expense List */

	function getExpenseList(){
	  	$query ="SELECT tel.*,tec.categoryName FROM tbexpenselist tel LEFT JOIN tbexpensecategory tec ON tec.id=tel.categoryId ORDER BY expenseDate DESC";
	  	return $this->getData($query);
	}

	function getExpenseDetailsById($id){
		$id = $this->makeSecure($id);
	  	$query ="SELECT tel.*,tec.categoryName FROM tbexpenselist tel LEFT JOIN tbexpensecategory tec ON tec.id=tel.categoryId WHERE tel.id='$id'";
	  	return $this->getData($query);
	}

	function saveExpenseInformation($name,$expenseAmount,$expenseDate,$description,$categoryId,$reference,$attachment){
		$name = $this->makeSecure($name);
		$expenseAmount = $this->makeSecure($expenseAmount);
		$expenseDate = $this->makeSecure($expenseDate);
		$description = $this->makeSecure($description);
		$categoryId = $this->makeSecure($categoryId);
		$reference = $this->makeSecure($reference);
		$attachment = $this->makeSecure($attachment);

  		$query ="INSERT INTO tbexpenselist (name, categoryId, amount, reference, description, expenseDate, attachment) VALUES('$name','$categoryId','$expenseAmount','$reference','$description','$expenseDate','$attachment')";
  		return $this->insertData($query);
	}


	function updateExpenseInformation($id,$name,$expenseAmount,$expenseDate,$description,$categoryId,$reference,$attachment){
		$id = $this->makeSecure($id);
		$name = $this->makeSecure($name);
		$expenseAmount = $this->makeSecure($expenseAmount);
		$expenseDate = $this->makeSecure($expenseDate);
		$description = $this->makeSecure($description);
		$categoryId = $this->makeSecure($categoryId);
		$reference = $this->makeSecure($reference);
		$attachment = $this->makeSecure($attachment);

  		$query ="UPDATE tbexpenselist SET name='$name', categoryId='$categoryId', amount='$expenseAmount', reference='$reference', description='$description', expenseDate='$expenseDate', attachment='$attachment' WHERE id='$id'";
  		return $this->updateData($query);
	}

	/* Designation List */

	function getDesignationList(){
	  	$query ="SELECT * FROM tbdesignationlist";
	  	return $this->getData($query);
	}

	function saveDesignationInformation($name,$description){
		$name = $this->makeSecure($name);
		$description = $this->makeSecure($description);
  		$query ="INSERT INTO  tbdesignationlist(designationName, designationDescription) VALUES ('$name','$description')";
  		return $this->insertData($query);
	}

	function updateDesignationInformation($id,$name,$description){
		$id = $this->makeSecure($id);
		$name = $this->makeSecure($name);
		$description = $this->makeSecure($description);
  		$query ="UPDATE tbdesignationlist SET designationName='$name', designationDescription='$description' WHERE id='$id'";
  		return $this->updateData($query);
	}

	function deleteDesignationInformation($id){
		$id = $this->makeSecure($id);
  		$query ="DELETE FROM tbdesignationlist WHERE id='$id'";
  		return $this->deleteData($query);
	}

	/* Customer List */

	function getCustomerList(){
	  	$query ="SELECT * FROM tbcustomerinfo";
	  	return $this->getData($query);
	}

	function saveCustomerInformation($fullname,$phone,$email,$address){
		
		$fullname = $this->makeSecure($fullname);
		$phone = $this->makeSecure($phone);
		$email = $this->makeSecure($email);
		$address = $this->makeSecure($address);

  		$query ="INSERT INTO tbcustomerinfo (fullname, phone, email, address) VALUES('$fullname','$phone','$email','$address')";
  		return $this->insertData($query);
	}

	function updateCustomerInformation($id,$fullname,$phone,$email,$address){
		
		$id = $this->makeSecure($id);
		$fullname = $this->makeSecure($fullname);
		$phone = $this->makeSecure($phone);
		$email = $this->makeSecure($email);
		$address = $this->makeSecure($address);

  		$query ="UPDATE tbcustomerinfo SET fullname='$fullname', phone='$phone', email='$email', address='$address' WHERE id='$id'";
  		return $this->updateData($query);
	}

	function getCustomerPurchaseList($rid){
		$rid = $this->makeSecure($rid);
		$query ="SELECT ti.*, tc.fullname customerName,te.empName billby FROM tbinvoiceinfo ti LEFT JOIN tbcustomerinfo tc ON tc.id=ti.customerId LEFT JOIN tbemployeelist te ON te.id=ti.userId WHERE customerId='$rid' ORDER BY id DESC";
  		return $this->getData($query);
	}

	function deleteCustomerInformation($id){
		$id = $this->makeSecure($id);
  		$query ="DELETE FROM tbcustomerinfo WHERE id='$id'";
  		return $this->deleteData($query);
	}


	/* Employee List */

	function getEmployeeList(){
	  	$query ="SELECT tel.*, tu.userName , tdl.designationName FROM tbemployeelist tel LEFT JOIN tbusers tu ON tu.empId=tel.id LEFT JOIN tbdesignationlist tdl ON tdl.id=tel.empDesignationId";
	  	return $this->getData($query);
	}

	function getDailyBasisEmployeeList(){
	  	$query ="SELECT tel.*, tu.userName , tdl.designationName FROM tbemployeelist tel LEFT JOIN tbusers tu ON tu.empId=tel.id LEFT JOIN tbdesignationlist tdl ON tdl.id=tel.empDesignationId WHERE tel.empSalaryType='1'";
	  	return $this->getData($query);
	}

	function getEmployeeDetailsById($id){
	  	$query ="SELECT tel.*, tu.userName , tdl.designationName FROM tbemployeelist tel LEFT JOIN tbusers tu ON tu.empId=tel.id LEFT JOIN tbdesignationlist tdl ON tdl.id=tel.empDesignationId WHERE tel.id='$id'";
	  	return $this->getData($query);
	}

	function saveEmployeeInformation($empName,$empPhone,$empEmail,$empJoiningDate,$empAddress,$description,$empDesignationId,$empSalaryType,$salaryAmount,$empAccountStatus,$empGender,$image){
		
		$empName = $this->makeSecure($empName);
		$empPhone = $this->makeSecure($empPhone);
		$empEmail = $this->makeSecure($empEmail);
		$empJoiningDate = $this->makeSecure($empJoiningDate);
		$empAddress = $this->makeSecure($empAddress);
		$description = $this->makeSecure($description);
		$empDesignationId = $this->makeSecure($empDesignationId);
		$empSalaryType = $this->makeSecure($empSalaryType);
		$salaryAmount = $this->makeSecure($salaryAmount);
		$empAccountStatus = $this->makeSecure($empAccountStatus);
		$empGender = $this->makeSecure($empGender);
		$empImage = $this->makeSecure($image);

  		$query ="INSERT INTO tbemployeelist (empName, empPhone, empEmail, empDesignationId, empAddress, empDescription, empJoiningDate, empSalaryType, empAccountStatus, salaryAmount, empGender, empImage) VALUES('$empName','$empPhone','$empEmail','$empDesignationId','$empAddress','$description','$empJoiningDate','$empSalaryType','$empAccountStatus','$salaryAmount','$empGender','$empImage')";
  		return $this->insertData($query);
	}

	function updateEmployeeInformation($id,$empName,$empPhone,$empEmail,$empJoiningDate,$empAddress,$description,$empDesignationId,$empSalaryType,$salaryAmount,$empAccountStatus,$empGender,$image){
		
		$id = $this->makeSecure($id);
		$empName = $this->makeSecure($empName);
		$empPhone = $this->makeSecure($empPhone);
		$empEmail = $this->makeSecure($empEmail);
		$empJoiningDate = $this->makeSecure($empJoiningDate);
		$empAddress = $this->makeSecure($empAddress);
		$description = $this->makeSecure($description);
		$empDesignationId = $this->makeSecure($empDesignationId);
		$empSalaryType = $this->makeSecure($empSalaryType);
		$salaryAmount = $this->makeSecure($salaryAmount);
		$empAccountStatus = $this->makeSecure($empAccountStatus);
		$empGender = $this->makeSecure($empGender);
		$empImage = $this->makeSecure($image);

  		$query ="UPDATE tbemployeelist SET empName='$empName', empPhone='$empPhone', empEmail='$empEmail', empDesignationId='$empDesignationId', empAddress='$empAddress', empDescription='$description', empJoiningDate='$empJoiningDate', empSalaryType='$empSalaryType', empAccountStatus='$empAccountStatus', salaryAmount='$salaryAmount', empGender='$empGender', empImage='$empImage' WHERE id='$id'";
  		return $this->insertData($query);
	}

	function deleteExecutiveInformation($id){
		$id = $this->makeSecure($id);
  		$query ="DELETE FROM tbemployeelist WHERE id='$id'";
  		return $this->deleteData($query);
	}

	function getActivityLogs(){
  		$query ="SELECT tal.*,te.empName FROM tbactivity_logs tal LEFT JOIN tbusers tu ON tu.id=tal.user_id  LEFT JOIN tbemployeelist te ON te.id=tu.empId ORDER BY tal.acl_id DESC";
  		return $this->getData($query);
	}

	function checkDailySalaryRecords($empId,$date){
		$empId = $this->makeSecure($empId);
		$sdate = $this->makeSecure($date);

	  	$query ="SELECT * FROM tbemployeesalary WHERE empId ='$empId'&&sdate='$sdate'";
	  	return $this->getRowNumber($query);
	}

	function saveDailySalaryRecords($empId,$date,$salary){
		$empId = $this->makeSecure($empId);
		$date = $this->makeSecure($date);
		$salary = $this->makeSecure($salary);
  		$query ="INSERT INTO tbemployeesalary(empId, sdate, amount) VALUES ('$empId','$date','$salary')";
  		return $this->insertData($query);
	}

	function getMonthlyBasisEmployeeList(){
	  	$query ="SELECT tel.*, tu.userName , tdl.designationName FROM tbemployeelist tel LEFT JOIN tbusers tu ON tu.empId=tel.id LEFT JOIN tbdesignationlist tdl ON tdl.id=tel.empDesignationId WHERE tel.empSalaryType='0'";
	  	return $this->getData($query);
	}

	function checkMonthlySalaryRecords($empId,$month,$year){
		$empId = $this->makeSecure($empId);
		$month = $this->makeSecure($month);
		$year = $this->makeSecure($year);


	  	$query ="SELECT * FROM tbemployeesalary WHERE empId ='$empId' AND YEAR(sdate) = '$year' AND MONTH(sdate) = '$month'";
	  	return $this->getRowNumber($query);
	}

	function saveMonthlySalaryRecords($empId,$date,$salary){
		$empId = $this->makeSecure($empId);
		$date = $this->makeSecure($date);
		$salary = $this->makeSecure($salary);
  		$query ="INSERT INTO tbemployeesalary(empId, sdate, amount) VALUES ('$empId','$date','$salary')";
  		return $this->insertData($query);
	}

	
	function getMonthlyBasisEmployeeSalaryReport($month,$year){
	  	$query ="SELECT tel.*, tu.userName , tdl.designationName,tes.amount,tes.sdate FROM tbemployeelist tel LEFT JOIN tbusers tu ON tu.empId=tel.id LEFT JOIN tbdesignationlist tdl ON tdl.id=tel.empDesignationId  LEFT JOIN tbemployeesalary tes ON tes.empId=tel.id WHERE tel.empSalaryType='0' AND YEAR(sdate) = '$year' AND MONTH(sdate) = '$month' ";
	  	return $this->getData($query); 
	}

	
	function getDailyBasisEmployeeSalaryReport($fromDate,$toDate){
	  	$query ="SELECT tel.*, tu.userName , tdl.designationName,tes.amount,tes.sdate FROM tbemployeelist tel LEFT JOIN tbusers tu ON tu.empId=tel.id LEFT JOIN tbdesignationlist tdl ON tdl.id=tel.empDesignationId  LEFT JOIN tbemployeesalary tes ON tes.empId=tel.id WHERE tel.empSalaryType='1' AND sdate BETWEEN '$fromDate' AND '$toDate' ";
	  	return $this->getData($query); 
	}

	function getEmployeeRoleList(){
	  	$query ="SELECT tel.empName,tel.empPhone, tu.* , tdl.designationName FROM tbusers tu INNER JOIN tbemployeelist tel ON tu.empId=tel.id LEFT JOIN tbdesignationlist tdl ON tdl.id=tel.empDesignationId";
	  	return $this->getData($query);
	}

	function saveAccessRole($empId,$empName,$empPassword,$userType){
		$empId = $this->makeSecure($empId);
		$empName = $this->makeSecure($empName);
		$empPassword = $this->makeSecure($empPassword);
		$userType = $this->makeSecure($userType);
  		$query ="INSERT INTO tbusers(empId, userName, userPassword, userTypeId) VALUES ('$empId','$empName','$empPassword','$userType')";
  		return $this->insertData($query);
	}

	function deleteAccessRole($id){
		$id = $this->makeSecure($id);
  		$query ="DELETE FROM tbusers WHERE id='$id'";
  		return $this->deleteData($query);
	}


	/* POS */
	function saveItemInfoToList($tmpId,$itemId,$item_quantity,$price){
		$tmpId = $this->makeSecure($tmpId);
		$itemId = $this->makeSecure($itemId);
		$quantity = $this->makeSecure($item_quantity);
		$price = $this->makeSecure($price);
  		$query ="INSERT INTO tbtmppos_item(tmpId, itemId, quantity, unitPrice) VALUES ('$tmpId','$itemId','$quantity','$price')";
  		return $this->insertData($query);
	}

	function updateItemInfoToList($tmpId,$itemId,$item_quantity,$price){
		$tmpId = $this->makeSecure($tmpId);
		$itemId = $this->makeSecure($itemId);
		$quantity = $this->makeSecure($item_quantity);
		$price = $this->makeSecure($price);
  		$query ="UPDATE tbtmppos_item SET quantity=quantity+'$quantity', unitPrice='$price' WHERE  tmpId ='$tmpId'&&itemId ='$itemId'";
  		return $this->updateData($query);
	}


	function getPOSTmpItemList($tmpId){
		$tmpId = $this->makeSecure($tmpId);
  		$query ="SELECT ttpi.*,til.itemName,til.itemStockManagement FROM tbtmppos_item ttpi LEFT JOIN tbitemlist til ON til.id=ttpi.itemId LEFT JOIN tbitemcategory tic ON tic.id=til.itemCategoryId WHERE tmpId='$tmpId' ";
  		return $this->getData($query);
	}

	function getTmpPOSItemDetails($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT * FROM tbtmppos_item WHERE id='$id' ";
  		return $this->getData($query);
	}

	function checkItemToPOSTmp($tid,$id){
		$tid = $this->makeSecure($tid);
		$id = $this->makeSecure($id);

	  	$query ="SELECT * FROM tbtmppos_item WHERE tmpId ='$tid'&&itemId ='$id'";
	  	return $this->getRowNumber($query);
	}

	function increaseItemQuantityToPOSTmp($id){
		$id = $this->makeSecure($id);

	  	$query ="UPDATE tbtmppos_item SET quantity = quantity+1 WHERE id ='$id'";
	  	return $this->updateData($query);
	}


	function decreaseItemQuantityToPOSTmp($id){
		$id = $this->makeSecure($id);

	  	$query ="UPDATE tbtmppos_item SET quantity = quantity-1 WHERE id ='$id'";
	  	return $this->updateData($query);
	}

	function removeItemFromPOSTmp($id){
		$id = $this->makeSecure($id);

	  	$query ="DELETE FROM tbtmppos_item WHERE id ='$id'";
	  	return $this->deleteData($query);
	}

	function getTableListByAreaId($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT * FROM tbtablelist WHERE tableAreaId='$id' ";
  		return $this->getData($query);
	}

	function getBookedTableList(){
	  	$query ="SELECT tho.*,ttl.tableName, ttl.tableMaxSeats,ttl.tableStatus, tal.areaName FROM tbholdorder tho LEFT JOIN tbtablelist ttl ON ttl.id=tho.tableId LEFT JOIN tbarealist tal ON tal.id=ttl.tableAreaId ";
	  	return $this->getData($query);
	}

	function getHoldOrderData(){
	  	$query ="SELECT tho.*,ttl.tableName, ttl.tableMaxSeats,ttl.tableStatus, tal.areaName, te.empName FROM tbholdorder tho LEFT JOIN tbtablelist ttl ON ttl.id=tho.tableId LEFT JOIN tbarealist tal ON tal.id=ttl.tableAreaId  LEFT JOIN tbemployeelist te ON te.id=tho.waiterId";
	  	return $this->getData($query);
	}

	function getHoldOrderDataById($id){
	  	$query ="SELECT tho.*,ttl.tableName, ttl.tableMaxSeats,ttl.tableStatus, tal.areaName, te.empName FROM tbholdorder tho LEFT JOIN tbtablelist ttl ON ttl.id=tho.tableId LEFT JOIN tbarealist tal ON tal.id=ttl.tableAreaId  LEFT JOIN tbemployeelist te ON te.id=tho.waiterId WHERE tho.id='$id'";
	  	return $this->getData($query);
	}

	function saveHoldOrderInfo($tmpPOSNumber,$tableId,$waiterId,$holdType){
		$tmpPOSNumber = $this->makeSecure($tmpPOSNumber);
		$tableId = $this->makeSecure($tableId);
		$waiterId = $this->makeSecure($waiterId);
		$holdType = $this->makeSecure($holdType);
		
  		$query ="INSERT INTO tbholdorder(tmpPOSId, tableId, waiterId, holdType) VALUES ('$tmpPOSNumber','$tableId','$waiterId','$holdType')";
  		return $this->insertData($query);
	}

	function removeHoldIdFromDB($id){
		$id = $this->makeSecure($id);

	  	$query ="DELETE FROM tbholdorder WHERE id ='$id'";
	  	return $this->deleteData($query);
	}

	function checkBookedTable($tableId){
	  	$query ="SELECT * FROM tbholdorder WHERE tableId='$tableId'";
	  	return $this->getData($query);
	}

	function getNewInvoiceNumber(){
		$query = "SELECT MAX(invoiceNumber) as maximum FROM tbinvoiceinfo";
  		return $this->getData($query);
	}

	function tmpToSales($invoiceNumber,$itemId,$quantity,$unitPrice){
		$invoiceNumber = $this->makeSecure($invoiceNumber);
		$itemId = $this->makeSecure($itemId);
		$quantity = $this->makeSecure($quantity);
		$unitPrice = $this->makeSecure($unitPrice);

		$query ="INSERT INTO tbpossalesitems(invoiceNumber, itemId, quantity, unitPrice) VALUES ('$invoiceNumber','$itemId','$quantity','$unitPrice')";
  		return $this->insertData($query);
	}

	function updateDecreaseItemQuantity($itemId,$quantity){
		$itemId = $this->makeSecure($itemId);
		$quantity = $this->makeSecure($quantity);
  		$query ="UPDATE tbitemlist SET itemQuantity = itemQuantity - '$quantity' WHERE id= '$itemId'";
  		return $this->updateData($query);
	}

	function savePOSInvoice($invoiceNumber,$customerId,$discountAmount,$vatAmount,$deliveryCharge,$paidAmount,$invoiceDate,$invoiceTime,$userId,$paymentType){
		$invoiceNumber = $this->makeSecure($invoiceNumber);
		$customerId = $this->makeSecure($customerId);
		$discountAmount = $this->makeSecure($discountAmount);
		$vatAmount = $this->makeSecure($vatAmount);
		$deliveryCharge = $this->makeSecure($deliveryCharge);
		$paidAmount = $this->makeSecure($paidAmount);
		$invoiceDate = $this->makeSecure($invoiceDate);
		$invoiceTime = $this->makeSecure($invoiceTime);
		$userId = $this->makeSecure($userId);
		$paymentType = $this->makeSecure($paymentType);

		$query ="INSERT INTO tbinvoiceinfo(invoiceNumber, customerId, discountAmount, vatAmount, deliveryCharge, paidAmount, invoiceDate, invoiceTime, userId, paymentType) VALUES ('$invoiceNumber','$customerId','$discountAmount','$vatAmount','$deliveryCharge','$paidAmount','$invoiceDate','$invoiceTime','$userId','$paymentType')";
  		return $this->insertData($query);
	}

	function getInvoiceDetailsById($id){
		$id = $this->makeSecure($id);
		$query ="SELECT * FROM tbinvoiceinfo WHERE invoiceNumber='$id'";
  		return $this->getData($query);
	}


	function getPOSSalesItemList($invoiceNumber){
		$invoiceNumber = $this->makeSecure($invoiceNumber);
  		$query ="SELECT ttpi.*,til.itemName,til.itemStockManagement FROM tbpossalesitems ttpi LEFT JOIN tbitemlist til ON til.id=ttpi.itemId LEFT JOIN tbitemcategory tic ON tic.id=til.itemCategoryId WHERE invoiceNumber='$invoiceNumber' ";
  		return $this->getData($query);
	}

	//For Admin panel
	function getSalesList(){
		$query ="SELECT ti.*, tc.fullname customerName,te.empName billby FROM tbinvoiceinfo ti LEFT JOIN tbcustomerinfo tc ON tc.id=ti.customerId LEFT JOIN tbemployeelist te ON te.id=ti.userId ORDER BY id DESC";
  		return $this->getData($query);
	}

	//For Executive panel
	function getSalesListForExecutivePanel($id){
		$id = $this->makeSecure($id);
		$query ="SELECT ti.*, tc.fullname customerName,te.empName billby FROM tbinvoiceinfo ti LEFT JOIN tbcustomerinfo tc ON tc.id=ti.customerId LEFT JOIN tbemployeelist te ON te.id=ti.userId WHERE ti.userId='$id' ORDER BY id DESC";
  		return $this->getData($query);
	}

	// For Admin panel
	function getTodaysSaleList($todaysDate){
		$todaysDate = $this->makeSecure($todaysDate);
		$query ="SELECT ti.*, tc.fullname customerName,te.empName billby FROM tbinvoiceinfo ti LEFT JOIN tbcustomerinfo tc ON tc.id=ti.customerId LEFT JOIN tbemployeelist te ON te.id=ti.userId WHERE ti.invoiceDate='$todaysDate' ORDER BY ti.id DESC";
  		return $this->getData($query);
	}

	//For Executive panel
	function getTodaysSaleListForExecutivePanel($exid,$todaysDate){
		$exid = $this->makeSecure($exid);
		$todaysDate = $this->makeSecure($todaysDate);
		$query ="SELECT ti.*, tc.fullname customerName,te.empName billby FROM tbinvoiceinfo ti LEFT JOIN tbcustomerinfo tc ON tc.id=ti.customerId LEFT JOIN tbemployeelist te ON te.id=ti.userId WHERE ti.invoiceDate='$todaysDate' && ti.userId='$exid' ORDER BY ti.id DESC";
  		return $this->getData($query);
	}

	// For Admin Panel
	function getDateWiseSalesList($fromDate,$toDate){
		$fromDate = $this->makeSecure($fromDate);
		$toDate = $this->makeSecure($toDate);
		$query ="SELECT ti.*, tc.fullname customerName,te.empName billby FROM tbinvoiceinfo ti LEFT JOIN tbcustomerinfo tc ON tc.id=ti.customerId LEFT JOIN tbemployeelist te ON te.id=ti.userId WHERE invoiceDate BETWEEN '$fromDate' AND '$toDate' ORDER BY id DESC";
  		return $this->getData($query);
	}

	//For Executive Panel
	function getDateWiseSalesListForExecutivePanel($exid,$fromDate,$toDate){
		$exid = $this->makeSecure($exid);
		$fromDate = $this->makeSecure($fromDate);
		$toDate = $this->makeSecure($toDate);
		$query ="SELECT ti.*, tc.fullname customerName,te.empName billby FROM tbinvoiceinfo ti LEFT JOIN tbcustomerinfo tc ON tc.id=ti.customerId LEFT JOIN tbemployeelist te ON te.id=ti.userId WHERE ti.userId='$exid' AND ti.invoiceDate BETWEEN '$fromDate' AND '$toDate' ORDER BY id DESC";
  		return $this->getData($query);
	}

	function getTodaysExpenseList($todaysDate){
		$todaysDate = $this->makeSecure($todaysDate);
	  	$query ="SELECT tel.*,tec.categoryName FROM tbexpenselist tel LEFT JOIN tbexpensecategory tec ON tec.id=tel.categoryId WHERE expenseDate='$todaysDate'";
	  	return $this->getData($query);
	}


	function getDateWiseExpenseList($fromDate,$toDate){
		$fromDate = $this->makeSecure($fromDate);
		$toDate = $this->makeSecure($toDate);
	  	$query ="SELECT tel.*,tec.categoryName FROM tbexpenselist tel LEFT JOIN tbexpensecategory tec ON tec.id=tel.categoryId WHERE expenseDate BETWEEN '$fromDate' AND '$toDate'";
	  	return $this->getData($query);
	}

	function getCustomerWiseInvoiceList($id){
		$query ="SELECT * FROM tbinvoiceinfo WHERE customerId='$id'";
  		return $this->getData($query);
	}

	/* Dashboard */

	function getTodaysItemSaleList($todaysDate){
		$todaysDate = $this->makeSecure($todaysDate);
		$query ="SELECT distinct tpos.itemId,ti.invoiceDate  FROM tbinvoiceinfo ti LEFT JOIN tbpossalesitems tpos ON tpos.invoiceNumber=ti.invoiceNumber WHERE ti.invoiceDate='$todaysDate'";
  		return $this->getData($query);
	}

	function getItemSaleHistoryTotalQuantity($id,$date){
	    $id = $this->makeSecure($id);
	    $date = $this->makeSecure($date);
  		$query ="SELECT tbitemlist.itemName itemName, tbpossalesitems.*,tbinvoiceinfo.invoiceDate,tic.categoryName FROM tbpossalesitems LEFT JOIN tbitemlist ON tbpossalesitems.itemId=tbitemlist.id LEFT JOIN tbitemcategory tic ON tic.id=tbitemlist.itemCategoryId LEFT JOIN tbinvoiceinfo ON tbpossalesitems.invoiceNumber=tbinvoiceinfo.invoiceNumber  WHERE tbpossalesitems.itemId='$id'&&tbinvoiceinfo.invoiceDate='$date'";
  		return $this->getData($query);
  	}

	function getLastNDays($days, $format = 'd/m'){
	     date_default_timezone_set('Asia/Dhaka');

	      $m = date("m"); 
	      $de= date("d"); 
	      $y= date("Y");
	      $dateArray = array();
	      for($i=0; $i<=$days-1; $i++){
	          $dateArray[] = date($format, mktime(0,0,0,$m,($de-$i),$y)) ; 
	      }
	      return array_reverse($dateArray);
	  }

	function getSalesAmountHistoryByDate($date){
	    $date = $this->makeSecure($date);
  		$query ="SELECT tbitemlist.itemName itemName, tbpossalesitems.*,tbinvoiceinfo.invoiceDate FROM tbpossalesitems LEFT JOIN tbitemlist ON tbpossalesitems.itemId=tbitemlist.id LEFT JOIN tbinvoiceinfo ON tbpossalesitems.invoiceNumber=tbinvoiceinfo.invoiceNumber WHERE tbinvoiceinfo.invoiceDate='$date'";
  		return $this->getData($query);
  	}


}
?>
