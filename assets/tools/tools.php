<?php
class MyTools{
	function adminMenu(){
	$url=basename($_SERVER['PHP_SELF']);
	?>
	    <li class="<?php if($url=='dashboard.php'){ echo 'active';}?>"><a href="dashboard.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        <li><a href="pos.php"><i class="fa fa-shopping-cart"></i> <span> POS </span></a></li>
        <li class="<?php if($url=='sales.php'){ echo 'active';}?>"><a href="sales.php"><i class="fa fa-industry"></i> <span> Sales </span></a></li>
        
	    <li class="treeview <?php if($url=='item_list.php'||$url=='new_item.php'||$url=='item_category_list.php'||$url=='item_department_list.php'){ echo 'active';}?>">
	      <a href="#">
	        <i class="fa fa-cutlery"></i> <span>Menu Item Settings</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-left pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">
	        <li class="<?php if($url=='item_list.php'){ echo 'active';}?>"><a href="item_list.php"><i class="fa fa-hand-o-right"></i> Item List</a></li>
	        <li class="<?php if($url=='new_item.php'){ echo 'active';}?>"><a href="new_item.php"><i class="fa fa-hand-o-right"></i> Add New Item</a></li>
	        <li class="<?php if($url=='item_category_list.php'){ echo 'active';}?>"><a href="item_category_list.php"><i class="fa fa-hand-o-right"></i> Item Category List</a></li>
	        <li class="<?php if($url=='item_department_list.php'){ echo 'active';}?>"><a href="item_department_list.php"><i class="fa fa-hand-o-right"></i> Item Department List</a></li>
	      </ul>
	    </li>

	    <li class="treeview <?php if($url=='employee_list.php'||$url=='activity_logs.php'||$url=='payments.php'||$url=='designation_list.php'||$url=='access_role.php'||$url=='daily_basis_salary_report.php'||$url=='monthly_basis_salary_report.php'||$url=='daily_basis_salary.php'||$url=='monthly_basis_salary.php'){ echo 'active';}?>">
	      <a href="#">
	        <i class="fa fa-group"></i> <span> Manage Employee</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-left pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">
	        <li class="<?php if($url=='employee_list.php'){ echo 'active';}?>"><a href="employee_list.php"><i class="fa fa-hand-o-right"></i> Employee List</a></li>
	        <li class="<?php if($url=='access_role.php'){ echo 'active';}?>"><a href="access_role.php"><i class="fa fa-hand-o-right"></i> Login Access Role</a></li>
	        <li class="<?php if($url=='activity_logs.php'){ echo 'active';}?>"><a href="activity_logs.php"><i class="fa fa-hand-o-right"></i> Activity Logs</a></li>
	        <li class="<?php if($url=='payments.php'||$url=='daily_basis_salary_report.php'||$url=='monthly_basis_salary_report.php'||$url=='daily_basis_salary.php'||$url=='monthly_basis_salary.php'){ echo 'active';}?>"><a href="payments.php"><i class="fa fa-hand-o-right"></i> Payments</a></li>
	        <li class="<?php if($url=='designation_list.php'){ echo 'active';}?>"><a href="designation_list.php"><i class="fa fa-hand-o-right"></i> Designation List</a></li>
	       </ul>
	    </li>

	    <li class="treeview <?php if($url=='table_list.php'||$url=='area_list.php'||$url=='kitchen_list.php'){ echo 'active';}?>">
	      <a href="#">
	        <i class="fa fa-cubes"></i> <span>Restaurant Settings</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-left pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">
	        <li class="<?php if($url=='table_list.php'){ echo 'active';}?>"><a href="table_list.php"><i class="fa fa-hand-o-right"></i> Manage Table</a></li>
	        <li class="<?php if($url=='area_list.php'){ echo 'active';}?>"><a href="area_list.php"><i class="fa fa-hand-o-right"></i> Manage Area</a></li>
	       <!-- <li class="<?php if($url=='kitchen_list.php'){ echo 'active';}?>"><a href="kitchen_list.php"><i class="fa fa-hand-o-right"></i> Manage Kitchen </a></li> -->
	      </ul>
	    </li>

	    <li class="treeview <?php if($url=='supplier_list.php'||$url=='new_supplier.php'||$url=='supplies_list.php'||$url=='new_supplies.php'){ echo 'active';}?>">
	      <a href="#">
	        <i class="fa fa-briefcase"></i> <span>Inventory Management</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-left pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">
	        <li class="<?php if($url=='supplies_list.php'){ echo 'active';}?>"><a href="supplies_list.php"><i class="fa fa-hand-o-right"></i> Supplies List</a></li>
	        <li class="<?php if($url=='new_supplies.php'){ echo 'active';}?>"><a href="new_supplies.php"><i class="fa fa-hand-o-right"></i> New Stock Supply</a></li>
	      	<li class="<?php if($url=='supplier_list.php'){ echo 'active';}?>"><a href="supplier_list.php"><i class="fa fa-hand-o-right"></i> Supplier List</a></li>
	      </ul>
	    </li>

	    <li class="treeview <?php if($url=='expense_list.php'||$url=='add_new_expense.php'||$url=='expense_categories.php'){ echo 'active';}?>">
	      <a href="#">
	        <i class="fa fa-money"></i> <span>Expenses</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-left pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">
	        <li class="<?php if($url=='expense_list.php'){ echo 'active';}?>"><a href="expense_list.php"><i class="fa fa-hand-o-right"></i> Expenses List</a></li>
	        <li class="<?php if($url=='add_new_expense.php'){ echo 'active';}?>"><a href="add_new_expense.php"><i class="fa fa-hand-o-right"></i> Add New Expense</a></li>
	        <li class="<?php if($url=='expense_categories.php'){ echo 'active';}?>"><a href="expense_categories.php"><i class="fa fa-hand-o-right"></i> Expense categories  </a></li>
	      </ul>
	    </li>

	    <li class="treeview <?php if($url=='customer_list.php'||$url=='add_new_customer.php'){ echo 'active';}?>">
	      <a href="#">
	        <i class="fa fa-money"></i> <span>Customers</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-left pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">
	        <li class="<?php if($url=='customer_list.php'){ echo 'active';}?>"><a href="customer_list.php"><i class="fa fa-hand-o-right"></i> Customer List</a></li>
	        <li class="<?php if($url=='add_new_customer.php'){ echo 'active';}?>"><a href="add_new_customer.php"><i class="fa fa-hand-o-right"></i> Add New Customer</a></li>
	      </ul>
	    </li>

	    <li class="treeview <?php if($url=='todays_sale.php'||$url=='date_wise_sales_report.php'||$url=='todays_expense_list.php'||$url=='date_wise_expense_report.php'){ echo 'active';}?>">
	      <a href="#">
	        <i class="fa fa-money"></i> <span>Reports</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-left pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">
	        <li class="<?php if($url=='todays_sale.php'){ echo 'active';}?>"><a href="todays_sale.php"><i class="fa fa-hand-o-right"></i> Today's Sales</a></li>
	        <li class="<?php if($url=='date_wise_sales_report.php'){ echo 'active';}?>"><a href="date_wise_sales_report.php"><i class="fa fa-hand-o-right"></i> Date Wise Sales Report</a></li>
	        <li class="<?php if($url=='todays_expense_list.php'){ echo 'active';}?>"><a href="todays_expense_list.php"><i class="fa fa-hand-o-right"></i> Today's Expense List  </a></li>
	        <li class="<?php if($url=='date_wise_expense_report.php'){ echo 'active';}?>"><a href="date_wise_expense_report.php"><i class="fa fa-hand-o-right"></i> Date Wise Expense Report</a></li>
	      </ul>
	    </li>

	<?php 
	}

	function executiveMenu(){
	$url=basename($_SERVER['PHP_SELF']);
	?>
		<li class="<?php if($url=='dashboard.php'){ echo 'active';}?>"><a href="dashboard.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        <li><a href="pos.php"><i class="fa fa-shopping-cart"></i> <span> POS </span></a></li>
        <li class="<?php if($url=='sales.php'){ echo 'active';}?>"><a href="sales.php"><i class="fa fa-industry"></i> <span> Sales </span></a></li>
        
	    <li class="treeview <?php if($url=='item_list.php'||$url=='new_item.php'||$url=='item_category_list.php'||$url=='item_department_list.php'){ echo 'active';}?>">
	      <a href="#">
	        <i class="fa fa-cutlery"></i> <span>Menu Item Settings</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-left pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">
	        <li class="<?php if($url=='item_list.php'){ echo 'active';}?>"><a href="item_list.php"><i class="fa fa-hand-o-right"></i> Item List</a></li>
	        <li class="<?php if($url=='item_category_list.php'){ echo 'active';}?>"><a href="item_category_list.php"><i class="fa fa-hand-o-right"></i> Item Category List</a></li>
	        <li class="<?php if($url=='item_department_list.php'){ echo 'active';}?>"><a href="item_department_list.php"><i class="fa fa-hand-o-right"></i> Item Department List</a></li>
	      </ul>
	    </li>

	    <li class="treeview <?php if($url=='customer_list.php'||$url=='add_new_customer.php'){ echo 'active';}?>">
	      <a href="#">
	        <i class="fa fa-money"></i> <span>Customers</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-left pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">
	        <li class="<?php if($url=='customer_list.php'){ echo 'active';}?>"><a href="customer_list.php"><i class="fa fa-hand-o-right"></i> Customer List</a></li>
	        <li class="<?php if($url=='add_new_customer.php'){ echo 'active';}?>"><a href="add_new_customer.php"><i class="fa fa-hand-o-right"></i> Add New Customer</a></li>
	      </ul>
	    </li>

	    <li class="treeview <?php if($url=='todays_sale.php'||$url=='date_wise_sales_report.php'||$url=='todays_expense_list.php'||$url=='date_wise_expense_report.php'){ echo 'active';}?>">
	      <a href="#">
	        <i class="fa fa-money"></i> <span>Reports</span>
	        <span class="pull-right-container">
	          <i class="fa fa-angle-left pull-right"></i>
	        </span>
	      </a>
	      <ul class="treeview-menu">
	        <li class="<?php if($url=='todays_sale.php'){ echo 'active';}?>"><a href="todays_sale.php"><i class="fa fa-hand-o-right"></i> Today's Sales</a></li>
	        <li class="<?php if($url=='date_wise_sales_report.php'){ echo 'active';}?>"><a href="date_wise_sales_report.php"><i class="fa fa-hand-o-right"></i> Date Wise Sales Report</a></li>
	      </ul>
	    </li>
	<?php 
	}
	

	function logoArea(){
	?>
	<!-- Logo -->
    <a href="dashboard.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>RMS</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>FEITS - RMS</b></span>
    </a>

	<?php 
	}

	function control_sidebar(){
	?>
	<!-- 
		<li>
	    	<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
	  	</li> 
  	-->
	<?php 
	}


	function developer(){

		echo "<p align='center'> Developed by <b>Far-East IT Solutions Limited</b></p>";
		
	}
	 
	function title(){
		echo "Restaurant Management System";	
	}

	function adminFullname(){
		echo $_SESSION['adminRMSfname'];
	}


	function exFullname(){
		echo $_SESSION['exRMSfname'];	
	}

	function profileLink(){ ?>
              <!-- Menu Body -->
              <!-- <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
              </li> -->
              <!-- Menu Footer-->
              <li class="user-footer">
               <!--  <div class="pull-left">
                  <a href="#" class="btn btn-success">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="signout.php" class="btn btn-danger">Sign out</a>
                </div> -->
                <center>
                  <a href="signout.php" class="btn btn-danger">Sign out</a>
                </center>
              </li>
	<?php 
	 }
	 
	 function adminImage(){
	 	echo "../dist/user_img/".$_SESSION['adminRMSImage'];
	 }
	 
	 function executiveImage(){
	 	echo "../dist/user_img/".$_SESSION['exRMSImage'];
	 }

	function myFooter(){ ?>
      <footer class="main-footer">
	    <div class="pull-right hidden-xs">
	      <b>Version</b> 1.0.0
	    </div>
	    <strong>Copyright &copy; <a href="https://feits.co">FEITS </a>.</strong> All rights
	    reserved.
	  </footer>
	<?php
	}
} 
?>