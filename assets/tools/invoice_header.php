<?php
	function invoiceCompanyTitle(){
		return "Far East Restaurant & Cafe";
	}

	function invoiceCompanyaddress(){
		return "House #51, Road #18, Uttara - 11, Dhaka-1230";
	}

	function invoiceCompanyPhone(){
		return "Phone: +8801763346334 ";
	}

	function invoiceCompanyEmail(){
		return "Email: info@feits.co";
	}

	function footerText1(){
		return "To enjoy the glow of good health, you must exercise.";
	}

	function footerText2(){
		return "Thank you very much.";
	}

?>