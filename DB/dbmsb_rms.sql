-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 05, 2018 at 07:24 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbmsb_rms`
--
CREATE DATABASE IF NOT EXISTS `dbmsb_rms` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `dbmsb_rms`;

-- --------------------------------------------------------

--
-- Table structure for table `tbactivity_logs`
--

DROP TABLE IF EXISTS `tbactivity_logs`;
CREATE TABLE `tbactivity_logs` (
  `acl_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `activity` varchar(20) NOT NULL,
  `adate` date NOT NULL,
  `time` varchar(20) NOT NULL,
  `loginIp` varchar(60) NOT NULL,
  `createdDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbactivity_logs`
--

INSERT INTO `tbactivity_logs` (`acl_id`, `user_id`, `activity`, `adate`, `time`, `loginIp`, `createdDateTime`) VALUES
(44, 1, 'Log out', '2018-04-17', '04:44:21pm', '::1', '2018-04-17 16:44:21'),
(45, 1, 'Login', '2018-04-17', '04:44:24pm', '::1', '2018-04-17 16:44:24'),
(46, 1, 'Log out', '2018-04-17', '04:51:33pm', '::1', '2018-04-17 16:51:34'),
(47, 1, 'Login', '2018-04-17', '04:51:48pm', '::1', '2018-04-17 16:51:48'),
(48, 1, 'Log out', '2018-04-17', '05:03:22pm', '::1', '2018-04-17 17:03:22'),
(49, 1, 'Login', '2018-04-17', '05:03:25pm', '::1', '2018-04-17 17:03:25'),
(50, 1, 'Login', '2018-04-17', '05:03:40pm', '::1', '2018-04-17 17:03:40'),
(51, 1, 'Log out', '2018-04-17', '05:45:50pm', '::1', '2018-04-17 17:45:50'),
(52, 1, 'Login', '2018-04-17', '05:45:52pm', '::1', '2018-04-17 17:45:52'),
(53, 1, 'Log out', '2018-04-17', '05:46:18pm', '::1', '2018-04-17 17:46:18'),
(54, 1, 'Login', '2018-04-17', '05:46:20pm', '::1', '2018-04-17 17:46:20'),
(55, 1, 'Login', '2018-04-18', '09:18:13am', '::1', '2018-04-18 09:18:13'),
(56, 1, 'Log out', '2018-04-18', '09:27:14am', '::1', '2018-04-18 09:27:14'),
(57, 1, 'Login', '2018-04-18', '09:27:16am', '::1', '2018-04-18 09:27:16'),
(58, 1, 'Log out', '2018-04-18', '09:27:54am', '::1', '2018-04-18 09:27:54'),
(59, 1, 'Login', '2018-04-18', '09:27:56am', '::1', '2018-04-18 09:27:57'),
(60, 1, 'Log out', '2018-04-18', '09:28:13am', '::1', '2018-04-18 09:28:13'),
(61, 1, 'Login', '2018-04-18', '09:28:15am', '::1', '2018-04-18 09:28:15'),
(62, 1, 'Log out', '2018-04-18', '09:29:36am', '::1', '2018-04-18 09:29:36'),
(63, 1, 'Login', '2018-04-18', '09:30:27am', '::1', '2018-04-18 09:30:27'),
(64, 1, 'Login', '2018-04-18', '09:30:35am', '::1', '2018-04-18 09:30:35'),
(65, 1, 'Log out', '2018-04-18', '09:32:05am', '::1', '2018-04-18 09:32:05'),
(66, 1, 'Login', '2018-04-18', '09:32:08am', '::1', '2018-04-18 09:32:08'),
(67, 1, 'Log out', '2018-04-18', '09:32:13am', '::1', '2018-04-18 09:32:13'),
(68, 1, 'Login', '2018-04-18', '09:32:23am', '::1', '2018-04-18 09:32:23'),
(69, 1, 'Login', '2018-04-18', '09:32:32am', '::1', '2018-04-18 09:32:32'),
(70, 1, 'Log out', '2018-04-18', '09:33:29am', '::1', '2018-04-18 09:33:29'),
(71, 1, 'Login', '2018-04-18', '09:33:34am', '::1', '2018-04-18 09:33:34'),
(72, 1, 'Log out', '2018-04-18', '09:34:21am', '::1', '2018-04-18 09:34:21'),
(73, 1, 'Login', '2018-04-18', '09:35:46am', '::1', '2018-04-18 09:35:46'),
(74, 1, 'Log out', '2018-04-18', '09:41:57am', '::1', '2018-04-18 09:41:57'),
(75, 1, 'Login', '2018-04-18', '09:41:59am', '::1', '2018-04-18 09:41:59'),
(76, 1, 'Log out', '2018-04-18', '09:42:16am', '::1', '2018-04-18 09:42:16'),
(77, 1, 'Login', '2018-04-18', '09:42:18am', '::1', '2018-04-18 09:42:18'),
(78, 1, 'Log out', '2018-04-18', '09:42:29am', '::1', '2018-04-18 09:42:29'),
(79, 1, 'Login', '2018-04-18', '09:42:31am', '::1', '2018-04-18 09:42:31'),
(80, 1, 'Log out', '2018-04-18', '09:42:46am', '::1', '2018-04-18 09:42:46'),
(81, 1, 'Login', '2018-04-18', '09:42:48am', '::1', '2018-04-18 09:42:48'),
(82, 1, 'Log out', '2018-04-18', '09:43:09am', '::1', '2018-04-18 09:43:10'),
(83, 1, 'Login', '2018-04-18', '09:43:12am', '::1', '2018-04-18 09:43:12'),
(84, 1, 'Login', '2018-04-18', '09:44:05am', '192.168.0.248', '2018-04-18 09:44:05'),
(85, 1, 'Login', '2018-04-18', '09:44:13am', '192.168.0.249', '2018-04-18 09:44:13'),
(86, 1, 'Log out', '2018-04-18', '09:44:26am', '192.168.0.248', '2018-04-18 09:44:26'),
(87, 1, 'Log out', '2018-04-18', '09:44:36am', '192.168.0.249', '2018-04-18 09:44:36'),
(88, 1, 'Login', '2018-04-18', '09:44:45am', '192.168.0.248', '2018-04-18 09:44:45'),
(89, 1, 'Login', '2018-04-18', '09:44:49am', '192.168.0.249', '2018-04-18 09:44:49'),
(90, 1, 'Log out', '2018-04-18', '09:44:53am', '192.168.0.248', '2018-04-18 09:44:53'),
(91, 1, 'Login', '2018-04-18', '09:45:10am', '192.168.0.250', '2018-04-18 09:45:10'),
(92, 1, 'Login', '2018-04-18', '09:45:28am', '192.168.0.248', '2018-04-18 09:45:28'),
(93, 1, 'Log out', '2018-04-18', '09:45:29am', '192.168.0.250', '2018-04-18 09:45:29'),
(94, 1, 'Log out', '2018-04-18', '09:48:18am', '::1', '2018-04-18 09:48:18'),
(95, 1, 'Login', '2018-04-18', '09:48:21am', '::1', '2018-04-18 09:48:21'),
(96, 1, 'Log out', '2018-04-18', '09:49:09am', '::1', '2018-04-18 09:49:09'),
(97, 1, 'Login', '2018-04-18', '09:49:11am', '::1', '2018-04-18 09:49:11'),
(98, 1, 'Log out', '2018-04-18', '09:49:57am', '::1', '2018-04-18 09:49:57'),
(99, 1, 'Login', '2018-04-18', '09:49:59am', '::1', '2018-04-18 09:49:59'),
(100, 1, 'Login', '2018-04-18', '09:50:01am', '192.168.0.248', '2018-04-18 09:50:01'),
(101, 1, 'Log out', '2018-04-18', '09:50:08am', '192.168.0.248', '2018-04-18 09:50:08'),
(102, 1, 'Login', '2018-04-18', '09:50:15am', '192.168.0.248', '2018-04-18 09:50:15'),
(103, 1, 'Log out', '2018-04-18', '09:50:25am', '192.168.0.248', '2018-04-18 09:50:25'),
(104, 1, 'Login', '2018-04-18', '09:50:37am', '192.168.0.250', '2018-04-18 09:50:37'),
(105, 1, 'Login', '2018-04-18', '09:50:51am', '192.168.0.248', '2018-04-18 09:50:51'),
(106, 1, 'Log out', '2018-04-18', '09:50:57am', '192.168.0.248', '2018-04-18 09:50:58'),
(107, 1, 'Log out', '2018-04-18', '09:51:16am', '::1', '2018-04-18 09:51:16'),
(108, 1, 'Login', '2018-04-18', '09:51:35am', '::1', '2018-04-18 09:51:35'),
(109, 1, 'Log out', '2018-04-18', '09:51:43am', '::1', '2018-04-18 09:51:43'),
(110, 1, 'Login', '2018-04-18', '09:52:19am', '::1', '2018-04-18 09:52:19'),
(111, 1, 'Login', '2018-04-18', '10:15:27am', '::1', '2018-04-18 10:15:27'),
(112, 1, 'Login', '2018-04-18', '10:43:37am', '192.168.0.4', '2018-04-18 10:43:37'),
(113, 1, 'Sign out', '2018-04-18', '12:42:12pm', '::1', '2018-04-18 12:42:12'),
(114, 1, 'Sign in', '2018-04-18', '12:42:14pm', '::1', '2018-04-18 12:42:14'),
(115, 1, 'Sign out', '2018-04-18', '12:49:22pm', '::1', '2018-04-18 12:49:22'),
(116, 1, 'Sign in', '2018-04-18', '12:49:34pm', '::1', '2018-04-18 12:49:34'),
(117, 1, 'Sign in', '2018-04-18', '04:18:10pm', '192.168.0.107', '2018-04-18 16:18:10'),
(118, 1, 'Sign out', '2018-04-18', '05:27:52pm', '::1', '2018-04-18 17:27:52'),
(119, 1, 'Sign in', '2018-04-18', '05:28:01pm', '::1', '2018-04-18 17:28:01'),
(120, 1, 'Sign out', '2018-04-18', '05:28:22pm', '::1', '2018-04-18 17:28:22'),
(121, 1, 'Sign in', '2018-04-18', '05:28:24pm', '::1', '2018-04-18 17:28:24'),
(122, 1, 'Sign out', '2018-04-18', '05:28:37pm', '::1', '2018-04-18 17:28:37'),
(123, 1, 'Sign in', '2018-04-18', '05:28:40pm', '::1', '2018-04-18 17:28:40'),
(124, 1, 'Sign in', '2018-04-18', '06:01:23pm', '192.168.0.11', '2018-04-18 18:01:23'),
(125, 1, 'Sign in', '2018-04-19', '09:41:50am', '::1', '2018-04-19 09:41:50'),
(126, 1, 'Sign in', '2018-04-19', '02:46:56pm', '::1', '2018-04-19 14:46:56'),
(127, 1, 'Sign out', '2018-04-19', '05:36:42pm', '::1', '2018-04-19 17:36:42'),
(128, 1, 'Sign in', '2018-04-19', '05:36:44pm', '::1', '2018-04-19 17:36:44'),
(129, 1, 'Sign in', '2018-04-22', '09:45:43am', '::1', '2018-04-22 09:45:43'),
(130, 1, 'Sign in', '2018-04-22', '11:34:50am', '::1', '2018-04-22 11:34:50'),
(131, 1, 'Sign in', '2018-04-22', '01:26:15pm', '::1', '2018-04-22 13:26:15'),
(132, 1, 'Sign out', '2018-04-22', '03:46:16pm', '::1', '2018-04-22 15:46:16'),
(133, 5, 'Sign in', '2018-04-22', '03:48:09pm', '::1', '2018-04-22 15:48:10'),
(134, 5, 'Sign in', '2018-04-22', '03:48:17pm', '::1', '2018-04-22 15:48:17'),
(135, 5, 'Sign in', '2018-04-22', '03:48:21pm', '::1', '2018-04-22 15:48:21'),
(136, 5, 'Sign in', '2018-04-22', '03:50:08pm', '::1', '2018-04-22 15:50:08'),
(137, 1, 'Sign in', '2018-04-22', '03:51:11pm', '::1', '2018-04-22 15:51:11'),
(138, 1, 'Sign out', '2018-04-22', '03:53:06pm', '::1', '2018-04-22 15:53:06'),
(139, 5, 'Sign in', '2018-04-22', '03:53:13pm', '::1', '2018-04-22 15:53:13'),
(140, 5, 'Sign in', '2018-04-22', '03:53:52pm', '::1', '2018-04-22 15:53:52'),
(141, 5, 'Sign in', '2018-04-22', '03:54:14pm', '::1', '2018-04-22 15:54:14'),
(142, 5, 'Sign in', '2018-04-23', '09:21:30am', '::1', '2018-04-23 09:21:30'),
(143, 0, 'Sign out', '2018-04-23', '09:21:47am', '::1', '2018-04-23 09:21:47'),
(144, 5, 'Sign in', '2018-04-23', '09:22:09am', '::1', '2018-04-23 09:22:09'),
(145, 0, 'Sign out', '2018-04-23', '09:37:39am', '::1', '2018-04-23 09:37:39'),
(146, 1, 'Sign in', '2018-04-23', '09:37:46am', '::1', '2018-04-23 09:37:46'),
(147, 1, 'Sign out', '2018-04-23', '10:22:00am', '::1', '2018-04-23 10:22:00'),
(148, 5, 'Sign in', '2018-04-23', '10:22:12am', '::1', '2018-04-23 10:22:12'),
(149, 1, 'Sign in', '2018-04-23', '10:35:59am', '::1', '2018-04-23 10:35:59'),
(150, 5, 'Sign in', '2018-04-23', '01:11:24pm', '192.168.0.4', '2018-04-23 13:11:24'),
(151, 0, 'Sign out', '2018-04-23', '01:29:58pm', '192.168.0.4', '2018-04-23 13:29:58'),
(152, 5, 'Sign in', '2018-04-23', '01:31:02pm', '192.168.0.4', '2018-04-23 13:31:02'),
(153, 1, 'Sign out', '2018-04-23', '02:02:48pm', '::1', '2018-04-23 14:02:48'),
(154, 1, 'Sign in', '2018-04-23', '02:02:50pm', '::1', '2018-04-23 14:02:50'),
(155, 1, 'Sign in', '2018-04-23', '03:39:28pm', '192.168.0.107', '2018-04-23 15:39:28'),
(156, 0, 'Sign out', '2018-04-23', '04:47:49pm', '192.168.0.4', '2018-04-23 16:47:49'),
(157, 1, 'Sign in', '2018-04-23', '04:48:04pm', '192.168.0.4', '2018-04-23 16:48:04'),
(158, 1, 'Sign in', '2018-04-24', '09:44:47am', '::1', '2018-04-24 09:44:47'),
(159, 1, 'Sign in', '2018-04-24', '10:52:45am', '192.168.0.4', '2018-04-24 10:52:45'),
(160, 5, 'Sign in', '2018-04-24', '11:01:50am', '192.168.0.222', '2018-04-24 11:01:50'),
(161, 1, 'Sign in', '2018-04-24', '11:05:26am', '::1', '2018-04-24 11:05:26'),
(162, 1, 'Sign out', '2018-04-24', '11:06:20am', '::1', '2018-04-24 11:06:20'),
(163, 5, 'Sign in', '2018-04-24', '11:07:41am', '::1', '2018-04-24 11:07:41'),
(164, 0, 'Sign out', '2018-04-24', '11:12:04am', '::1', '2018-04-24 11:12:04'),
(165, 1, 'Sign in', '2018-04-24', '11:12:11am', '::1', '2018-04-24 11:12:11'),
(166, 1, 'Sign in', '2018-04-24', '02:54:41pm', '192.168.0.107', '2018-04-24 14:54:41'),
(167, 1, 'Sign out', '2018-04-24', '03:09:19pm', '::1', '2018-04-24 15:09:19'),
(168, 1, 'Sign in', '2018-04-24', '03:09:47pm', '::1', '2018-04-24 15:09:47'),
(169, 1, 'Sign in', '2018-04-24', '03:12:25pm', '::1', '2018-04-24 15:12:25'),
(170, 1, 'Sign out', '2018-04-24', '03:12:30pm', '::1', '2018-04-24 15:12:30'),
(171, 1, 'Sign in', '2018-04-24', '03:12:52pm', '::1', '2018-04-24 15:12:52'),
(172, 1, 'Sign out', '2018-04-24', '05:49:00pm', '::1', '2018-04-24 17:49:00'),
(173, 1, 'Sign in', '2018-04-25', '09:12:04am', '::1', '2018-04-25 09:12:04'),
(174, 1, 'Sign in', '2018-04-25', '09:34:40am', '192.168.0.107', '2018-04-25 09:34:40'),
(175, 1, 'Sign in', '2018-04-25', '09:40:08am', '192.168.0.250', '2018-04-25 09:40:09'),
(176, 1, 'Sign in', '2018-04-25', '09:40:10am', '192.168.0.248', '2018-04-25 09:40:10'),
(177, 1, 'Sign out', '2018-04-25', '09:40:42am', '192.168.0.250', '2018-04-25 09:40:42'),
(178, 1, 'Sign in', '2018-04-25', '09:41:16am', '192.168.0.250', '2018-04-25 09:41:16'),
(179, 1, 'Sign out', '2018-04-25', '09:41:27am', '192.168.0.250', '2018-04-25 09:41:27'),
(180, 1, 'Sign in', '2018-04-25', '09:41:35am', '192.168.0.250', '2018-04-25 09:41:35'),
(181, 1, 'Sign out', '2018-04-25', '09:44:44am', '192.168.0.248', '2018-04-25 09:44:44'),
(182, 1, 'Sign in', '2018-04-25', '09:45:06am', '192.168.0.248', '2018-04-25 09:45:06'),
(183, 1, 'Sign out', '2018-04-25', '09:45:55am', '192.168.0.250', '2018-04-25 09:45:55'),
(184, 1, 'Sign in', '2018-04-25', '09:46:11am', '192.168.0.250', '2018-04-25 09:46:11'),
(185, 1, 'Sign out', '2018-04-25', '09:46:23am', '192.168.0.250', '2018-04-25 09:46:23'),
(186, 1, 'Sign in', '2018-04-25', '09:46:40am', '192.168.0.250', '2018-04-25 09:46:40'),
(187, 1, 'Sign in', '2018-04-25', '09:57:57am', '192.168.0.249', '2018-04-25 09:57:57'),
(188, 1, 'Sign out', '2018-04-25', '10:12:19am', '192.168.0.248', '2018-04-25 10:12:19'),
(189, 5, 'Sign in', '2018-04-25', '10:12:39am', '192.168.0.248', '2018-04-25 10:12:39'),
(190, 1, 'Sign in', '2018-04-25', '10:12:47am', '192.168.0.5', '2018-04-25 10:12:47'),
(191, 1, 'Sign out', '2018-04-25', '10:25:44am', '192.168.0.250', '2018-04-25 10:25:44'),
(192, 5, 'Sign in', '2018-04-25', '10:26:02am', '192.168.0.250', '2018-04-25 10:26:02'),
(193, 0, 'Sign out', '2018-04-25', '10:26:57am', '192.168.0.250', '2018-04-25 10:26:57'),
(194, 1, 'Sign in', '2018-04-25', '10:27:09am', '192.168.0.250', '2018-04-25 10:27:09'),
(195, 1, 'Sign in', '2018-04-25', '11:06:59am', '192.168.0.107', '2018-04-25 11:06:59'),
(196, 1, 'Sign in', '2018-04-30', '12:00:48pm', '::1', '2018-04-30 12:00:48');

-- --------------------------------------------------------

--
-- Table structure for table `tbarealist`
--

DROP TABLE IF EXISTS `tbarealist`;
CREATE TABLE `tbarealist` (
  `id` int(11) NOT NULL,
  `areaName` varchar(255) NOT NULL,
  `areaDescription` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbarealist`
--

INSERT INTO `tbarealist` (`id`, `areaName`, `areaDescription`) VALUES
(1, 'Area1', 'None'),
(2, 'Area2', 'None');

-- --------------------------------------------------------

--
-- Table structure for table `tbcustomerinfo`
--

DROP TABLE IF EXISTS `tbcustomerinfo`;
CREATE TABLE `tbcustomerinfo` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbcustomerinfo`
--

INSERT INTO `tbcustomerinfo` (`id`, `fullname`, `phone`, `email`, `address`) VALUES
(1, 'Samsujjaman Bappy', '01824168996', 'abc@gmail.com', 'uttara11'),
(2, 'Far East IT Solutions Ltd.', '01824168966', 'samsujjamanbappy@gmail.com', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230');

-- --------------------------------------------------------

--
-- Table structure for table `tbdesignationlist`
--

DROP TABLE IF EXISTS `tbdesignationlist`;
CREATE TABLE `tbdesignationlist` (
  `id` int(11) NOT NULL,
  `designationName` varchar(255) NOT NULL,
  `designationDescription` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbdesignationlist`
--

INSERT INTO `tbdesignationlist` (`id`, `designationName`, `designationDescription`) VALUES
(2, 'Waiter', 'None'),
(3, 'Manager', 'None');

-- --------------------------------------------------------

--
-- Table structure for table `tbemployeelist`
--

DROP TABLE IF EXISTS `tbemployeelist`;
CREATE TABLE `tbemployeelist` (
  `id` int(11) NOT NULL,
  `empName` varchar(255) NOT NULL,
  `empPhone` varchar(30) NOT NULL,
  `empEmail` varchar(50) NOT NULL,
  `empDesignationId` varchar(3) NOT NULL,
  `empAddress` text NOT NULL,
  `empDescription` text NOT NULL,
  `empJoiningDate` date NOT NULL,
  `empSalaryType` varchar(1) NOT NULL,
  `empAccountStatus` int(1) NOT NULL DEFAULT '1',
  `salaryAmount` varchar(25) NOT NULL DEFAULT '0',
  `empGender` varchar(1) NOT NULL,
  `empImage` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbemployeelist`
--

INSERT INTO `tbemployeelist` (`id`, `empName`, `empPhone`, `empEmail`, `empDesignationId`, `empAddress`, `empDescription`, `empJoiningDate`, `empSalaryType`, `empAccountStatus`, `salaryAmount`, `empGender`, `empImage`) VALUES
(1, 'Samsujjaman Bappy', '01824168996', 'samsujjamanbappy@gmail.com', '2', 'Uttara 11', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', '2018-04-01', '0', 1, '2500', '1', 'employee201804250650_4.'),
(2, 'Samsujjaman', '01824168996', 'samsujjaman@gmail.com', '2', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 'Uttara', '2018-04-10', '1', 1, '270', '1', 'employee201804050518_2.jpg'),
(4, 'Tamim', '01671937065', 'tamim@feits.co', '3', 'Mogbazar', 'Gorib Manush', '2017-09-13', '0', 1, '100000', '1', 'employee201804250617_3.');

-- --------------------------------------------------------

--
-- Table structure for table `tbemployeesalary`
--

DROP TABLE IF EXISTS `tbemployeesalary`;
CREATE TABLE `tbemployeesalary` (
  `id` int(11) NOT NULL,
  `empId` varchar(10) NOT NULL,
  `sdate` date NOT NULL,
  `amount` varchar(5) NOT NULL,
  `createdDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbemployeesalary`
--

INSERT INTO `tbemployeesalary` (`id`, `empId`, `sdate`, `amount`, `createdDateTime`) VALUES
(5, '2', '2018-04-18', '270', '2018-04-19 13:15:41'),
(7, '1', '2018-04-11', '2500', '2018-04-19 14:50:53'),
(9, '3', '2018-04-19', '250', '2018-04-19 14:58:49'),
(10, '', '0000-00-00', '', '2018-04-22 15:25:54'),
(11, '2', '2018-04-25', '270', '2018-04-25 10:11:41');

-- --------------------------------------------------------

--
-- Table structure for table `tbexpensecategory`
--

DROP TABLE IF EXISTS `tbexpensecategory`;
CREATE TABLE `tbexpensecategory` (
  `id` int(11) NOT NULL,
  `categoryName` varchar(255) NOT NULL,
  `categoryDescription` text NOT NULL,
  `createdDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbexpensecategory`
--

INSERT INTO `tbexpensecategory` (`id`, `categoryName`, `categoryDescription`, `createdDateTime`) VALUES
(1, 'ABCreter', 'Naiertertre', '2018-04-02 17:21:53'),
(3, 'Bills', 'Electricity, Gas, Water, Internet', '2018-04-03 14:11:02'),
(4, 'Employee Salary', '', '2018-04-19 10:22:10');

-- --------------------------------------------------------

--
-- Table structure for table `tbexpenselist`
--

DROP TABLE IF EXISTS `tbexpenselist`;
CREATE TABLE `tbexpenselist` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `categoryId` varchar(3) NOT NULL,
  `amount` varchar(25) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `expenseDate` date NOT NULL,
  `attachment` text NOT NULL,
  `createdDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbexpenselist`
--

INSERT INTO `tbexpenselist` (`id`, `name`, `categoryId`, `amount`, `reference`, `description`, `expenseDate`, `attachment`, `createdDateTime`) VALUES
(1, 'Rice', '1', '250', '1', 'Nai', '2018-04-03', '', '2018-04-03 13:06:53'),
(2, 'gas bill', '3', '800', '', '', '0000-00-00', 'expense201804031024_1.png', '2018-04-03 14:55:24'),
(3, 'Electricity Bills', '3', '1526', '', '', '2018-04-16', 'expense201804031012_2.jpg', '2018-04-03 14:57:12'),
(4, 'Internet Bill', '3', '780', '', '', '2018-04-03', 'expense201804031013_3.png', '2018-04-03 14:58:13'),
(5, 'Mahabubur Rahman', '1', '250', '', 'Nai', '2018-04-03', 'expense201804031239_5.png', '2018-04-03 15:03:03'),
(6, 'Napa Extra', '3', '20', '', '', '2018-04-03', '', '2018-04-03 15:03:35'),
(7, 'New Computer ', '3', '20000', '', '', '2018-04-03', '', '2018-04-03 15:03:35'),
(8, 'Breakfast', '3', '232', '', '', '2018-04-17', '', '2018-04-17 15:03:35'),
(9, 'Breakfast', '3', '186', '', '', '2018-04-16', '', '2018-04-16 15:03:35'),
(10, '2018-04-19', '4', '750', '', '', '2018-04-19', '', '2018-04-19 10:22:32');

-- --------------------------------------------------------

--
-- Table structure for table `tbholdorder`
--

DROP TABLE IF EXISTS `tbholdorder`;
CREATE TABLE `tbholdorder` (
  `id` int(11) NOT NULL,
  `tmpPOSId` varchar(60) NOT NULL,
  `tableId` varchar(10) NOT NULL,
  `bookedSeats` varchar(5) NOT NULL,
  `waiterId` varchar(10) NOT NULL,
  `holdType` int(1) NOT NULL DEFAULT '0',
  `dateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbholdorder`
--

INSERT INTO `tbholdorder` (`id`, `tmpPOSId`, `tableId`, `bookedSeats`, `waiterId`, `holdType`, `dateTime`) VALUES
(12, '89328003', '3', '', '2', 1, '2018-04-24 09:18:25'),
(13, '24682624', '8', '', '3', 1, '2018-04-25 03:48:19'),
(14, '', '7', '', '3', 1, '2018-04-25 03:58:19');

-- --------------------------------------------------------

--
-- Table structure for table `tbinvoiceinfo`
--

DROP TABLE IF EXISTS `tbinvoiceinfo`;
CREATE TABLE `tbinvoiceinfo` (
  `id` int(11) NOT NULL,
  `invoiceNumber` int(10) NOT NULL,
  `customerId` varchar(10) NOT NULL,
  `discountAmount` varchar(10) NOT NULL,
  `vatAmount` varchar(10) NOT NULL,
  `deliveryCharge` varchar(10) NOT NULL,
  `paidAmount` varchar(10) NOT NULL,
  `invoiceDate` date NOT NULL,
  `invoiceTime` varchar(12) NOT NULL,
  `paymentType` varchar(1) NOT NULL DEFAULT '1',
  `userId` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbinvoiceinfo`
--

INSERT INTO `tbinvoiceinfo` (`id`, `invoiceNumber`, `customerId`, `discountAmount`, `vatAmount`, `deliveryCharge`, `paidAmount`, `invoiceDate`, `invoiceTime`, `paymentType`, `userId`) VALUES
(1, 1, '1', '5', '15', '50', '1000', '2018-04-16', '12:21:32pm', '1', '1'),
(2, 2, '1', '5', '15', '50', '1000', '2018-04-10', '12:21:32pm', '1', '1'),
(3, 3, '1', '5', '15', '50', '1000', '2018-04-09', '12:21:32pm', '1', '1'),
(4, 4, '1', '5', '15', '50', '1000', '2018-04-17', '12:21:32pm', '1', '1'),
(5, 5, '1', '5', '15', '50', '1000', '2018-04-17', '12:21:32pm', '1', '1'),
(6, 6, '1', '5', '15', '50', '1000', '2018-04-17', '12:21:32pm', '1', '1'),
(7, 7, '1', '5', '15', '50', '1000', '2018-04-14', '12:21:32pm', '1', '1'),
(8, 8, '1', '5', '15', '50', '1000', '2018-04-12', '12:21:32pm', '1', '1'),
(9, 13, '1', '100', '0', '100', '8500', '2018-04-17', '03:41:21pm', '1', '1'),
(10, 10, '0', '0', '0', '0', '2500', '2018-04-17', '04:43:01pm', '1', '1'),
(13, 11, '0', '0', '0', '0', '2500', '2018-04-17', '04:43:01pm', '1', '1'),
(14, 14, '1', '0', '15', '0', '1200', '2018-04-18', '10:38:32am', '1', '1'),
(15, 15, '1', '0', '015', '0', '2000', '2018-04-18', '10:45:56am', '1', '1'),
(16, 16, '1', '0', '15', '0', '10', '2018-04-18', '10:46:52am', '1', '1'),
(17, 17, '1', '0', '015', '0', '1500', '2018-04-18', '10:47:59am', '1', '1'),
(18, 18, '0', '0', '0', '0', '1000', '2018-04-18', '10:48:38am', '1', '1'),
(19, 19, '1', '0', '0', '0', '500', '2018-04-22', '11:53:22am', '1', '1'),
(20, 20, '1', '0', '0', '0', '8', '2018-04-23', '09:22:27am', '1', '5'),
(21, 21, '2', '50', '15', '50', '600', '2018-04-23', '12:52:11pm', '1', '5'),
(22, 22, '2', '0', '15', '50', '3500', '2018-04-23', '01:15:23pm', '1', '5'),
(23, 23, '0', '0', '015', '050', '0', '2018-04-23', '01:17:15pm', '1', '5'),
(24, 24, '0', '0', '015', '50', '0', '2018-04-23', '01:36:11pm', '1', '5'),
(25, 25, '2', '0', '15', '50', '3500', '2018-04-23', '05:24:21pm', '1', '1'),
(26, 26, '1', '0', '15', '50', '80', '2018-04-23', '05:25:29pm', '1', '1'),
(27, 27, '2', '0', '0', '0', '520', '2018-04-23', '05:27:20pm', '1', '1'),
(28, 28, '2', '0', '15', '50', '2000', '2018-04-24', '10:51:01am', '1', '1'),
(29, 29, '2', '0', '15', '0', '3500', '2018-04-24', '11:03:20am', '1', '5'),
(30, 30, '1', '0', '0', '0', '2000', '2018-04-24', '11:03:42am', '1', '5'),
(31, 31, '2', '0', '15', '0', '', '2018-04-24', '11:04:34am', '1', '5'),
(32, 32, '2', '0', '15', '0', '', '2018-04-24', '11:04:52am', '1', '5'),
(33, 33, '0', '0', '15', '0', '', '2018-04-24', '11:09:23am', '1', '5'),
(34, 34, '0', '0', '20', '0', '', '2018-04-24', '11:09:36am', '1', '5'),
(35, 35, '0', '0', '0', '50', '', '2018-04-24', '11:11:16am', '1', '5'),
(36, 36, '2', '0', '15', '100', '', '2018-04-24', '11:11:46am', '1', '5'),
(37, 37, '0', '1', '1', '1', '10', '2018-04-24', '02:56:47pm', '2', '1'),
(38, 38, '0', '10', '10', '10', '450', '2018-04-24', '02:57:59pm', '2', '1'),
(39, 39, '0', '10', '2', '10', '450', '2018-04-24', '02:58:52pm', '2', '1'),
(40, 40, '1', '0', '15', '0', '', '2018-04-24', '02:59:10pm', '1', '1'),
(41, 41, '0', '0', '15', '0', '', '2018-04-24', '02:59:43pm', '2', '1'),
(42, 42, '0', '0', '15', '0', '', '2018-04-24', '03:01:16pm', '1', '1'),
(43, 43, '1', '0', '15', '0', '20', '2018-04-24', '03:07:49pm', '1', '1'),
(44, 44, '0', '0', '15', '0', '', '2018-04-24', '03:08:09pm', '1', '1'),
(45, 45, '1', '0', '15', '0', '', '2018-04-24', '03:10:05pm', '1', '1'),
(46, 46, '2', '10', '12', '0', '1000', '2018-04-25', '09:53:05am', '1', '1'),
(47, 47, '0', '0', '0', '0', '1000', '2018-04-25', '09:53:35am', '1', '1'),
(48, 48, '0', '0', '0', '0', '', '2018-04-25', '09:53:44am', '1', '1'),
(49, 49, '1', '0', '0', '0', '288', '2018-04-25', '10:06:14am', '2', '1'),
(50, 50, '0', '0', '0', '0', '', '2018-04-25', '10:07:11am', '1', '1'),
(51, 51, '0', '0', '0', '0', '', '2018-04-25', '10:09:34am', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbitemcategory`
--

DROP TABLE IF EXISTS `tbitemcategory`;
CREATE TABLE `tbitemcategory` (
  `id` int(11) NOT NULL,
  `categoryName` varchar(255) NOT NULL,
  `categoryDescription` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbitemcategory`
--

INSERT INTO `tbitemcategory` (`id`, `categoryName`, `categoryDescription`) VALUES
(2, 'Bread', 'None'),
(3, 'Juice689698698', 'None8768976986986');

-- --------------------------------------------------------

--
-- Table structure for table `tbitemdepartment`
--

DROP TABLE IF EXISTS `tbitemdepartment`;
CREATE TABLE `tbitemdepartment` (
  `id` int(11) NOT NULL,
  `departmentName` varchar(255) NOT NULL,
  `departmentDescription` text NOT NULL,
  `createdDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbitemdepartment`
--

INSERT INTO `tbitemdepartment` (`id`, `departmentName`, `departmentDescription`, `createdDateTime`) VALUES
(2, 'Cold Subs', 'Cold Subs', '2018-03-28 14:18:54'),
(3, 'Appetizers', 'None', '2018-03-28 14:19:19'),
(4, 'Cake', 'None', '2018-03-28 14:19:40');

-- --------------------------------------------------------

--
-- Table structure for table `tbitemlist`
--

DROP TABLE IF EXISTS `tbitemlist`;
CREATE TABLE `tbitemlist` (
  `id` int(11) NOT NULL,
  `itemName` varchar(255) NOT NULL,
  `itemCategoryId` varchar(5) NOT NULL,
  `itemDepartmentId` varchar(5) NOT NULL,
  `itemDescription` text NOT NULL,
  `itemCode` varchar(50) NOT NULL,
  `itemSellPrice` varchar(10) NOT NULL,
  `itemQuantity` varchar(10) NOT NULL,
  `itemExpireDate` date NOT NULL,
  `itemExpireDateAction` int(11) NOT NULL DEFAULT '0',
  `itemType` varchar(1) NOT NULL,
  `itemStockManagement` int(1) NOT NULL DEFAULT '0',
  `itemStockAlert` int(1) NOT NULL DEFAULT '1',
  `itemPhoto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbitemlist`
--

INSERT INTO `tbitemlist` (`id`, `itemName`, `itemCategoryId`, `itemDepartmentId`, `itemDescription`, `itemCode`, `itemSellPrice`, `itemQuantity`, `itemExpireDate`, `itemExpireDateAction`, `itemType`, `itemStockManagement`, `itemStockAlert`, `itemPhoto`) VALUES
(1, 'Spicy Apple Dip', '3', '3', 'None', '1234', '7.32', '12', '2018-03-31', 1, '0', 1, 1, 'item201804010518_1.jpeg'),
(2, 'Organic Wholemeal Spelt Bread', '3', '3', 'None', '45121', '560', '14', '2018-03-31', 1, '1', 1, 0, 'item201804010546_2.'),
(3, 'Chicken Teriyaki', '3', '3', 'None', '21232', '485', '23', '2018-03-21', 1, '0', 1, 1, 'item201804010508_3.jpg'),
(5, 'Burgers', '2', '4', 'None', '123554', '450', '22', '2018-03-27', 0, '1', 1, 1, 'item201803290555_4.jpeg'),
(7, 'CHICKEN POPEYES', '2', '4', 'None', '123555', '560', '22', '2018-03-27', 0, '1', 0, 0, 'item201803290555_4.jpeg'),
(8, 'CHICKEN SANDWICH', '2', '4', 'None', '123556', '320', '22', '2018-03-27', 0, '1', 0, 0, 'item201803290555_4.jpeg'),
(9, 'SPICY CHICKEN SANDWICH', '2', '4', 'None', '123557', '355', '22', '2018-03-27', 0, '1', 0, 0, 'item201803290555_4.jpeg'),
(10, 'CHICKEN TENDERS', '2', '4', 'None', '123558', '263', '22', '2018-03-27', 0, '1', 0, 0, 'item201803290555_4.jpeg'),
(11, 'WHOPPER BURGER KING', '2', '4', 'None', '123559', '300', '22', '2018-03-27', 0, '1', 0, 0, 'item201803290555_4.jpeg'),
(12, 'CHEESE CURDS CULVER\'S', '2', '4', 'None', '123560', '325', '22', '2018-03-27', 0, '1', 0, 0, 'item201803290555_4.jpeg'),
(14, 'ewrwerwer', '2', '2', 'reterterter', '#657567567', '435343543', '43485', '2018-04-25', 1, '0', 1, 1, 'item201804250521_11.png'),
(15, 'Chicken Kabab', '2', '3', 'chicken kabab with garlic topping.', '012', '81', '1', '2018-05-05', 1, '1', 1, 1, 'item201804250659_12.jpg'),
(16, 'hnm', '2', '', '', 'hjngh', '156', '45', '2018-04-30', 0, '0', 1, 1, 'item201804300824_12.');

-- --------------------------------------------------------

--
-- Table structure for table `tbkitchenlist`
--

DROP TABLE IF EXISTS `tbkitchenlist`;
CREATE TABLE `tbkitchenlist` (
  `id` int(11) NOT NULL,
  `kitchenName` varchar(255) NOT NULL,
  `kitchenDescription` text NOT NULL,
  `createdDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbkitchenlist`
--

INSERT INTO `tbkitchenlist` (`id`, `kitchenName`, `kitchenDescription`, `createdDateTime`) VALUES
(1, 'Kitchen1', 'None', '2018-03-28 11:49:07'),
(2, 'Kitchen2', 'None', '2018-03-28 12:08:02');

-- --------------------------------------------------------

--
-- Table structure for table `tbpossalesitems`
--

DROP TABLE IF EXISTS `tbpossalesitems`;
CREATE TABLE `tbpossalesitems` (
  `id` int(11) NOT NULL,
  `invoiceNumber` varchar(10) NOT NULL,
  `itemId` varchar(10) NOT NULL,
  `quantity` varchar(10) NOT NULL,
  `unitPrice` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbpossalesitems`
--

INSERT INTO `tbpossalesitems` (`id`, `invoiceNumber`, `itemId`, `quantity`, `unitPrice`) VALUES
(1, '1', '3', '1', '485'),
(2, '1', '3', '1', '485'),
(3, '2', '2', '5', '485'),
(4, '1', '1', '3', '485'),
(5, '3', '9', '2', '355'),
(6, '3', '1', '3', '7.32'),
(7, '9', '3', '2', '485'),
(8, '9', '2', '2', '560'),
(9, '9', '5', '2', '450'),
(10, '9', '7', '2', '560'),
(11, '9', '8', '2', '320'),
(12, '9', '9', '2', '355'),
(13, '8', '10', '2', '263'),
(14, '7', '11', '2', '300'),
(15, '9', '12', '2', '325'),
(16, '10', '1', '1', '7.32'),
(17, '10', '3', '1', '485'),
(18, '10', '2', '1', '560'),
(19, '10', '5', '1', '450'),
(20, '10', '7', '1', '560'),
(21, '10', '1', '1', '7.32'),
(22, '10', '3', '1', '485'),
(23, '10', '1', '2', '7.32'),
(24, '10', '3', '1', '485'),
(25, '10', '2', '1', '560'),
(26, '10', '5', '2', '450'),
(27, '10', '5', '1', '450'),
(28, '10', '2', '1', '560'),
(29, '14', '5', '1', '450'),
(30, '14', '7', '1', '560'),
(31, '15', '5', '1', '450'),
(32, '15', '2', '2', '560'),
(33, '16', '1', '1', '7.32'),
(34, '17', '1', '1', '7.32'),
(35, '18', '5', '2', '450'),
(36, '19', '5', '1', '450'),
(37, '20', '1', '1', '7.32'),
(38, '21', '1', '1', '7.32'),
(39, '21', '3', '1', '485'),
(40, '22', '1', '3', '7.32'),
(41, '22', '3', '1', '485'),
(42, '22', '5', '5', '450'),
(43, '23', '3', '1', '485'),
(44, '24', '1', '1', '7.32'),
(45, '24', '5', '2', '450'),
(46, '24', '12', '1', '325'),
(47, '25', '1', '2', '7.32'),
(48, '25', '3', '1', '485'),
(49, '25', '2', '1', '560'),
(50, '25', '7', '1', '560'),
(51, '25', '5', '3', '450'),
(52, '26', '1', '3', '7.32'),
(53, '27', '1', '4', '7.32'),
(54, '27', '3', '1', '485'),
(55, '28', '1', '1', '7.32'),
(56, '28', '3', '1', '485'),
(57, '28', '2', '1', '560'),
(58, '28', '5', '1', '450'),
(59, '29', '1', '3', '7.32'),
(60, '29', '3', '2', '485'),
(61, '29', '2', '2', '560'),
(62, '29', '7', '1', '560'),
(63, '29', '12', '1', '325'),
(64, '30', '5', '2', '450'),
(65, '30', '3', '1', '485'),
(66, '30', '2', '1', '560'),
(67, '31', '1', '2', '7.32'),
(68, '31', '3', '2', '485'),
(69, '31', '2', '1', '560'),
(70, '31', '5', '1', '450'),
(71, '31', '7', '1', '560'),
(72, '32', '1', '1', '7.32'),
(73, '32', '3', '1', '485'),
(74, '32', '2', '1', '560'),
(75, '33', '1', '1', '7.32'),
(76, '33', '3', '1', '485'),
(77, '34', '1', '1', '7.32'),
(78, '34', '3', '1', '485'),
(79, '35', '3', '1', '485'),
(80, '35', '2', '1', '560'),
(81, '36', '1', '1', '7.32'),
(82, '36', '3', '1', '485'),
(83, '37', '5', '1', '450'),
(84, '38', '5', '1', '450'),
(85, '39', '7', '1', '560'),
(86, '40', '3', '1', '485'),
(87, '40', '2', '1', '560'),
(88, '41', '2', '1', '560'),
(89, '41', '3', '1', '485'),
(90, '42', '1', '2', '7.32'),
(91, '43', '1', '1', '7.32'),
(92, '44', '7', '1', '560'),
(93, '44', '5', '1', '450'),
(94, '45', '1', '1', '7.32'),
(95, '45', '5', '1', '450'),
(96, '46', '5', '1', '450'),
(97, '46', '7', '1', '560'),
(98, '47', '1', '1', '7.32'),
(99, '47', '5', '1', '450'),
(100, '47', '7', '1', '560'),
(101, '48', '5', '1', '450'),
(102, '48', '8', '1', '320'),
(103, '49', '1', '4', '7.32'),
(104, '49', '8', '1', '320'),
(105, '49', '14', '1', '43534354'),
(106, '50', '1', '1', '7.32'),
(107, '50', '14', '1', '43534354'),
(108, '51', '14', '51', '43534354');

-- --------------------------------------------------------

--
-- Table structure for table `tbsupplier`
--

DROP TABLE IF EXISTS `tbsupplier`;
CREATE TABLE `tbsupplier` (
  `id` int(11) NOT NULL,
  `supplierName` varchar(255) NOT NULL,
  `supplierPhone` varchar(25) NOT NULL,
  `supplierEmail` varchar(50) NOT NULL,
  `supplierAddress` text NOT NULL,
  `supplierDescription` text NOT NULL,
  `createdDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbsupplier`
--

INSERT INTO `tbsupplier` (`id`, `supplierName`, `supplierPhone`, `supplierEmail`, `supplierAddress`, `supplierDescription`, `createdDateTime`) VALUES
(1, 'Samsujjaman Bappy', '01824168996', 'samsujjaman@gmail.com', 'Uttara', 'Nai', '2018-04-01 12:09:36'),
(2, 'Tamim B', '0152444586', 'tamim@feits.co', 'Uttara 11', 'Nai', '2018-04-01 12:29:04'),
(3, 'tyretyrtyry', '3563363556363737347', '', 'ytruyruy', 'uyrtiutiu', '2018-04-25 10:18:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbsuppliesitems`
--

DROP TABLE IF EXISTS `tbsuppliesitems`;
CREATE TABLE `tbsuppliesitems` (
  `id` int(11) NOT NULL,
  `suppliesId` varchar(50) NOT NULL,
  `itemId` varchar(6) NOT NULL,
  `quantity` varchar(5) NOT NULL,
  `purchasePrice` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbsuppliesitems`
--

INSERT INTO `tbsuppliesitems` (`id`, `suppliesId`, `itemId`, `quantity`, `purchasePrice`) VALUES
(1, '1', '2', '50', '45'),
(2, '1', '1', '56', '20'),
(3, '1', '3', '2', '50'),
(4, '1', '2', '454', '45'),
(5, '1', '5', '15', '20'),
(6, '2', '5', '2', '20'),
(7, '3', '5', '20', '1'),
(8, '4', '3', '20', '25'),
(9, '4', '1', '75', '55'),
(10, '4', '2', '32', '12'),
(11, '5', '3', '32', '20');

-- --------------------------------------------------------

--
-- Table structure for table `tbsupplieslist`
--

DROP TABLE IF EXISTS `tbsupplieslist`;
CREATE TABLE `tbsupplieslist` (
  `id` int(11) NOT NULL,
  `suppliesId` varchar(20) NOT NULL,
  `supplierId` varchar(10) NOT NULL,
  `createdBy` varchar(5) NOT NULL,
  `dateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbsupplieslist`
--

INSERT INTO `tbsupplieslist` (`id`, `suppliesId`, `supplierId`, `createdBy`, `dateTime`) VALUES
(1, '1', '1', '1', '2018-04-02 14:38:09'),
(2, '2', '2', '1', '2018-04-02 14:38:54'),
(3, '3', '1', '1', '2018-04-02 14:41:31'),
(4, '4', '1', '1', '2018-04-02 15:24:06'),
(5, '5', '2', '1', '2018-04-02 15:24:49'),
(6, '6', '1', '1', '2018-04-02 17:45:15');

-- --------------------------------------------------------

--
-- Table structure for table `tbtablelist`
--

DROP TABLE IF EXISTS `tbtablelist`;
CREATE TABLE `tbtablelist` (
  `id` int(11) NOT NULL,
  `tableName` varchar(255) NOT NULL,
  `tableMaxSeats` varchar(3) NOT NULL,
  `tableStatus` int(1) NOT NULL DEFAULT '1',
  `tableDescription` text NOT NULL,
  `tableAreaId` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbtablelist`
--

INSERT INTO `tbtablelist` (`id`, `tableName`, `tableMaxSeats`, `tableStatus`, `tableDescription`, `tableAreaId`) VALUES
(1, 'Table1', '5', 1, 'None', '1'),
(2, 'Table2', '4', 4, 'GGSSVV', '1'),
(3, 'Table3', '4', 2, 'GGSSVV', '1'),
(4, 'Table4', '4', 4, 'GGSSVV', '1'),
(5, 'Table1', '5', 1, 'None', '2'),
(6, 'Table2', '4', 4, 'GGSSVV', '2'),
(7, 'Table3', '4', 2, 'GGSSVV', '2'),
(8, 'Table4', '4', 4, 'GGSSVV', '2');

-- --------------------------------------------------------

--
-- Table structure for table `tbtmppos_item`
--

DROP TABLE IF EXISTS `tbtmppos_item`;
CREATE TABLE `tbtmppos_item` (
  `id` int(11) NOT NULL,
  `tmpId` varchar(50) NOT NULL,
  `itemId` varchar(10) NOT NULL,
  `quantity` varchar(8) NOT NULL,
  `unitPrice` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbtmppos_item`
--

INSERT INTO `tbtmppos_item` (`id`, `tmpId`, `itemId`, `quantity`, `unitPrice`) VALUES
(99, '30313117', '1', '1', '7.32'),
(100, '30313117', '5', '4', '450'),
(101, '30313117', '7', '1', '560'),
(108, '67916873', '1', '5', '7.32'),
(109, '67916873', '8', '2', '320'),
(110, '67916873', '9', '1', '355'),
(114, '24682624', '1', '1', '7.32'),
(115, '24682624', '5', '1', '450'),
(117, '', '15', '1', '81');

-- --------------------------------------------------------

--
-- Table structure for table `tbtmpsuppliesitems`
--

DROP TABLE IF EXISTS `tbtmpsuppliesitems`;
CREATE TABLE `tbtmpsuppliesitems` (
  `id` int(11) NOT NULL,
  `tmpId` varchar(50) NOT NULL,
  `itemId` varchar(6) NOT NULL,
  `quantity` varchar(5) NOT NULL,
  `purchasePrice` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbusers`
--

DROP TABLE IF EXISTS `tbusers`;
CREATE TABLE `tbusers` (
  `id` int(11) NOT NULL,
  `empId` varchar(5) NOT NULL,
  `userName` varchar(255) NOT NULL,
  `userPassword` text NOT NULL,
  `userTypeId` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbusers`
--

INSERT INTO `tbusers` (`id`, `empId`, `userName`, `userPassword`, `userTypeId`) VALUES
(1, '1', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1),
(5, '2', 'executive', '3250d1e21c4281d3cd9479f5685770b6', 2),
(7, '4', 'ertertre', '21232f297a57a5a743894a0e4a801fc3', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbactivity_logs`
--
ALTER TABLE `tbactivity_logs`
  ADD PRIMARY KEY (`acl_id`);

--
-- Indexes for table `tbarealist`
--
ALTER TABLE `tbarealist`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `areaName` (`areaName`);

--
-- Indexes for table `tbcustomerinfo`
--
ALTER TABLE `tbcustomerinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbdesignationlist`
--
ALTER TABLE `tbdesignationlist`
  ADD PRIMARY KEY (`id`,`designationName`);

--
-- Indexes for table `tbemployeelist`
--
ALTER TABLE `tbemployeelist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbemployeesalary`
--
ALTER TABLE `tbemployeesalary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbexpensecategory`
--
ALTER TABLE `tbexpensecategory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categoryName` (`categoryName`);

--
-- Indexes for table `tbexpenselist`
--
ALTER TABLE `tbexpenselist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbholdorder`
--
ALTER TABLE `tbholdorder`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbinvoiceinfo`
--
ALTER TABLE `tbinvoiceinfo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoiceNumber` (`invoiceNumber`);

--
-- Indexes for table `tbitemcategory`
--
ALTER TABLE `tbitemcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbitemdepartment`
--
ALTER TABLE `tbitemdepartment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `departmentName` (`departmentName`);

--
-- Indexes for table `tbitemlist`
--
ALTER TABLE `tbitemlist`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `itemCode` (`itemCode`);

--
-- Indexes for table `tbkitchenlist`
--
ALTER TABLE `tbkitchenlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbpossalesitems`
--
ALTER TABLE `tbpossalesitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbsupplier`
--
ALTER TABLE `tbsupplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbsuppliesitems`
--
ALTER TABLE `tbsuppliesitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbsupplieslist`
--
ALTER TABLE `tbsupplieslist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbtablelist`
--
ALTER TABLE `tbtablelist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbtmppos_item`
--
ALTER TABLE `tbtmppos_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbtmpsuppliesitems`
--
ALTER TABLE `tbtmpsuppliesitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbusers`
--
ALTER TABLE `tbusers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userEmail` (`userName`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbactivity_logs`
--
ALTER TABLE `tbactivity_logs`
  MODIFY `acl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=197;

--
-- AUTO_INCREMENT for table `tbarealist`
--
ALTER TABLE `tbarealist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbcustomerinfo`
--
ALTER TABLE `tbcustomerinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbdesignationlist`
--
ALTER TABLE `tbdesignationlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbemployeelist`
--
ALTER TABLE `tbemployeelist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbemployeesalary`
--
ALTER TABLE `tbemployeesalary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbexpensecategory`
--
ALTER TABLE `tbexpensecategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbexpenselist`
--
ALTER TABLE `tbexpenselist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbholdorder`
--
ALTER TABLE `tbholdorder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbinvoiceinfo`
--
ALTER TABLE `tbinvoiceinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `tbitemcategory`
--
ALTER TABLE `tbitemcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbitemdepartment`
--
ALTER TABLE `tbitemdepartment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbitemlist`
--
ALTER TABLE `tbitemlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbkitchenlist`
--
ALTER TABLE `tbkitchenlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbpossalesitems`
--
ALTER TABLE `tbpossalesitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `tbsupplier`
--
ALTER TABLE `tbsupplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbsuppliesitems`
--
ALTER TABLE `tbsuppliesitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbsupplieslist`
--
ALTER TABLE `tbsupplieslist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbtablelist`
--
ALTER TABLE `tbtablelist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbtmppos_item`
--
ALTER TABLE `tbtmppos_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT for table `tbtmpsuppliesitems`
--
ALTER TABLE `tbtmpsuppliesitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbusers`
--
ALTER TABLE `tbusers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
