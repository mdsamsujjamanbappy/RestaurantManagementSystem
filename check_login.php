<?php session_start();
  require_once("assets/tools/loginfunctions.php"); 
  require_once("assets/tools/functions.php"); 
  $db_handle = new myLoginFunctions();                          
  $db_handle1 = new myFunctions();                          

  if(isset($_POST['uname'])&&isset($_POST['upass'])){
   $uname = ($_POST['uname']);
   $upass = ($_POST['upass']);
   $type = ($_POST['userType']);
   $r = $db_handle->checkMSBUserLoginDetails($uname,$upass,$type);

   if(count($r)>0){ 
      $results = $db_handle->getMSBUserLoginDetails($uname,$upass,$type);
      foreach($results as $user) {
        $id=htmlentities($user["id"]); 
        $userFullName=htmlentities($user["empName"]); 
        $userTypeId=htmlentities($user["userTypeId"]);
        // $empImage=htmlentities($user["empImage"]);
      }

      if($userTypeId==1){
        $_SESSION['adminRMSAccess']="Permit";
        $_SESSION['adminRMSId']=$id;
        $_SESSION['adminRMSfname']=$userFullName; 
        $_SESSION['adminRMSImage'] = "admin.jpg";

        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d');
        $time = date("h:i:sa");
        $results = $db_handle1->insertActivityLog($_SESSION['adminRMSId'],"Sign in", $date, $time);
        echo "adminLogin";
      }

      if($type==2){
        $_SESSION['exRMSAccess']="Permit";
        $_SESSION['exRMSId']=$id;
        $_SESSION['exRMSfname']=$userFullName; 
        $_SESSION['exRMSImage'] = "executive.jpg";
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d');
        $time = date("h:i:sa");
        $results = $db_handle1->insertActivityLog($_SESSION['exRMSId'],"Sign in", $date, $time);
        echo "executiveLogin";
      }
   }
}
?>